#ifndef HEADER_ZEPHYR_EXCEPTIONS
#define HEADER_ZEPHYR_EXCEPTIONS 1

/// @file
/// @brief Exception definitions

#include <string>
#include <utility>
#include <exception>

#include "zephyrlib-so-api.hxx"

namespace zephyr {

    /// <summary>
    /// Base class for exceptions thrown by the library
    /// </summary>
    class ZEPHYRLIB_API Exception : public std::exception {
    private:
        const char * m_msg;
        std::string m_str;
    public:

        /// <summary>
        /// Constructs exception using <c>c-string</c>
        /// <remarks>
        /// The string argument is not maintained - expected to be have global lifetime semantics
        /// </remarks>
        /// </summary>
        /// <param name="msg">message string</param>
        explicit Exception(const char * msg) : m_msg(msg) {}

        /// <summary>
        /// Constructs exception using <c>std::string</c>
        /// </summary>
        /// <param name="msg">message string</param>
        explicit Exception(std::string msg) : m_str(std::move(msg)) { m_msg = m_str.c_str(); }

        /// <summary>
        /// Gets exception message
        /// </summary>
        virtual char const * what() const noexcept override { return m_msg; }

    };

    /// <summary>
    /// Thrown during module compilation
    /// </summary>
    class ZEPHYRLIB_API CompilationException : public Exception { using Exception::Exception; };

    /// <summary>
    /// Thrown during serialization
    /// </summary>
    class ZEPHYRLIB_API SerializationException : public Exception { using Exception::Exception; };

    /// <summary>
    /// Thrown when an unexpected argument is provided
    /// </summary>
    class ZEPHYRLIB_API IllegalArgumentException : public Exception { using Exception::Exception; };

    /// <summary>
    /// Thrown when an internal assertion check fails
    /// </summary>
    class ZEPHYRLIB_API AssertionException : public Exception { using Exception::Exception; };

    /// <summary>
    /// Thrown when an unexpected value is <c>nullptr</c>
    /// </summary>
    class ZEPHYRLIB_API NullPointerException : public Exception { using Exception::Exception; };

}

#endif
