#ifndef HEADER_ZEPHYR_RTTI_VALUE
#define HEADER_ZEPHYR_RTTI_VALUE 1

/// @file
/// @brief Type-erased value interface

#include <memory>
#include <string>
#include <utility>
#include <istream>
#include <ostream>
#include <typeinfo>
#include <type_traits>

#include "../def.hpp"
#include "../exceptions.hpp"
#include "descriptor.hpp"

#include "zephyrlib-so-api.hxx"

namespace zephyr {
    namespace rtti {

        /// @ingroup Value Interface
        /// <summary>
        /// Value interface
        /// </summary>
        struct ZEPHYRLIB_API Value {
        protected:
            ZEPHYR_DECLARE_DEFAULT_CTORS_5(Value);

        public:

            virtual ~Value() = default;

            /// <summary>
            /// Type info of this value implementation
            /// </summary>
            virtual const std::type_info & typeinfo() const = 0;

            /// <summary>
            /// Raw data
            /// </summary>
            virtual const void * data() const = 0;

            /// <summary>
            /// Raw data
            /// </summary>
            virtual void * data() = 0;

            virtual std::unique_ptr<Value> copy() const { throw zephyr::Exception("type not copyable"); }

            virtual void serialize(std::ostream & out, bool binary = true, usize & crc = voidCrc) const { throw zephyr::Exception("type serialization not supported"); }

            virtual void deserialize(std::istream & in, bool binary = true, usize & crc = voidCrc) { throw zephyr::Exception("type deerialization not supported"); }

            /// <summary>
            /// Reinterpret cast as target type
            /// </summary>
            /// <typeparam name="type">target type</typeparam>
            /// <returns>Reinterpreted value reference</returns>
            template<typename type>
            type & cast() { return *static_cast<type *>(data()); }

            /// <summary>
            /// Reinterpret cast as target const type
            /// </summary>
            /// <typeparam name="type">target type</typeparam>
            /// <returns>Reinterpreted value reference</returns>
            /// <seealso cref="Value::reinterpret<type>()"/>
            template<typename type>
            const type & cast() const { return *static_cast<const type *>(data()); }

        protected:
            static usize voidCrc;

        };

        /// <summary>
        /// Concrete value implementation
        /// </summary>
        /// <typeparam name="value_t">value type</typeparam>
        template<typename value_t>
        struct ValueImpl : Value {

            value_t value;

            /// <summary>
            /// Initializes a value object with the specified object
            /// </summary>
            /// <param name="value">value object</param>
            explicit ValueImpl(value_t value) : value(std::move(value)) {}

            virtual const std::type_info & typeinfo() const override { return typeid(value_t); }
            virtual void * data() override { return static_cast<void *>(&value); }
            virtual const void * data() const override { return static_cast<const void *>(&value); }

            virtual void serialize(std::ostream & out, bool binary = true, usize & crc = Value::voidCrc) const override {
                return Value::serialize(out, binary, crc);
            }

            virtual void deserialize(std::istream & in, bool binary = true, usize & crc = Value::voidCrc) override {
                return Value::deserialize(in, binary, crc);
            }

        };

        /// <summary>
        /// Binds target object as <see cref="zephyr::rtti::Value"/>
        /// </summary>
        /// <typeparam name="x">type</typeparam>
        /// <param name="value">value object</param>
        /// <returns><see cref="zephyr::rtti::ValueImpl"/> object</returns>
        template<typename x>
        inline auto BindValue(x value) -> ValueImpl<x> { return ValueImpl<x>(std::move(value)); }

        /// <seealso cref="zephyr::rtti::BindValue(value, identifier)"/>
        template<typename x>
        inline auto BindValueShared(x value) -> std::shared_ptr<ValueImpl<x>> {
            return std::shared_ptr<ValueImpl<x>>(new ValueImpl<x>(std::move(value)));
        }

    }
}

namespace zephyr {
    namespace rtti {
#define impl_a_(type) \
        template<> ZEPHYRLIB_API void ValueImpl<type>::serialize(std::ostream &, bool, usize &) const; \
        template<> ZEPHYRLIB_API void ValueImpl<type>::deserialize(std::istream &, bool, usize &);
#define impl_b_(type) impl_a_(signed type); impl_a_(unsigned type)
        impl_a_(bool);
        impl_b_(char);
        impl_b_(short);
        impl_b_(int);
        impl_b_(long);
        impl_b_(long long);
        impl_a_(float);
        impl_a_(double);
        impl_a_(long double);
#undef impl_b_
#undef impl_a_
    }
}

#endif
