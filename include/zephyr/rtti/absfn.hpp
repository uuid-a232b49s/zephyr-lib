#ifndef HEADER_ZEPHYR_RTTI_ABSFN
#define HEADER_ZEPHYR_RTTI_ABSFN 1

/// @file
/// @brief Invocation RTTI

#include <memory>
#include <vector>
#include <string>
#include <utility>
#include <typeinfo>

#include "../def.hpp"

#include "zephyrlib-so-api.hxx"

namespace zephyr {
    namespace rtti {
        template<typename signature> struct AbstractFn;
    }
}

namespace zephyr {
    namespace rtti {

        /// <summary>
        /// Base specialization for every other AbstractFn instance
        /// </summary>
        template<>
        struct ZEPHYRLIB_API AbstractFn<void> {
        protected:
            ZEPHYR_DECLARE_DEFAULT_CTORS_5(AbstractFn);
        public:
            virtual ~AbstractFn() = default;
        };

        /// <summary>
        /// Invocable interface base
        /// </summary>
        /// <typeparam name="ret_t">return type</typeparam>
        /// <typeparam name="...args_t">argument types</typeparam>
        template<typename ret_t, typename ... args_t>
        struct AbstractFn<ret_t(args_t...)> : AbstractFn<void> {
        protected:
            ZEPHYR_DECLARE_DEFAULT_CTORS_5(AbstractFn);
        public:
            /// <summary>
            /// Invokes the underlying implementation
            /// </summary>
            /// <param name="...args">arguments</param>
            /// <returns>Implementation defined</returns>
            virtual ret_t invoke(args_t ... args) const = 0;
        };

        /// <summary>
        /// Late bound interface
        /// </summary>
        struct ZEPHYRLIB_API LateBoundFn : AbstractFn<void(void * address, const isize * offsets)> {
        protected:
            ZEPHYR_DECLARE_DEFAULT_CTORS_5(LateBoundFn);
        public:

            /// <summary>
            /// Invokes the bound method
            /// 
            /// <para>
            /// Offsets[N] represents an offset for argument at index N<br>
            /// If return type is non-void then offsets[argCount] represents offset for the return value<br>
            /// Target argument memory address is defined as:
            /// \code{.cpp}
            /// auto argN = reinterpret_cast<unsigned char *>(address) + offsets[N];
            /// \endcode
            /// </para>
            /// 
            /// <para>
            /// Arguments are guaranteed to hold initialized values<br>
            /// Return value is not initialized upon this method being invoked - it must be initialized by the implementation
            /// </para>
            /// 
            /// <para>
            /// Implementation details:<br>
            /// Move-consumes supplied arguments (dtors not called), move-constructs return value (placement)<br>
            /// Example (pseudocode):
            /// \code{.cpp}
            /// auto fn = BindFn<long(int, short)>([] (int a, short b) -> long { return a + b; });
            /// int argA = 1;
            /// short argB = 2;
            /// std::aligned_storage<long> ret; // must not be initialized - as far as RAII is concerned
            /// isize args[] = {
            ///     reinterpret_cast<isize>(reinterpret_cast<void *>(&argA)) - reinterpret_cast<isize>(reinterpret_cast<void *>(0)),
            ///     reinterpret_cast<isize>(reinterpret_cast<void *>(&argB)) - reinterpret_cast<isize>(reinterpret_cast<void *>(0)),
            ///     reinterpret_cast<isize>(reinterpret_cast<void *>(&ret)) - reinterpret_cast<isize>(reinterpret_cast<void *>(0))
            /// };
            /// fn.invoke(nullptr, args); // if fails (via exception) ret is assumed to remain uninitialized
            /// std::cerr << ret << std::endl; // 3
            /// reinterpret_cast<long*>(ret)->~long(); // manually deallocate 'ret'
            /// \endcode
            /// </para>
            /// </summary>
            /// <param name="address">base argument address</param>
            /// <param name="offsets">argument offsets relative to base address</param>
            virtual void invoke(void * address, const isize * offsets) const override = 0;

            /// <summary>
            /// Return typeid
            /// </summary>
            virtual const std::type_info * ret() const = 0;

            /// <summary>
            /// Argument typeids
            /// </summary>
            virtual const std::vector<const std::type_info *> & args() const = 0;

        };

    }
}

#include "absfn.details.inl"

namespace zephyr {
    namespace rtti {

        template<typename signature_t, typename lambda_t>
        struct LateBoundLambda;

        /// <summary>
        /// Late-bound lambda implementation
        /// </summary>
        /// <typeparam name="lambda_t">lambda type (function pointers supported)</typeparam>
        /// <typeparam name="ret_t">return type</typeparam>
        /// <typeparam name="...args_t">argument types</typeparam>
        template<typename lambda_t, typename ret_t, typename ... args_t>
        struct LateBoundLambda<ret_t(args_t...), lambda_t> : LateBoundFn {
            using SignatureType = ret_t(args_t...);
            using LambdaType = lambda_t;
        private:
            mutable lambda_t m_lambda;
        public:

            /// <summary>
            /// Constructs with lambda
            /// </summary>
            /// <param name="lambda">lambda object</param>
            LateBoundLambda(lambda_t lambda) : m_lambda(std::move(lambda)) {}

            virtual void invoke(void * address, const isize * offsets) const override {
                details::invokeSequence<sizeof...(args_t)>::template invoke<lambda_t, ret_t, args_t...>(m_lambda, address, offsets);
            }

            virtual const std::type_info * ret() const override { return &typeid(ret_t); }

            virtual const std::vector<const std::type_info *> & args() const override {
                static std::vector<const std::type_info *> args0 = { (&typeid(args_t))... };
                return args0;
            }

        };

        /// <summary>
        /// Binds specified lambda
        /// </summary>
        /// <typeparam name="signature_t">function signature</typeparam>
        /// <typeparam name="lambda_t">lambda type</typeparam>
        /// <param name="lambda">lambda</param>
        /// <returns>Late-bound implementation</returns>
        template<typename signature_t, typename lambda_t>
        inline auto BindFn(lambda_t lambda) -> LateBoundLambda<signature_t, lambda_t> { return LateBoundLambda<signature_t, lambda_t>(std::move(lambda)); }

        /// <summary>
        /// Binds specified lambda
        /// </summary>
        /// <typeparam name="signature_t">function signature</typeparam>
        /// <typeparam name="lambda_t">lambda type</typeparam>
        /// <param name="lambda">lambda</param>
        /// <returns>Late-bound implementation shared pointer</returns>
        /// <seealso cref="BindFn(lambda_t)"/>
        template<typename signature_t, typename lambda_t>
        inline auto BindFnShared(lambda_t lambda) -> std::shared_ptr<LateBoundLambda<signature_t, lambda_t>> {
            using x = LateBoundLambda<signature_t, lambda_t>;
            return std::shared_ptr<x>(new x(std::move(lambda)));
        }

    }
}

#endif
