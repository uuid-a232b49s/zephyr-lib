#ifndef HEADER_ZEPHYR_RTTI_TYPEADDR
#define HEADER_ZEPHYR_RTTI_TYPEADDR 1

/// @file
/// @brief Address related type definitions

#include "zephyrlib-so-api.hxx"

namespace zephyr {
    namespace rtti {

        /// <summary>
/// Address locality
/// <para>
/// Describes where the target object is located
/// </para>
/// </summary>
        enum struct AddressLocality : bool {

            /// <summary>
            /// Object is located on the heap
            /// <para>
            /// Unrestricted access across threads
            /// </para>
            /// </summary>
            heap = 0,

            /// <summary>
            /// Object is located on the stack
            /// <para>
            /// Only the owning thread can access the underlying object
            /// </para>
            /// </summary>
            stack = 1

        };

        /// <summary>
        /// Address constraints descriptor
        /// </summary>
        struct ZEPHYRLIB_API AddressConstraints {

            /// <summary>
            /// Address locality
            /// </summary>
            AddressLocality locality : 1;

            /// <summary>
            /// whether the object is constant
            /// </summary>
            bool immutable : 1;

            constexpr inline AddressConstraints(AddressLocality locality = AddressLocality::heap, bool immutable = false) noexcept :
                locality(locality),
                immutable(immutable) {}

            constexpr inline AddressConstraints(unsigned char value) noexcept :
                locality(static_cast<AddressLocality>(value & (0x1u << 0u))),
                immutable(static_cast<bool>(value & (0x1u << 1u))) {}

            /// <summary>
            /// integral representation of this address constraints object
            /// </summary>
            constexpr inline unsigned char value() const noexcept {
                using t = unsigned char;
                return (static_cast<t>(locality) << 0) | (static_cast<t>(immutable) << 1);
            }

        };

    }
}

#endif
