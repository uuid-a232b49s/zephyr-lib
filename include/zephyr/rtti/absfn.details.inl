#if !defined(HEADER_ZEPHYR_RTTI_ABSFN) | defined(HEADER_ZEPHYR_RTTI_ABSFN_DETAILS)
#error illegal include
#endif
#define HEADER_ZEPHYR_RTTI_ABSFN_DETAILS 1

#include <new>

#include "../def.hpp"

namespace zephyr {
    namespace rtti {
        namespace details {

            template<typename ret_t, typename ... args_t>
            struct invokeLambda {
                template<typename lambda_t, usize ... S>
                static void invoke(lambda_t & lambda, void * address, const isize * offsets) {
                    new (reinterpret_cast<unsigned char *>(address) + offsets[sizeof...(args_t)]) ret_t(lambda((*reinterpret_cast<args_t *>(reinterpret_cast<unsigned char *>(address) + offsets[S]))...));
                }
            };

            template<typename ... args_t>
            struct invokeLambda<void, args_t...> {
                template<typename lambda_t, usize ... S>
                static void invoke(lambda_t & lambda, void * address, const isize * offsets) {
                    lambda((*reinterpret_cast<args_t *>(reinterpret_cast<unsigned char *>(address) + offsets[S]))...);
                }
            };

            template<>
            struct invokeLambda<void> {
                template<typename lambda_t, usize...>
                static void invoke(lambda_t & lambda, void *, const isize *) { lambda(); }
            };

            template<usize N, usize ...S>
            struct invokeSequence : invokeSequence<N - 1, N - 1, S...> {};
            template<usize ...S>
            struct invokeSequence<0, S...> {
                template<typename lambda_t, typename ret_t, typename ... args_t>
                static void invoke(lambda_t & lambda, void * address, const isize * offsets) {
                    invokeLambda<ret_t, args_t...>::template invoke<lambda_t, S...>(lambda, address, offsets);
                }
            };

        }
    }
}
