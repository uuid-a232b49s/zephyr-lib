#if !defined(HEADER_ZEPHYR_RTTI_DESCRIPTOR) | defined(HEADER_ZEPHYR_RTTI_DESCRIPTOR_TRAITS)
#error illegal include
#endif
#define HEADER_ZEPHYR_RTTI_DESCRIPTOR_TRAITS 1

#include <new>
#include <utility>
#include <type_traits>

namespace zephyr {
    namespace rtti {
        namespace traits {

            namespace details {

                struct err {};

                template<bool test>
                using conditional = typename std::conditional<test, std::true_type, std::false_type>::type;

            }

            template<typename x>
            static void ctor_(void * m) { new (m) x(); }
            template<typename x>
            using ctorTest = typename details::conditional<std::is_default_constructible<x>::value>;
            template<typename x, bool valid_>
            struct ctorImpl { static std::shared_ptr<AbstractFn<void(void * memory)>> ctor0() { return nullptr; } };
            template<typename x>
            struct ctorImpl<x, true> {
                static std::shared_ptr<AbstractFn<void(void * memory)>> ctor0() {
                    struct impl_t : AbstractFn<void(void * memory)> {
                        virtual void invoke(void * m) const override { ctor_<x>(m); }
                    } static impl;
                    static std::shared_ptr<AbstractFn<void(void * memory)>> ptr{ &impl, [] (void *) {} };
                    return ptr;
                }
            };

            template<typename x>
            static void dtor_(void * m) { reinterpret_cast<x *>(m)->~x(); }
            template<typename x>
            using dtorTest = typename std::conditional<(std::is_destructible<x>::value), std::true_type, std::false_type>::type;
            template<typename x, bool valid_>
            struct dtorImpl { static constexpr bool hasDtor0 = false; static std::shared_ptr<AbstractFn<void(void * memory)>> dtor0() { return nullptr; } };
            template<typename x>
            struct dtorImpl<x, true> {
                static constexpr bool hasDtor0 = true;
                static std::shared_ptr<AbstractFn<void(void * memory)>> dtor0() {
                    struct impl_t : AbstractFn<void(void * memory)> {
                        virtual void invoke(void * m) const override { dtor_<x>(m); }
                    } static impl;
                    static std::shared_ptr<AbstractFn<void(void * memory)>> ptr{ &impl, [] (void *) {} };
                    return ptr;
                }
            };

            template<typename x>
            static void copy_(const void * o, void * m) {
                new (m) x(*reinterpret_cast<const x *>(o));
            }
            template<typename x>
            using copyTest = typename std::conditional<(std::is_copy_constructible<x>::value), std::true_type, std::false_type>::type;
            template<typename x, bool valid_>
            struct copyImpl { static constexpr bool hasCopy0 = false; static std::shared_ptr<AbstractFn<void(const void * object, void * memory)>> copy0() { return nullptr; } };
            template<typename x>
            struct copyImpl<x, true> {
                static constexpr bool hasCopy0 = true;
                static std::shared_ptr<AbstractFn<void(const void * object, void * memory)>> copy0() {
                    struct impl_t : AbstractFn<void(const void * object, void * memory)> {
                        virtual void invoke(const void * o, void * m) const override { copy_<x>(o, m); }
                    } static impl;
                    static std::shared_ptr<AbstractFn<void(const void * object, void * memory)>> ptr{ &impl, [] (void *) {} };
                    return ptr;
                }
            };

            template<typename x>
            static void move_(void * o, void * m) {
                {
                    using namespace std;
                    new (m) x(move(*reinterpret_cast<x *>(o)));
                }
                dtor_<x>(o);
            }
            template<typename x>
            using moveTest = typename std::conditional<(std::is_move_constructible<x>::value && dtorTest<x>::value), std::true_type, std::false_type>::type;
            template<typename x, bool valid_>
            struct moveImpl { static std::shared_ptr<AbstractFn<void(void * object, void * memory)>> move0() { return nullptr; } };
            template<typename x>
            struct moveImpl<x, true> {
                static std::shared_ptr<AbstractFn<void(void * object, void * memory)>> move0() {
                    struct impl_t : AbstractFn<void(void * object, void * memory)> {
                        virtual void invoke(void * o, void * m) const override { move_<x>(o, m); }
                    } static impl;
                    static std::shared_ptr<AbstractFn<void(void * object, void * memory)>> ptr{ &impl, [] (void *) {} };
                    return ptr;
                }
            };

            template<typename x>
            static bool toBool_(void * o) {
                bool v = static_cast<bool>(*reinterpret_cast<x *>(o));
                dtor_<x>(o);
                return v;
            }
            template<typename x>
            struct toBoolTest {
                template<typename a> static auto test(a *) -> decltype(static_cast<bool>(*reinterpret_cast<const a *>(0)));
                template<typename> static details::err test(...);
                static constexpr bool value = (!std::is_same<decltype(test<x>(0)), details::err>::value) && dtorTest<x>::value;
            };
            template<typename x, bool valid_>
            struct toBoolImpl {
                static constexpr bool hasToBool0 = false;
                static std::shared_ptr<AbstractFn<bool(void * object)>> toBool0() { return nullptr; }
            };
            template<typename x>
            struct toBoolImpl<x, true> {
                static constexpr bool hasToBool0 = true;
                static std::shared_ptr<AbstractFn<bool(void * object)>> toBool0() {
                    struct impl_t : AbstractFn<bool(void * object)> {
                        virtual bool invoke(void * o) const override { return toBool_<x>(o); }
                    } static impl;
                    static std::shared_ptr<AbstractFn<bool(void * object)>> ptr{ &impl, [] (void *) {} };
                    return ptr;
                }
            };

        }
    }
}
