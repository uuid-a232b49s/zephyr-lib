#ifndef HEADER_ZEPHYR_RTTI_DESCRIPTOR
#define HEADER_ZEPHYR_RTTI_DESCRIPTOR 1

/// @file
/// @brief RTTI descriptor interfaces

#include <memory>
#include <utility>
#include <typeinfo>

#include "../def.hpp"
#include "absfn.hpp"

#include "zephyrlib-so-api.hxx"

namespace zephyr {
    namespace rtti {

        /// <summary>
        /// Runtime type information descriptor
        /// <para>
        /// Defines runtime type interaction interface
        /// </para>
        /// </summary>
        struct ZEPHYRLIB_API RttiDescriptor {
        protected:
            ZEPHYR_DECLARE_DELETED_CTORS_4(RttiDescriptor);
            RttiDescriptor() = default;

        public:
            virtual ~RttiDescriptor() = default;

            /// <summary>
            /// Bounds type size
            /// </summary>
            virtual usize size() const = 0;

            /// <summary>
            /// Bound type align
            /// </summary>
            virtual usize align() const = 0;

            /// <summary>
            /// Typeid info object of the bound type
            /// </summary>
            virtual const std::type_info & typeinfo() const = 0;

            /// <summary>
            /// Typeid info object of bound type pointer
            /// </summary>
            virtual const std::type_info & ptrTypeinfo(bool immutable = false) const = 0;

            /// <summary>
            /// Default initializer
            /// <para>
            /// Initializes uninitialized arg[0]
            /// </para>
            /// </summary>
            virtual std::shared_ptr<AbstractFn<void(void * memory)>> ctor() const { return nullptr; }

            /// <summary>
            /// Destructor
            /// <para>
            /// Deinitializes initialized arg[0]
            /// </para>
            /// </summary>
            virtual std::shared_ptr<AbstractFn<void(void * object)>> dtor() const { return nullptr; }

            /// <summary>
            /// Copy
            /// <para>
            /// Copies initialized arg[0] into uninitalized arg[1]
            /// </para>
            /// </summary>
            virtual std::shared_ptr<AbstractFn<void(const void * object, void * memory)>> copy() const { return nullptr; }

            /// <summary>
            /// Move
            /// <para>
            /// Moves initialized arg[0] into uninitialized arg[1], deinitializes arg[0]
            /// </para>
            /// </summary>
            virtual std::shared_ptr<AbstractFn<void(void * object, void * memory)>> move() const { return nullptr; }

            /// <summary>
            /// Bool conversion
            /// <para>
            /// Converts initialized object to bool representation, deinitializes object
            /// </para>
            /// </summary>
            virtual std::shared_ptr<AbstractFn<bool(void * object)>> toBool() const { return nullptr; }

        };

    }
}

#include "descriptor.traits.inl"

namespace zephyr {
    namespace rtti {

        /// <summary>
        /// Automatic generic implementation
        /// <para>
        /// The type must be: non-const non-volatile non-reference non-decayable
        /// </para>
        /// </summary>
        /// <typeparam name="x">Bound c++ native type</typeparam>
        template<typename x>
        struct RttiDescriptorGeneric : RttiDescriptor {

            static_assert(!std::is_const<x>::value, "const types not supported");
            static_assert(!std::is_volatile<x>::value, "volatile types not supported");
            static_assert(!std::is_reference<x>::value, "reference types not supported");
            static_assert(std::is_same<x, typename std::decay<x>::type>::value, "decayable types not supported");

        public:

            /// <summary>
            /// Bound type
            /// </summary>
            using Type = x;

            virtual usize size() const override { return sizeof(x); }
            virtual usize align() const override { return alignof(x); }
            virtual const std::type_info & typeinfo() const override { return typeid(x); }
            virtual const std::type_info & ptrTypeinfo(bool immutable = false) const override {
                return immutable ? typeid(const x *) : typeid(x *);
            }

            virtual std::shared_ptr<AbstractFn<void(void * memory)>> ctor() const override {
                return traits::ctorImpl<x, traits::ctorTest<x>::value>::ctor0();
            }

            virtual std::shared_ptr<AbstractFn<void(void * memory)>> dtor() const override {
                return traits::dtorImpl<x, traits::dtorTest<x>::value>::dtor0();
            }

            virtual std::shared_ptr<AbstractFn<void(const void * object, void * memory)>> copy() const override {
                return traits::copyImpl<x, traits::copyTest<x>::value>::copy0();
            }

            virtual std::shared_ptr<AbstractFn<void(void * object, void * memory)>> move() const override {
                return traits::moveImpl<x, traits::moveTest<x>::value>::move0();
            }

            virtual std::shared_ptr<AbstractFn<bool(void * object)>> toBool() const override {
                return traits::toBoolImpl<x, traits::toBoolTest<x>::value>::toBool0();
            }

            /// <summary>
            /// Static instance for this generic rtti
            /// </summary>
            static const std::shared_ptr<const RttiDescriptorGeneric> & Instance() noexcept;

        };

        template<>
        struct ZEPHYRLIB_API RttiDescriptorGeneric<void> : RttiDescriptor {
            using Type = void;
            virtual usize size() const override { return 0; }
            virtual usize align() const override { return 0; }
            virtual const std::type_info & typeinfo() const override;
            virtual const std::type_info & ptrTypeinfo(bool immutable = false) const override;
            static const std::shared_ptr<const RttiDescriptorGeneric> & Instance() noexcept;
        };

    }
}

namespace zephyr {
    namespace rtti {
#define impl_1_(type) template<> ZEPHYRLIB_API const std::shared_ptr<const RttiDescriptorGeneric<type>> & RttiDescriptorGeneric<type>::Instance() noexcept;
#define impl_2_(type) impl_1_(signed type); impl_1_(unsigned type)
        impl_1_(bool);
        impl_2_(char);
        impl_2_(short);
        impl_2_(int);
        impl_2_(long);
        impl_2_(long long);
        impl_1_(float);
        impl_1_(double);
        impl_1_(long double);
#undef impl_2_
#undef impl_1_
    }
}

namespace zephyr {
    namespace rtti {
        template<typename x>
        const std::shared_ptr<const RttiDescriptorGeneric<x>> & RttiDescriptorGeneric<x>::Instance() noexcept {
            static const RttiDescriptorGeneric instance;
            static const std::shared_ptr<const RttiDescriptorGeneric<x>> ptr { &instance, [] (const void *) noexcept {} };
            return ptr;
        }
    }
}

#endif
