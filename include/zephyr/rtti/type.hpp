#ifndef HEADER_ZEPHYR_RTTI_TYPE
#define HEADER_ZEPHYR_RTTI_TYPE 1

/// @file
/// @brief Runtime type interface

#include <memory>
#include <string>

#include "../def.hpp"
#include "symbol.hpp"
#include "type-addr.hpp"

#include "zephyrlib-so-api.hxx"

namespace zephyr {
    namespace rtti {
        struct RttiDescriptor;
        struct AddressType;
        struct AddressConstraints;
    }
}

namespace zephyr {
    namespace rtti {

        /// <summary>
        /// Type interface
        /// <para>
        /// Defines runtime type information interface
        /// </para>
        /// </summary>
        struct ZEPHYRLIB_API Type : Symbol {
        protected:
            Type() = default;
            ZEPHYR_DECLARE_DELETED_CTORS_4(Type);

        public:

            virtual ~Type() = default;

            /// <summary>
            /// Type size
            /// </summary>
            virtual usize size() const = 0;

            /// <summary>
            /// Type alignment
            /// </summary>
            virtual usize align() const = 0;

            /// <summary>
            /// Obtains address (pointer) type from this type
            /// </summary>
            /// <param name="constraints">address constraints</param>
            /// <returns>Address type object</returns>
            virtual std::shared_ptr<const AddressType> address(AddressConstraints constraints = AddressConstraints()) const = 0;

        };

        /// <summary>
        /// Managed pointer alias for <see cref="Type"/>
        /// </summary>
        using TypePtr = std::shared_ptr<const Type>;

        /// <summary>
        /// Native type
        /// <para>
        /// Represents a native c++ bound type
        /// </para>
        /// </summary>
        struct ZEPHYRLIB_API NativeType : virtual Type {
        protected:
            NativeType() = default;
        public:
            /// <summary>
            /// Rtti descriptor for this type
            /// </summary>
            virtual std::shared_ptr<const RttiDescriptor> rtti() const = 0;
        };

        /// <summary>
        /// Void type
        /// </summary>
        struct ZEPHYRLIB_API VoidType : Type {
        private:
            VoidType() = default;
            ~VoidType() = default;
        public:
            /// <summary>
            /// Singleton instance
            /// </summary>
            static const std::shared_ptr<const VoidType> Instance;
        };

        /// <summary>
        /// Address type
        /// </summary>
        struct ZEPHYRLIB_API AddressType : virtual Type, virtual NativeType {
        protected:
            AddressType() = default;
        public:

            /// <summary>
            /// Constraints of this address
            /// </summary>
            virtual AddressConstraints constraints() const = 0;

            /// <summary>
            /// Address element
            /// </summary>
            /// <returns>dereferenced type</returns>
            virtual std::shared_ptr<const Type> base() const = 0;

        };

    }
}

#endif
