#ifndef HEADER_ZEPHYR_RTTI_SYMBOL
#define HEADER_ZEPHYR_RTTI_SYMBOL 1

/// @file
/// @brief Smybol interface

#include <memory>
#include <string>

#include "../def.hpp"

#include "zephyrlib-so-api.hxx"

namespace zephyr {
    namespace rtti {
        struct Module;
        struct Visitor;
    }
}

namespace zephyr {
    namespace rtti {

        /// <summary>
        /// Module-bound resource
        /// </summary>
        struct ZEPHYRLIB_API Symbol {
        protected:
            ZEPHYR_DECLARE_DEFAULT_CTORS_5(Symbol);

        public:

            virtual ~Symbol() = default;

            /// <summary>
            /// Identifier of this symbol
            /// </summary>
            virtual const std::string & identifier() const = 0;

            /// <summary>
            /// Containing module
            /// </summary>
            /// <returns>module pointer</returns>
            virtual std::shared_ptr<const Module> module() const = 0;

            virtual std::shared_ptr<void> accept(Visitor *) const = 0;

        };

        /// <summary>
        /// Managed pointer alias for <see cref="Symbol"/>
        /// </summary>
        using SymbolPtr = std::shared_ptr<const Symbol>;

    }
}

#endif
