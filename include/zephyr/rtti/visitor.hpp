#ifndef HEADER_ZEPHYR_RTTI_VISITOR
#define HEADER_ZEPHYR_RTTI_VISITOR 1

#include <memory>

#include "zephyrlib-so-api.hxx"

namespace zephyr {
    namespace rtti {
        struct NativeType;
        struct VoidType;
        struct AddressType;
        struct Method;
    }
}

namespace zephyr {
    namespace rtti {

        struct ZEPHYRLIB_API Visitor {
            virtual ~Visitor() = default;
            virtual std::shared_ptr<void> visitDefault() { return nullptr; }
            virtual std::shared_ptr<void> visit(std::shared_ptr<const Method>) { return visitDefault(); }
            virtual std::shared_ptr<void> visit(std::shared_ptr<const NativeType>) { return visitDefault(); }
            virtual std::shared_ptr<void> visit(std::shared_ptr<const VoidType>) { return visitDefault(); }
            virtual std::shared_ptr<void> visit(std::shared_ptr<const AddressType>) { return visitDefault(); }
        };

    }
}

#endif
