#ifndef HEADER_ZEPHYR_RTTI_METHOD
#define HEADER_ZEPHYR_RTTI_METHOD 1

/// @file
/// @brief Runtime method interface

#include <memory>
#include <vector>
#include <string>

#include "../def.hpp"
#include "symbol.hpp"
#include "visitor.hpp"

#include "zephyrlib-so-api.hxx"

namespace zephyr {
    namespace rtti {
        struct Type;
    }
}

namespace zephyr {
    namespace rtti {

        /// <summary>
        /// Method interface
        /// <para>
        /// Defines runtime method information interface
        /// </para>
        /// </summary>
        struct ZEPHYRLIB_API Method : Symbol {
        protected:
            Method() = default;
            ZEPHYR_DECLARE_DELETED_CTORS_4(Method);

        public:

            virtual ~Method() = default;

            /// <summary>
            /// Argument count
            /// </summary>
            virtual usize argc() const = 0;

            /// <summary>
            /// Argument type
            /// </summary>
            /// <param name="index">argument index</param>
            /// <returns>Argument type at specified index</returns>
            virtual std::shared_ptr<const Type> argv(usize index) const = 0;

            /// <summary>
            /// Return type
            /// </summary>
            /// <returns>Return type of this method</returns>
            virtual std::shared_ptr<const Type> ret() const = 0;

            /// <summary>
            /// Maximal stack size the method can use
            /// </summary>
            virtual usize maxStackSize() const = 0;

        };

        /// <summary>
        /// Managed pointer alias for <see cref="Method"/>
        /// </summary>
        using MethodPtr = std::shared_ptr<const Method>;

    }
}

#endif
