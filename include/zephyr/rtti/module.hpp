#ifndef HEADER_ZEPHYR_RTTI_MODULE
#define HEADER_ZEPHYR_RTTI_MODULE 1

/// @file
/// @brief Module interface

#include <memory>
#include <string>
#include <typeinfo>

#include "../def.hpp"

#include "zephyrlib-so-api.hxx"

namespace zephyr {
    namespace rtti {
        struct Symbol;
        struct NativeType;
    }
}

namespace zephyr {
    namespace rtti {

        /// <summary>
        /// Compiled module
        /// <para>
        /// Represents a collection of resources that are necessary to run any compiled method from this module
        /// </para>
        /// </summary>
        struct ZEPHYRLIB_API Module {
        protected:
            Module() = default;
            ZEPHYR_DECLARE_DELETED_CTORS_4(Module);

        public:
            virtual ~Module() = default;

            /// <summary>
            /// Number of referenced modules
            /// </summary>
            virtual usize referenceCount() const = 0;

            /// <summary>
            /// Obtains a referenced module
            /// </summary>
            /// <param name="index">index to lookup</param>
            /// <returns>Module pointer</returns>
            /// <exception cref="IllegalArgumentException">if index out of bounds</exception>
            virtual std::shared_ptr<const Module> referenceAt(usize index) const = 0;

            /// <summary>
            /// Finds named symbol
            /// </summary>
            /// <param name="identifier">identifier to search</param>
            /// <returns>Symbol pointer or <c>nullptr</c> if not found</returns>
            virtual std::shared_ptr<const rtti::Symbol> findSymbol(const std::string & identifier) const = 0;

            /// <summary>
            /// Finds named symbol via Module::findSymbol(const std::string &) and dynamic-casts to the target type
            /// </summary>
            /// <typeparam name="x">object type</typeparam>
            /// <param name="identifier">identifier to search</param>
            /// <returns>Object pointer</returns>
            template<typename x>
            std::shared_ptr<const x> find(const std::string & identifier) const { return std::dynamic_pointer_cast<const x>(findSymbol(identifier)); }

            /// <summary>
            /// Finds native type
            /// </summary>
            /// <param name="typeinfo"><c>std::type_info</c> object of the native type</param>
            /// <returns>Native type pointer or <c>nullptr</c> if not found</returns>
            virtual std::shared_ptr<const NativeType> findNativeType(const std::type_info & typeinfo) const = 0;

            /// <summary>
            /// Finds native type
            /// </summary>
            /// <typeparam name="type">native type to find</typeparam>
            /// <returns>Native type pointer or <c>nullptr</c> if not found</returns>
            template<typename type>
            std::shared_ptr<const NativeType> findNativeType() const { return findNativeType(typeid(type)); }

            /// <summary>
            /// Module identifier
            /// </summary>
            virtual const std::string & identifier() const = 0;

        };

        /// <summary>
        /// Managed pointer alias for <see cref="Module"/>
        /// </summary>
        using ModulePtr = std::shared_ptr<const Module>;

    }
}

#endif
