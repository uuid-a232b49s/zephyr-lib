/// @dir compilation
/// @brief compilation

/// @file
/// @brief All-includes compilation headers

#include "compilation/compilation-unit.hpp"
#include "compilation/dataprovider.hpp"
#include "compilation/method.hpp"
#include "compilation/opcode.hpp"
#include "compilation/type.hpp"
