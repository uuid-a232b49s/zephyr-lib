#ifndef HEADER_ZEPHYR_COMPILATION_OPCODE
#define HEADER_ZEPHYR_COMPILATION_OPCODE 1

/// @file
/// @brief Operational code definition

#include <memory>
#include <istream>
#include <ostream>

#include "../def.hpp"

#include "zephyrlib-so-api.hxx"

namespace zephyr {
    namespace rtti {
        struct Method;
        struct LateBoundFn;
        struct Value;
    }
    namespace compilation {
        struct LabelRef;
        struct LocalRef;
        struct CompilationUnit;
    }
}

namespace zephyr {
    namespace compilation {

        /// <summary>
        /// Operational code
        /// <para>
        /// Represents a basic operation
        /// </para>
        /// </summary>
        struct ZEPHYRLIB_API OpCode {
            friend struct CompilationUnit;

            /// <summary>
            /// Defines <see cref="OpCode"/> type
            /// </summary>
            enum struct Operator {
                null,
                ldArg,
                ldConst,
                ldLoc,
                stLoc,
                clLoc,
                dup,
                rem,
                ret,
                ref,
                deref,
                invoke,
                invokeLbFn,
                jmp,
                branch
            };

            /// <summary>
            /// Mutates behavior of an <see cref="OpCode"/> when dealing with an operand
            /// </summary>
            enum struct LoadMode {

                /// <summary>
                /// Copy operand
                /// </summary>
                copy,

                /// <summary>
                /// Move operand
                /// </summary>
                move

            };

            /// <summary>
            /// Internal opcode data
            /// </summary>
            struct ZEPHYRLIB_API Data {
                union ZEPHYRLIB_API {
                    usize ull;
                    struct ZEPHYRLIB_API {
                        usize i;
                        usize b;
                    } m_ull_b;
                };
                std::shared_ptr<const rtti::Method> method;
                std::shared_ptr<const rtti::LateBoundFn> lbfn;
                std::shared_ptr<const rtti::Value> value;
            };

        private:
            Operator m_operator = Operator::null;
            Data m_data = {};

            struct impl_;

        public:

            /// <summary>
            /// Retrieves opcode <see cref="Operator"/>
            /// </summary>
            Operator op() const noexcept { return m_operator; }

            /// <summary>
            /// Retrieves opcode <see cref="Data"/>
            /// </summary>
            const Data & data() const noexcept { return m_data; }

            /// <summary>
            /// Loads an argument onto the stack
            /// <table>
            /// <tr><th>Semantic</th><th>Behavior</th></tr>
            /// <tr><td>Consume</td><td>None</td></tr>
            /// <tr><td>Push</td><td>Value of target argument type</td></tr>
            /// </table>
            /// </summary>
            /// <param name="idx">target argument index</param>
            /// <param name="mode">whether to copy or move argument onto the stack</param>
            static OpCode ldArg(usize idx, LoadMode mode = LoadMode::copy) noexcept;

            /// <summary>
            /// Loads a constant onto the stack
            /// <table>
            /// <tr><th>Semantic</th><th>Behavior</th></tr>
            /// <tr><td>Consume</td><td>None</td></tr>
            /// <tr><td>Push</td><td>Value of target constant type</td></tr>
            /// </table>
            /// </summary>
            /// <param name="constant">target constant</param>
            static OpCode ldConst(std::shared_ptr<const rtti::Value> constant);

            /// <summary>
            /// Loads a local onto the stack
            /// <table>
            /// <tr><th>Semantic</th><th>Behavior</th></tr>
            /// <tr><td>Consume</td><td>None</td></tr>
            /// <tr><td>Push</td><td>Value of target local type</td></tr>
            /// </table>
            /// </summary>
            /// <param name="local">local to load</param>
            /// <param name="mode">load mode, move will clear the local</param>
            static OpCode ldLoc(LocalRef local, LoadMode mode = LoadMode::copy) noexcept;

            /// <summary>
            /// Consumes value on the stack & stores it into local
            /// <table>
            /// <tr><th>Semantic</th><th>Behavior</th></tr>
            /// <tr><td>Consume</td><td>Value of target local type</td></tr>
            /// <tr><td>Push</td><td>None</td></tr>
            /// </table>
            /// </summary>
            /// <param name="local">local to store into</param>
            static OpCode stLoc(LocalRef local) noexcept;

            /// <summary>
            /// Clears target local
            /// <table>
            /// <tr><th>Semantic</th><th>Behavior</th></tr>
            /// <tr><td>Consume</td><td>None</td></tr>
            /// <tr><td>Push</td><td>None</td></tr>
            /// </table>
            /// </summary>
            /// <param name="local">local to clear</param>
            static OpCode clLoc(LocalRef local) noexcept;

            /// <summary>
            /// Duplicates the value on the stack
            /// <table>
            /// <tr><th>Semantic</th><th>Behavior</th></tr>
            /// <tr><td>Consume</td><td>None</td></tr>
            /// <tr><td>Push</td><td>A copy of the topmost stack value</td></tr>
            /// </table>
            /// </summary>
            static OpCode dup() noexcept;

            /// <summary>
            /// Discards the value on the stack
            /// <table>
            /// <tr><th>Semantic</th><th>Behavior</th></tr>
            /// <tr><td>Consume</td><td>Top-most stack value</td></tr>
            /// <tr><td>Push</td><td>None</td></tr>
            /// </table>
            /// </summary>
            static OpCode rem() noexcept;

            /// <summary>
            /// Returns from method
            /// <table>
            /// <tr><th>Semantic</th><th>Behavior</th></tr>
            /// <tr><td>Consume</td><td>Return type value if non-void, otherwise none</td></tr>
            /// <tr><td>Push</td><td>None</td></tr>
            /// </table>
            /// </summary>
            static OpCode ret() noexcept;

            /// <summary>
            /// Not implemented
            /// </summary>
            static OpCode ref() noexcept;

            /// <summary>
            /// Not implemented
            /// </summary>
            static OpCode deref() noexcept;

            /// <summary>
            /// Invokes managed method
            /// </summary>
            /// <table>
            /// <tr><th>Semantic</th><th>Behavior</th></tr>
            /// <tr><td>Consume</td><td>Argument type values of the target method</td></tr>
            /// <tr><td>Push</td><td>Return type value of the target method (none if void)</td></tr>
            /// </table>
            /// <param name="method">method to invoke</param>
            static OpCode invoke(std::shared_ptr<const rtti::Method> method) noexcept;

            /// <summary>
            /// Invokes late-bound method
            /// <para>
            /// Same stack behavior semantics as <see cref="OpCode::invoke(std::shared_ptr<const rtti::Method>)"/>
            /// </para>
            /// </summary>
            /// <param name="method">method to invoke</param>
            static OpCode invoke(std::shared_ptr<const rtti::LateBoundFn> method);

            /// <summary>
            /// Jumps to target label
            /// <table>
            /// <tr><th>Semantic</th><th>Behavior</th></tr>
            /// <tr><td>Consume</td><td>None</td></tr>
            /// <tr><td>Push</td><td>None</td></tr>
            /// </table>
            /// </summary>
            /// <param name="label">label to jump to</param>
            static OpCode jmp(LabelRef label) noexcept;

            /// <summary>
            /// Conditionally jumps to target label
            /// <table>
            /// <tr><th>Semantic</th><th>Behavior</th></tr>
            /// <tr><td>Consume</td><td>Bool-convertible value</td></tr>
            /// <tr><td>Push</td><td>None</td></tr>
            /// </table>
            /// </summary>
            /// <param name="label">label to jump to</param>
            /// <param name="branchOnTrue">whether to jump when value is <c>true</c> or <c>false</c></param>
            /// <returns></returns>
            static OpCode branch(LabelRef label, bool branchOnTrue) noexcept;

        };

    }
}

#endif
