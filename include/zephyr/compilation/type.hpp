#ifndef HEADER_ZEPHYR_COMPILATION_TYPE
#define HEADER_ZEPHYR_COMPILATION_TYPE 1

/// @file
/// @brief Type compilation interface

#include <utility>

#include "../def.hpp"
#include "../rtti/type.hpp"

#include "zephyrlib-so-api.hxx"

namespace zephyr {
    namespace compilation {

        /// <summary>
        /// Type declaration
        /// </summary>
        struct ZEPHYRLIB_API Type : virtual rtti::Type {
        protected:
            Type() = default;
        public:
            virtual std::shared_ptr<const rtti::Module> module() const override { return nullptr; }
        };

        /// <summary>
        /// Native type declaration
        /// </summary>
        struct ZEPHYRLIB_API NativeType : virtual Type, virtual rtti::NativeType {
            using Type::identifier;

            /// <summary>
            /// Sets type identifier
            /// </summary>
            /// <param name="identifier">new type identifier</param>
            virtual void identifier(std::string identifier) = 0;

            virtual std::shared_ptr<const rtti::Module> module() const override { return Type::module(); }

        };

    }
}

#endif
