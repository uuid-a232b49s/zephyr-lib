#ifndef HEADER_ZEPHYR_COMPILATION_METHOD
#define HEADER_ZEPHYR_COMPILATION_METHOD 1

/// @file
/// @brief Method compilation declaration

#include <memory>
#include <vector>
#include <string>
#include <utility>
#include <typeinfo>

#include "../def.hpp"
#include "../rtti/type.hpp"
#include "../rtti/method.hpp"

#include "opcode.hpp"

#include "zephyrlib-so-api.hxx"

namespace zephyr {
    namespace rtti {
        struct Type;
    }
    namespace compilation {
        struct CompilationUnit;
        struct Method;
    }
}

namespace zephyr {
    namespace compilation {

        /// <summary>
        /// Method label reference
        /// </summary>
        struct ZEPHYRLIB_API LabelRef {
            friend struct OpCode;
            friend struct Method;

        private:
            Method * m_method = nullptr;
            usize m_lblIdx = ~0ull;

        public:

            /// <summary>
            /// Marks current position in between Method::emit invocations
            /// <para>the label will point to the next emitted opcode</para>
            /// </summary>
            /// <exception cref="AssertionException">if label is already marked or if this label was not obtained from <see cref="Method::label()"/></exception>
            void mark();

        };

        /// <summary>
        /// Local reference
        /// </summary>
        struct ZEPHYRLIB_API LocalRef {
            friend struct OpCode;
            friend struct Method;
        private:
            usize m_locIdx = ~0ull;
        };

        /// <summary>
        /// Method declaration
        /// </summary>
        struct ZEPHYRLIB_API Method : rtti::Method {
            friend struct LabelRef;
            friend struct CompilationUnit;

        private:
            struct impl;

        protected:
            Method() = default;

        public:

            using rtti::Method::identifier;
            using rtti::Method::argc;
            using rtti::Method::argv;
            using rtti::Method::ret;

            /// <summary>
            /// Sets identifier for this method
            /// </summary>
            /// <param name="identifier">new identifier</param>
            virtual void identifier(const std::string & identifier) = 0;

            /// <summary>
            /// Sets arguments for this method
            /// </summary>
            /// <param name="argTypes">argument types for this method</param>
            virtual void args(const std::vector<std::shared_ptr<const rtti::Type>> & argTypes) = 0;

            /// <summary>
            /// Sets argument count for this method
            /// </summary>
            /// <param name="count">argument count</param>
            virtual void argc(usize count) = 0;

            /// <summary>
            /// Sets argument type at index
            /// </summary>
            /// <param name="index">argument index</param>
            /// <param name="argType">argument type</param>
            virtual void argv(usize index, const std::shared_ptr<const rtti::Type> argType) = 0;

            /// <summary>
            /// Sets return type for this method
            /// </summary>
            /// <param name="retType">return type for this method</param>
            virtual void ret(std::shared_ptr<const rtti::Type> retType) = 0;

            /// <summary>
            /// Emit opcode
            /// </summary>
            /// <param name="code"><see cref="rtti::OpCode"/> to emit</param>
            virtual void emit(OpCode code) = 0;

            /// <summary>
            /// Declare label
            /// </summary>
            /// <returns><see cref="LabelRef"/> object</returns>
            virtual LabelRef label() = 0;

            /// <summary>
            /// Declare local
            /// </summary>
            /// <param name="type">local type</param>
            /// <returns><see cref="LocalRef"/> object</returns>
            virtual LocalRef local(std::shared_ptr<const rtti::Type> type) = 0;

        };

    }
}

#endif
