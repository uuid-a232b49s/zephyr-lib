#ifndef HEADER_ZEPHYR_COMPILATION_DATAPROVIDER
#define HEADER_ZEPHYR_COMPILATION_DATAPROVIDER 1

#include <map>
#include <memory>
#include <string>
#include <typeinfo>
#include <typeindex>
#include <functional>

#include "../exceptions.hpp"

#include "zephyrlib-so-api.hxx"

namespace zephyr {
    namespace rtti {
        struct RttiDescriptor;
        struct Value;
        struct Module;
        struct LateBoundFn;
    }
}

namespace zephyr {
    namespace compilation {

        /// <summary>
        /// Base data provider
        /// <para>
        /// By default provides definitions for primitive numeric types and void
        /// </para>
        /// </summary>
        struct ZEPHYRLIB_API DataProvider {
            virtual ~DataProvider() = default;

            virtual const std::type_info & typeinfo(const std::string & identifier) const;
            virtual std::string identifier(const std::type_info & typeinfo) const;
            virtual std::shared_ptr<const rtti::RttiDescriptor> rtti(const std::type_info & typeinfo) const;

            virtual std::unique_ptr<rtti::Value> value(const std::type_info & typeinfo) const;

            virtual std::shared_ptr<const rtti::LateBoundFn> function(const std::string & identifier) const;
            virtual std::string identifier(const std::shared_ptr<const rtti::LateBoundFn> & function) const;
        };

        struct ZEPHYRLIB_API MappedDataProvider : DataProvider {
        protected:

            struct data_t {
                const std::string * m_identifier;
                std::shared_ptr<const rtti::RttiDescriptor> m_descriptor;
                std::function<std::unique_ptr<rtti::Value>(void)> m_newValue;
            };

            std::map<std::string, const std::type_info *> m_idToTi;
            std::map<std::type_index, data_t> m_tiToData;
            
            std::map<std::shared_ptr<const rtti::LateBoundFn>, const std::string *> m_fnToId;
            std::map<std::string, const std::shared_ptr<const rtti::LateBoundFn> *> m_IdToFn;

        public:
            virtual const std::type_info & typeinfo(const std::string & identifier) const override;
            virtual std::string identifier(const std::type_info & typeinfo) const override;
            virtual std::shared_ptr<const rtti::RttiDescriptor> rtti(const std::type_info & typeinfo) const override;
            virtual std::unique_ptr<rtti::Value> value(const std::type_info & typeinfo) const override;
            virtual std::shared_ptr<const rtti::LateBoundFn> function(const std::string & identifier) const override;
            virtual std::string identifier(const std::shared_ptr<const rtti::LateBoundFn> & function) const override;

            void registerType(
                std::string identifier,
                const std::type_info & typeinfo,
                std::shared_ptr<const rtti::RttiDescriptor> descriptor,
                std::function<std::unique_ptr<rtti::Value>(void)> newValue
            );

            const std::shared_ptr<const rtti::LateBoundFn> & registerFunction(std::string identifier, std::shared_ptr<const rtti::LateBoundFn> fn);

        };

        struct ZEPHYRLIB_API ModuleProvider {
            virtual ~ModuleProvider() = default;
            virtual std::shared_ptr<const rtti::Module> find(const std::string & identifier) const { throw Exception("module not found"); }
        };

        struct ZEPHYRLIB_API MappedModuleProvider : ModuleProvider {
            std::map<std::string, std::shared_ptr<const rtti::Module>> map;
            virtual std::shared_ptr<const rtti::Module> find(const std::string & identifier) const override;
        };

    }
}

#endif
