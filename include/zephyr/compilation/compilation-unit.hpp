#ifndef HEADER_ZEPHYR_COMPILATION_TRANSACTION
#define HEADER_ZEPHYR_COMPILATION_TRANSACTION 1

/// @file
/// @brief Compilation driver

#include <memory>
#include <vector>
#include <string>
#include <utility>
#include <istream>
#include <ostream>
#include <functional>

#include "../rtti/descriptor.hpp"

#include "opcode.hpp"
#include "dataprovider.hpp"

#include "zephyrlib-so-api.hxx"

namespace zephyr {
    namespace rtti {
        struct Module;
    }
    namespace compilation {
        struct Method;
        struct NativeType;
    }
}

namespace zephyr {
    namespace compilation {

        /// <summary>
        /// Represents a concrete unit that is to be compiled into an rtti::Module
        /// <para>
        /// Each unit defines its own resources and can reference other modules
        /// </para>
        /// </summary>
        struct ZEPHYRLIB_API CompilationUnit {
        private:
            struct impl;
            std::vector<std::shared_ptr<NativeType>> m_nativeTypes;
            std::vector<std::shared_ptr<Method>> m_methods;

            std::shared_ptr<NativeType> declareNativeType0(std::shared_ptr<const rtti::RttiDescriptor>);

        public:

            /// <summary>
            /// Identifier for this unit
            /// <para>
            /// Compiled module will inherit this property
            /// </para>
            /// </summary>
            std::string identifier;

            /// <summary>
            /// Auxiliary modules
            /// <para>
            /// Used for retrieving indirectly referenced native types (std::type_info* -> NativeType)
            /// </para>
            /// </summary>
            std::vector<std::shared_ptr<const rtti::Module>> referencedModules;

            CompilationUnit();
            CompilationUnit(CompilationUnit &&) noexcept;
            ~CompilationUnit();
            CompilationUnit(const CompilationUnit &) = delete;

            /// <summary>
            /// Declare a native type using an explicit rtti
            /// </summary>
            /// <param name="rtti">rtti descriptor</param>
            /// <returns>Declared native type pointer</returns>
            [[deprecated]]
            std::shared_ptr<NativeType> declareNativeType(std::shared_ptr<const rtti::RttiDescriptor> rtti) { return declareNativeType0(std::move(rtti)); }

            /// <summary>
            /// Declare a native type using templated argument
            /// </summary>
            /// <typeparam name="x">type to bind</typeparam>
            /// <returns>Declared native type pointer</returns>
            template<typename x>
            std::shared_ptr<NativeType> declareNativeType() { return declareNativeType0(rtti::RttiDescriptorGeneric<x>::Instance()); }

            /// <summary>
            /// Declare a managed method
            /// </summary>
            /// <returns>Declared managed method pointer</returns>
            std::shared_ptr<Method> declareMethod();

            /// <summary>
            /// Compile this unit
            /// </summary>
            /// <param name="threadCount">how many threads to use during compilation (0 = all available, 1 = current, 2+ = exact count)</param>
            /// <returns>Compiled module</returns>
            /// <exception cref="CompilationException">when compilation engine encounters an error</exception>
            std::shared_ptr<const rtti::Module> compile(unsigned int threadCount = 1) const;

            /// <summary>
            /// Serializes this module
            /// </summary>
            /// <param name="out">serialization stream</param>
            void serialize(std::ostream & out, const DataProvider & infoProvider = DataProvider(), bool binary = true) const;

            /// <summary>
            /// Deserializes into this module
            /// </summary>
            /// <param name="in">deserialization stream</param>
            /// <param name="references">reference modules to use</param>
            /// <param name="data">data provider</param>
            void deserialize(
                std::istream & in,
                const DataProvider & dataProvider = DataProvider(),
                const ModuleProvider & moduleProvider = ModuleProvider(),
                bool binary = true
            );

        };

    }
}

#endif
