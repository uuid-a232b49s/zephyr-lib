#ifndef HEADER_ZEPHYR_DEFINITION
#define HEADER_ZEPHYR_DEFINITION 1

/// @file
/// @brief Common definitions

namespace zephyr {

    /// <summary>
    /// Unsigned size type
    /// </summary>
    using usize = unsigned long long;

    /// <summary>
    /// Signed size type
    /// </summary>
    using isize = signed long long;

}

/// <summary>
/// Declares the 5 except 0-arg ctor as default
/// <param name="t">declaring type</param>
/// </summary>
#define ZEPHYR_DECLARE_DEFAULT_CTORS_4(t) \
t(const t &) = default; \
t(t &&) = default; \
t & operator=(const t &) = default; \
t & operator=(t &&) = default;

/// <summary>
/// Declares the 5 as default
/// <param name="t">declaring type</param>
/// </summary>
#define ZEPHYR_DECLARE_DEFAULT_CTORS_5(t) \
t() = default; \
ZEPHYR_DECLARE_DEFAULT_CTORS_4(t)

/// <summary>
/// Declares the 5 except 0-arg ctor as deleted
/// <param name="t">declaring type</param>
/// </summary>
#define ZEPHYR_DECLARE_DELETED_CTORS_4(t) \
t(const t &) = delete; \
t(t &&) = delete; \
t & operator=(const t &) = delete; \
t & operator=(t &&) = delete;

#endif
