#if !defined(HEADER_ZEPHYR_RUNTIME_EXECUTION) | defined(HEADER_ZEPHYR_RUNTIME_EXECUTION_DETAILS)
#error illegal include
#endif
#define HEADER_ZEPHYR_RUNTIME_EXECUTION_DETAILS 1

#include <memory>
#include <utility>
#include <typeinfo>
#include <type_traits>

#include "../def.hpp"
#include "../rtti/absfn.hpp"

namespace zephyr {
    namespace runtime {
        namespace details {

            static_assert(sizeof(unsigned char) == 1, "unexpected sizeof(unsigned char)");

            template<usize...>
            struct add;
            template<usize I, usize J, usize ... K>
            struct add<I, J, K...> : add<I + J, K...> {};
            template<usize I>
            struct add<I> { static constexpr usize value = I; };

            template<typename>
            struct sigRetType;
            template<typename ret_t_, typename ... args_t_>
            struct sigRetType<ret_t_(args_t_...)> :
                std::enable_if<
                add<std::is_reference<ret_t_>::value, std::is_reference<args_t_>::value...>::value == 0,
                ret_t_
            > {};

            template<typename>
            struct invoker;
            template<typename ret_t_, typename ... args_t_>
            struct invoker<ret_t_(args_t_...)> {
                template<typename ctx_t>
                static ret_t_ invoke(ctx_t * ctx, std::shared_ptr<const rtti::Method> & m, args_t_ ... args) {
                    {
                        static const std::type_info * const argTypes[] { (&typeid(args_t_))..., &typeid(ret_t_) };
                        ctx->verifyMethodSignature(m, argTypes, sizeof...(args_t_));
                    }
                    {
                        typename std::aligned_storage<sizeof(ret_t_), alignof(ret_t_)>::type ret;
                        {
                            isize offsets[sizeof...(args_t_) + 1] {
                                (reinterpret_cast<unsigned char *>(&args) - reinterpret_cast<unsigned char *>(0))...,
                                (reinterpret_cast<unsigned char *>(&ret) - reinterpret_cast<unsigned char *>(0))
                            };
                            ctx->execute0(std::move(m), nullptr, offsets);
                        }
                        {
                            ret_t_ ret0 { std::move(reinterpret_cast<ret_t_ &>(ret)) };
                            reinterpret_cast<ret_t_ *>(&ret)->~ret_t_();
                            return ret0;
                        }
                    }
                }
            };
            template<typename ... args_t_>
            struct invoker<void(args_t_...)> {
                template<typename ctx_t>
                static void invoke(ctx_t * ctx, std::shared_ptr<const rtti::Method> & m, args_t_ ... args) {
                    {
                        static const std::type_info * const argTypes[] { (&typeid(args_t_))..., &typeid(void) };
                        ctx->verifyMethodSignature(m, argTypes, sizeof...(args_t_));
                    }
                    {
                        isize offsets[sizeof...(args_t_)] {
                            (reinterpret_cast<unsigned char *>(&args) - reinterpret_cast<unsigned char *>(0))...
                        };
                        ctx->execute0(std::move(m), nullptr, offsets);
                    }
                }
            };
            template<>
            struct invoker<void()> {
                template<typename ctx_t>
                static void invoke(ctx_t * ctx, std::shared_ptr<const rtti::Method> & m) {
                    {
                        static const std::type_info * const argTypes[] { &typeid(void) };
                        ctx->verifyMethodSignature(m, argTypes, 0);
                    }
                    ctx->execute0(std::move(m), nullptr, nullptr);
                }
            };

        }
    }
}
