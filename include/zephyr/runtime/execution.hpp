#ifndef HEADER_ZEPHYR_RUNTIME_EXECUTION
#define HEADER_ZEPHYR_RUNTIME_EXECUTION 1

/// @file
/// @brief Execution interface

#include <memory>
#include <vector>
#include <utility>

#include "../def.hpp"
#include "../rtti/absfn.hpp"

#include "zephyrlib-so-api.hxx"

namespace zephyr {
    namespace compilation {
        struct CompilationUnit;
    }
    namespace rtti {
        struct Value;
        struct Method;
    }
}

#include "execution.details.inl"

namespace zephyr {
    namespace runtime {

        /// <summary>
        /// Execution context
        /// <para>
        /// Infrastructure for compiled method execution
        /// </para>
        /// </summary>
        struct ZEPHYRLIB_API ExecutionContext {
            friend struct compilation::CompilationUnit;
            template<typename> friend struct runtime::details::invoker;

        private:
            std::vector<std::unique_ptr<std::vector<unsigned char>>> m_memory;
            isize m_currentPage = -1;
            unsigned char * m_stack = nullptr;
            const void * m_instruction = nullptr;

            static void verifyMethodSignature(const std::shared_ptr<const rtti::Method> &, const std::type_info * const *, usize);
            void execute0(std::shared_ptr<const rtti::Method>, void *, isize *);
            void allocateStackPage(usize);
            void deallocateStackPage(usize);

        public:

            /// <summary>
            /// Minimal size for each stack page
            /// <para>
            /// Default: 2kB
            /// </para>
            /// </summary>
            usize pageSize = 1 << 11;

            /// <summary>
            /// Maximum stack memory
            /// <para>
            /// Default: 8MB
            /// </para>
            /// </summary>
            usize maxStackMemory = 1 << 23;

            ExecutionContext();
            ~ExecutionContext();
            ExecutionContext(const ExecutionContext &) = delete;
            ExecutionContext(ExecutionContext &&) = delete;

            /// <summary>
            /// Ctti execution
            /// </summary>
            /// <typeparam name="signature_t">invocation signature, must correspond to the invoked method</typeparam>
            /// <typeparam name="...args_t">supplied argument types</typeparam>
            /// <param name="method">target method to invoke</param>
            /// <param name="...args">arguments</param>
            /// <returns>Value corresponding to the method return type</returns>
            /// <exception cref="IllegalArgumentException">when method signature doesnt correspond to the specified signature</exception>
            template<typename signature_t, typename ... args_t>
            auto execute(std::shared_ptr<const rtti::Method> method, args_t ... args) -> typename details::sigRetType<signature_t>::type {
                return details::invoker<signature_t>::invoke(this, method, std::move(args)...);
            }

            /// <summary>
            /// Rtti execution
            /// </summary>
            /// <param name="method">target method to invoke</param>
            /// <param name="ret">return value</param>
            /// <param name="args">arguments</param>
            /// <param name="argCount">argument count to use, or -1 (default) to use the same count as the <c>args</c> parameter</param>
            void executeDynamic(std::shared_ptr<const rtti::Method> method, const std::shared_ptr<rtti::Value> & ret, const std::vector<std::shared_ptr<rtti::Value>> & args, isize argCount = -1);

        };

    }
}

#endif
