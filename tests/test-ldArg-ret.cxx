#include <cassert>

#include "zephyr.hpp"

namespace n_rtti = zephyr::rtti;

int main(int, char **) {
    using namespace zephyr;

    rtti::ModulePtr module;

    {
        compilation::CompilationUnit t;

        auto n = t.declareNativeType<unsigned long long>();

        auto m = t.declareMethod();
        {
            m->identifier("main");
            m->args({ n });
            m->ret(n);
            m->emit(compilation::OpCode::ldArg(0));
            m->emit(compilation::OpCode::ret());
        }

        auto compilaitonResult = t.compile();
        module = std::move(compilaitonResult);
    }

    runtime::ExecutionContext ctx;
    {
        auto ret = ctx.execute<usize(usize)>(module->find<rtti::Method>("main"), 7);
        assert(ret == 7);
    }
    {
        try {
            ctx.execute<void(usize, usize, usize)>(module->find<rtti::Method>("main"), 0, 0, 0);
        } catch (zephyr::IllegalArgumentException &) {
            goto lbl_ok;
        }
        return -1;
    lbl_ok:;
    }

}
