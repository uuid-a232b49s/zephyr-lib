#include <cassert>

#include "zephyr.hpp"

//struct B { B(int) {} };

int main(int, char **) {
    using namespace zephyr;

    rtti::ModulePtr module;

    //using C = rtti::ValueImpl<B>;

    {
        compilation::CompilationUnit t;
        using compilation::OpCode;

        auto n = t.declareNativeType<usize>();

        assert(dynamic_cast<rtti::Symbol *>(dynamic_cast<compilation::Type *>(n.get())) == dynamic_cast<rtti::Symbol *>(dynamic_cast<rtti::NativeType *>(n.get())));

        auto m = t.declareMethod();
        {
            m->identifier("main");
            m->args({ n, n, n });
            m->ret(n);

            auto lbl0 = m->label();

            /* 0 */ m->emit(OpCode::ldArg(0));
            /* 1 */ m->emit(OpCode::branch(lbl0, false));

            /* 2 */ m->emit(OpCode::ldArg(1));
            /* 3 */ m->emit(OpCode::ret());

            lbl0.mark();
            /* 4 */ m->emit(OpCode::ldArg(2));
            /* 5 */ m->emit(OpCode::ret());
        }

        auto compilaitonResult = t.compile();
        module = std::move(compilaitonResult);
    }

    runtime::ExecutionContext ctx;
    {
        auto ret = ctx.execute<usize(usize, usize, usize)>(module->find<rtti::Method>("main"), 1, 10, 20);
        assert(ret == 10);
    }
    {
        auto ret = ctx.execute<usize(usize, usize, usize)>(module->find<rtti::Method>("main"), 0, 10, 20);
        assert(ret == 20);
    }

}
