#include <cassert>

#include "zephyr.hpp"

int main(int, char **) {
    using namespace zephyr;

    rtti::ModulePtr module;

    {
        compilation::CompilationUnit t;

        auto n = t.declareNativeType<usize>();

        auto m = t.declareMethod();
        {
            m->identifier("main");
            m->ret(n);

            m->emit(compilation::OpCode::ldConst(rtti::BindValueShared<usize>(6)));
            m->emit(compilation::OpCode::ret());
        }

        auto compilationResult = t.compile();
        module = std::move(compilationResult);
    }

    runtime::ExecutionContext ctx;
    {
        auto ret = ctx.execute<usize()>(module->find<rtti::Method>("main"));
        assert(ret == 6);
    }

}
