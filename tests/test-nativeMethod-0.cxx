#include <cassert>

#include "zephyr.hpp"

static void test_0() {
    using namespace zephyr;

    auto boundFn = rtti::BindFnShared<usize(usize)>([] (usize x) -> usize { return x * 2 + 1; });
    assert(*boundFn->ret() == typeid(usize));
    assert(boundFn->args().size() == 1);
    assert(*boundFn->args()[0] == typeid(usize));

    usize A = 10;
    usize ret = 0;
    isize args[] = {
        reinterpret_cast<isize>(reinterpret_cast<void *>(&A)) - reinterpret_cast<isize>(reinterpret_cast<void *>(&A)),
        reinterpret_cast<isize>(reinterpret_cast<void *>(&ret)) - reinterpret_cast<isize>(reinterpret_cast<void *>(&A)),
    };
    boundFn->invoke(reinterpret_cast<void *>(&A), args);

    assert(ret == (10 * 2 + 1));
}

static void test_1() {
    using namespace zephyr;

    rtti::ModulePtr module;

    {
        using compilation::OpCode;
        compilation::CompilationUnit t;

        auto n = t.declareNativeType<usize>();

        auto m = t.declareMethod();
        {
            m->identifier("main");
            m->args({ n });
            m->ret(n);

            m->emit(OpCode::ldArg(0));
            m->emit(OpCode::invoke(rtti::BindFnShared<usize(usize)>([] (usize x) -> usize {
                assert(x == 7);
                return x + 1;
            })));
            m->emit(OpCode::ret());
        }

        module = t.compile();
    }

    runtime::ExecutionContext ctx;
    {
        auto ret = ctx.execute<usize(usize)>(module->find<rtti::Method>("main"), 7);
        assert(ret == 8);
    }
}

static void test_2() {
    using namespace zephyr;

    rtti::ModulePtr module;

    {
        using compilation::OpCode;
        compilation::CompilationUnit t;

        auto n = t.declareNativeType<int>();

        auto m = t.declareMethod();
        {
            m->identifier("main");
            m->args({ n });
            m->ret(n);

            m->emit(OpCode::invoke(rtti::BindFnShared<int()>([] () -> int { return 5; })));
            m->emit(OpCode::ret());
        }

        module = t.compile();
    }

    runtime::ExecutionContext ctx;
    {
        auto ret = ctx.execute<int(int)>(module->find<rtti::Method>("main"), -1);
        assert(ret == 5);
    }
}

static void test_3() {
    using namespace zephyr;

    int invoked = 0;

    rtti::ModulePtr module;

    {
        using compilation::OpCode;
        compilation::CompilationUnit t;

        auto n = t.declareNativeType<int>();

        {
            auto m = t.declareMethod();
            m->identifier("m0");

            m->emit(OpCode::invoke(rtti::BindFnShared<void()>([&] () -> void { invoked++; })));
            m->emit(OpCode::ret());
        }

        {
            auto m = t.declareMethod();
            m->identifier("m1");
            m->args({ n });

            m->emit(OpCode::ldArg(0));
            m->emit(OpCode::invoke(rtti::BindFnShared<void(int)>([&] (int a) -> void { invoked = a; })));
            m->emit(OpCode::ret());
        }

        module = t.compile();
    }

    {
        runtime::ExecutionContext ctx;
        ctx.execute<void()>(module->find<rtti::Method>("m0"));
        assert(invoked == 1);
    }

    {
        runtime::ExecutionContext ctx;
        ctx.execute<void(int)>(module->find<rtti::Method>("m1"), -3);
        assert(invoked == -3);
    }
}

int main(int, char **) {
    test_0();
    test_1();
    test_2();
    test_3();
}
