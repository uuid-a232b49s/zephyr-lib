#include <cassert>
#include <string>

#include "zephyr/../../src/crc.hxx"

int main(int, char **) {
    using namespace zephyr::impl;

    {
        std::string data[] { { "The quick brown fox " }, { "jumps over the lazy dog" } };
        crc32_t v = 0;
        for (auto & d : data) { v = crc32(d.begin(), d.end(), v); }
        assert(v == 0x414fa339);
    }
}
