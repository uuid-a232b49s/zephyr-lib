#include <limits>
#include <cassert>
#include <sstream>

#include "zephyr/../../src/crc.hxx"
#include "zephyr/../../src/primitive_serialization.hxx"

int main(int, char **) {

#define testIO(mRead, mWrite, ...) \
    do { \
        using namespace zephyr::impl; \
        zephyr::impl::crc32_t crcR { 0 }, crcW { 0 }; \
        for (decltype(mRead(*static_cast<std::stringstream *>(nullptr), crcR)) value : { __VA_ARGS__ }) { \
            std::stringstream ss; \
            mWrite(ss, value, crcW); \
            writeCrc(ss, crcW); \
            auto result = mRead(ss, crcR); \
            assert(result == value && (#mRead)); \
            assert(checkCrc(ss, crcR) && (#mRead)); \
        } \
    } while (false)

    testIO(readUInt8, writeUInt8, 0);
    testIO(readUInt16, writeUInt16, 0x0, 0x1234, 0x4321, 0xffff);
    testIO(readUInt32, writeUInt32, 0x0UL, 0x12345678UL, 0x87654321UL, 0xffffffffUL);
    testIO(readUInt64, writeUInt64, 0x0ULL, 0x1234567812345678ULL, 0x8765432187654321ULL, 0xffffffffffffffffULL);

    testIO(readUInt8, writeUInt8, 0x0, -1, -0x7f, 0x12, -0x21);
    testIO(readInt16, writeInt16, 0x0, -1, -0x7fff, 0x1234, -0x4321);
    testIO(readInt32, writeInt32, 0x0L, -1L, 0x7fffffffL, 0x12345678L, -0x8765432L);
    testIO(readInt64, writeInt64, 0x0LL, -1LL, 0x7fffffffffffffffLL, 0x1234567812345678LL, 0x876543218765432LL);

    testIO(readFlt32, writeFlt32, 0.0f, 0.5f, -0.5f, 98765.4321f, -98765.4321f, std::numeric_limits<float>::max(), std::numeric_limits<float>::min());
    testIO(readFlt64, writeFlt64, 0.0, 0.5, -0.5, 98765.4321, -98765.4321, std::numeric_limits<double>::max(), std::numeric_limits<double>::min());

    testIO(readString, writeString, "a", "ab", "a b c");

    return 0;
}
