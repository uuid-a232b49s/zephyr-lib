#include <cassert>

#include "zephyr.hpp"

namespace n_rtti = zephyr::rtti;

using ull = unsigned long long;

int main(int, char **) {
    using namespace zephyr;

    rtti::ModulePtr module;

    {
        compilation::CompilationUnit t;
        using compilation::OpCode;

        auto n = t.declareNativeType<ull>();

        auto m = t.declareMethod();
        {
            m->identifier("main");
            m->args({ n, n });
            m->ret(n);

            auto l0 = m->local(n);

            /* 0 */ m->emit(OpCode::ldArg(0));
            /* 1 */ m->emit(OpCode::stLoc(l0));
            /* 2 */ m->emit(OpCode::ldLoc(l0, OpCode::LoadMode::move));
            /* 3 */ m->emit(OpCode::ret());
        }

        auto compilaitonResult = t.compile();
        module = std::move(compilaitonResult);
    }

    runtime::ExecutionContext ctx;
    {
        auto ret = ctx.execute<ull(ull, ull)>(module->find<rtti::Method>("main"), 7, 8);
        assert(ret == 7);
    }

    return 0;

}
