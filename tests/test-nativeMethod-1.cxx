#include <array>
#include <cassert>

#include "zephyr.hpp"

int main(int, char **) {
    using namespace zephyr;

    using usize4 = std::array<usize, 4>;

    rtti::ModulePtr module;

    {
        using compilation::OpCode;
        compilation::CompilationUnit t;

        auto n = t.declareNativeType<usize>();
        auto na = t.declareNativeType<usize4>();

        auto m = t.declareMethod();
        {
            m->identifier("main");
            m->args({ n });
            m->ret(na);

            m->emit(OpCode::ldArg(0));
            m->emit(OpCode::invoke(rtti::BindFnShared<usize4(usize)>([] (usize x) -> usize4 {
                assert(x == 7);
                return { x * 1, x * 2, x * 3, x * 4 };
            })));
            m->emit(OpCode::ret());
        }

        module = t.compile();
    }

    runtime::ExecutionContext ctx;
    {
        auto expected = usize4{ 7 * 1, 7 * 2, 7 * 3, 7 * 4 };
        auto ret = ctx.execute<usize4(usize)>(module->find<rtti::Method>("main"), 7);
        assert(ret == expected);
    }

}
