#include <cassert>
#include <cstddef>
#include <vector>

#include "zephyr/../../src/structLayout.hxx"

int main(int, char **) {
    using ull = zephyr::ULL;

    struct typeMock {
        ull m_size, m_align;
        ull size() const { return m_size; }
        ull align() const { return m_align; }
        const typeMock & operator*() const { return *this; }
    };

    std::vector<typeMock> fields;
    std::vector<ull> offsets;
    ull size, align;

    {
        struct s {
            short a;
            int b;
        };

        fields.emplace_back(typeMock{ sizeof(decltype(((s *)0)->a)), alignof(decltype(((s *)0)->a)) });
        fields.emplace_back(typeMock{ sizeof(decltype(((s *)0)->b)), alignof(decltype(((s *)0)->b)) });
        offsets.resize(fields.size());

        zephyr::impl::layoutStruct(fields.begin(), fields.end(), align, size, offsets.begin());

        assert(size == sizeof(s));
        assert(align == alignof(s));
        assert(offsets[0] == offsetof(s, a));
        assert(offsets[1] == offsetof(s, b));

        fields.clear();
    }

    {
        struct s {
            short a;
            int b;
            ull c;
            char d;
        };

        fields.emplace_back(typeMock{ sizeof(decltype(s::a)), alignof(decltype(s::a)) });
        fields.emplace_back(typeMock{ sizeof(decltype(s::b)), alignof(decltype(s::b)) });
        fields.emplace_back(typeMock{ sizeof(decltype(s::c)), alignof(decltype(s::c)) });
        fields.emplace_back(typeMock{ sizeof(decltype(s::d)), alignof(decltype(s::d)) });
        offsets.resize(fields.size());

        zephyr::impl::layoutStruct(fields.begin(), fields.end(), align, size, offsets.begin());

        assert(size == sizeof(s));
        assert(align == alignof(s));
        assert(offsets[0] == offsetof(s, a));
        assert(offsets[1] == offsetof(s, b));
        assert(offsets[2] == offsetof(s, c));
        assert(offsets[3] == offsetof(s, d));

        fields.clear();
    }

    return 0;
}
