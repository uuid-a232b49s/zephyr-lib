#include <cassert>

#include "zephyr.hpp"

int main(int, char **) {
    using namespace zephyr;

    rtti::ModulePtr module;
    using INT = signed short;

    static int counter = 0;

    {
        using compilation::OpCode;
        compilation::CompilationUnit t;

        auto n = t.declareNativeType<INT>();
        auto b = t.declareNativeType<bool>();

        auto m0 = t.declareMethod();
        {
            m0->identifier("m0");
            m0->args({ n });
            m0->ret(n);
            auto lbl_0 = m0->label();
            m0->emit(OpCode::invoke(rtti::BindFnShared<void(void)>([] { counter++; })));
            m0->emit(OpCode::ldArg(0));
            m0->emit(OpCode::invoke(rtti::BindFnShared<bool(INT)>([] (INT a) -> bool { return a <= 0; })));
            m0->emit(OpCode::branch(lbl_0, true));
            m0->emit(OpCode::ldArg(0));
            m0->emit(OpCode::invoke(rtti::BindFnShared<INT(INT)>([] (INT a) -> INT { return a - 1; })));
            m0->emit(OpCode::invoke(m0));
            m0->emit(OpCode::ret());
            lbl_0.mark();
            m0->emit(OpCode::ldConst(rtti::BindValueShared<INT>(5)));
            m0->emit(OpCode::ret());
        }

        auto m1 = t.declareMethod();
        {
            m1->identifier("m1");
            m1->args({ n });
            m1->ret(n);
            m1->emit(OpCode::ldArg(0));
            m1->emit(OpCode::invoke(m0));
            m1->emit(OpCode::ret());
        }

        module = t.compile();
    }

    {
        auto m1 = module->find<rtti::Method>("m1");
        runtime::ExecutionContext ctx;
        //ctx.pageSize = m1->maxStackSize();
        auto ret = ctx.execute<INT(INT)>(m1, 63);
        assert(ret == 5);
        assert(counter == 64);
    }
}
