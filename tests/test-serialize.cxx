#include <cassert>
#include <sstream>
#include <iostream>

#include "zephyr.hpp"

int main(int, char **) {
    using namespace zephyr;

    auto fn = [] (int a, int b) -> int {
        int v = a;
        for (int i = 0; i < b; ++i) {
            v = (v * (i + 1)) - i;
        }
        return v;
    };

    compilation::MappedDataProvider infoProvider;

    auto c_int_0 = rtti::BindValueShared<int>(0);
    auto c_int_1 = rtti::BindValueShared<int>(1);

    auto & fn_less = infoProvider.registerFunction("<", rtti::BindFnShared<bool(int, int)>([] (int a, int b) -> int { return a < b; }));
    auto & fn_add = infoProvider.registerFunction("+", rtti::BindFnShared<int(int, int)>([] (int a, int b) -> int { return a + b; }));
    auto & fn_sub = infoProvider.registerFunction("-", rtti::BindFnShared<int(int, int)>([] (int a, int b) -> int { return a - b; }));
    auto & fn_mul = infoProvider.registerFunction("*", rtti::BindFnShared<int(int, int)>([] (int a, int b) -> int { return a * b; }));

    std::stringstream data;
    {
        rtti::ModulePtr module;
        {
            compilation::CompilationUnit cu;

            auto t_int = cu.declareNativeType<int>();
            t_int->identifier("int");
            t_int->identifier();

            auto t_bool = cu.declareNativeType<bool>();
            t_bool->identifier("bool");

            auto m = cu.declareMethod();
            {
                using compilation::OpCode;
                m->identifier("m");
                m->args({ t_int, t_int });
                m->ret(t_int);

                auto loc_i = m->local(t_int);
                auto loc_v = m->local(t_int);

                auto lbl_0 = m->label();
                auto lbl_1 = m->label();

                /* 00 */ m->emit(OpCode::ldArg(0));
                /* 01 */ m->emit(OpCode::stLoc(loc_v));
                /* 02 */ m->emit(OpCode::ldConst(c_int_0));
                /* 03 */ m->emit(OpCode::stLoc(loc_i));
                lbl_0.mark();
                /* 04 */ m->emit(OpCode::ldLoc(loc_i));
                /* 05 */ m->emit(OpCode::ldArg(1));
                /* 06 */ m->emit(OpCode::invoke(fn_less));
                /* 07 */ m->emit(OpCode::branch(lbl_1, false));

                /* 08 */ m->emit(OpCode::ldLoc(loc_i));
                /* 09 */ m->emit(OpCode::ldConst(c_int_1));
                /* 10 */ m->emit(OpCode::invoke(fn_add));
                /* 11 */ m->emit(OpCode::ldLoc(loc_v, OpCode::LoadMode::move));
                /* 12 */ m->emit(OpCode::invoke(fn_mul));
                /* 13 */ m->emit(OpCode::ldLoc(loc_i));
                /* 14 */ m->emit(OpCode::invoke(fn_sub));
                /* 15 */ m->emit(OpCode::stLoc(loc_v));

                /* 16 */ m->emit(OpCode::ldLoc(loc_i));
                /* 17 */ m->emit(OpCode::ldConst(c_int_1));
                /* 18 */ m->emit(OpCode::invoke(fn_add));
                /* 19 */ m->emit(OpCode::stLoc(loc_i));
                /* 20 */ m->emit(OpCode::jmp(lbl_0));
                lbl_1.mark();
                /* 21 */ m->emit(OpCode::clLoc(loc_i));
                /* 22 */ m->emit(OpCode::ldLoc(loc_v, OpCode::LoadMode::move));
                /* 23 */ m->emit(OpCode::ret());
            }

            module = cu.compile();

            {
                runtime::ExecutionContext ctx;
                auto result = ctx.execute<int(int, int)>(module->find<rtti::Method>("m"), 2, 10);
                auto expected = fn(2, 10);
                assert(result == expected);
            }

            cu.serialize(data, infoProvider);
        }
    }

    {
        rtti::ModulePtr m;
        {
            compilation::CompilationUnit cu;
            cu.deserialize(data, infoProvider);
            m = cu.compile();
        }
        {
            runtime::ExecutionContext ctx;
            auto result = ctx.execute<int(int, int)>(m->find<rtti::Method>("m"), 2, 10);
            auto expected = fn(2, 10);
            assert(result == expected);
        }
    }

}
