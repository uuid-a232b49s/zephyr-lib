#include <cassert>

#include "zephyr.hpp"

int main(int, char **) {
    using namespace zephyr;

    rtti::ModulePtr module;

    static int INT_instanceCount = 0;

    struct INT {
        int value = 0;
        INT() { INT_instanceCount++; }
        INT(const INT & m) : INT() { value = m.value; }
        INT(INT && m) : INT() { value = m.value; }
        ~INT() { INT_instanceCount--; }
    };

    {
        using compilation::OpCode;
        compilation::CompilationUnit t;

        auto n = t.declareNativeType<INT>();

        auto m0 = t.declareMethod();
        {
            m0->identifier("m0");
            m0->args({ n });
            m0->ret(n);
            m0->emit(OpCode::ldArg(0));
            m0->emit(OpCode::ret());
        }

        auto m1 = t.declareMethod();
        {
            m1->identifier("m1");
            m1->args({ n });
            m1->ret(n);
            m1->emit(OpCode::ldArg(0));
            m1->emit(OpCode::invoke(rtti::BindFnShared<INT(INT)>([] (INT a) -> INT { return a; })));
            m1->emit(OpCode::invoke(m0));
            m1->emit(OpCode::ret());
        }

        auto m2 = t.declareMethod();
        {
            m2->identifier("m2");
            m2->args({ n });
            m2->ret(n);
            m2->emit(OpCode::ldArg(0));
            m2->emit(OpCode::invoke(m1));
            m2->emit(OpCode::ret());
        }

        module = t.compile();
    }

    {
        runtime::ExecutionContext ctx;
        INT v;
        v.value = -9;
        auto ret = ctx.execute<INT(INT)>(module->find<rtti::Method>("m2"), v);
        assert(ret.value == v.value);
    }

    assert(INT_instanceCount == 0);
}
