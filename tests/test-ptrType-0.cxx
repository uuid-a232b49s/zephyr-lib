#include <cassert>

#include "zephyr.hpp"

int main(int, char **) {
    using namespace zephyr;

    rtti::ModulePtr module;
    using INT = signed short;

    {
        using compilation::OpCode;
        compilation::CompilationUnit t;

        auto n = t.declareNativeType<INT>();

        auto m0 = t.declareMethod();
        {
            m0->identifier("m0");
            m0->args({ n->address() });
            m0->ret(n);
            m0->emit(OpCode::ldArg(0));
            m0->emit(OpCode::deref());
            m0->emit(OpCode::ret());
        }

        module = t.compile();
    }

    runtime::ExecutionContext ctx;
    {
        INT arg = -4321;
        auto ret = ctx.execute<INT(INT *)>(module->find<rtti::Method>("m0"), &arg);
        assert(ret == -4321);
    }
}
