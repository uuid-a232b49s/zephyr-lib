#include <cassert>

#include "zephyr.hpp"

int main(int, char **) {
    using namespace zephyr;

    rtti::ModulePtr module;

    {
        compilation::CompilationUnit t;
        using compilation::OpCode;

        auto type_n = t.declareNativeType<usize>();
        type_n->identifier("usize");

        auto type_b = t.declareNativeType<bool>();
        type_b->identifier("bool");

        auto m = t.declareMethod();
        m->identifier("m");
        m->args({ type_n });
        m->ret(type_n);
        {
            // usize m(usize arg0) {
            //     usize v = 1;
            //     for (usize i = 0; i < arg0; ++i) {
            //         v = v * 2;
            //     }
            //     return v
            // }

            auto lbl_0 = m->label();
            auto lbl_1 = m->label();
            auto loc_i = m->local(type_n);
            auto loc_v = m->local(type_n);

            /* 00 */ m->emit(OpCode::ldConst(rtti::BindValueShared<usize>(1)));
            /* 01 */ m->emit(OpCode::ldConst(rtti::BindValueShared<usize>(0)));
            /* 02 */ m->emit(OpCode::stLoc(loc_i));
            /* 03 */ m->emit(OpCode::stLoc(loc_v));
            lbl_1.mark();
            /* 04 */ m->emit(OpCode::ldLoc(loc_i));
            /* 05 */ m->emit(OpCode::ldArg(0));
            /* 06 */ m->emit(OpCode::invoke(rtti::BindFnShared<bool(usize, usize)>([] (usize a, usize b) -> bool { return a < b; })));
            /* 07 */ m->emit(OpCode::branch(lbl_0, false));
            /* 08 */ m->emit(OpCode::ldLoc(loc_v));
            /* 09 */ m->emit(OpCode::invoke(rtti::BindFnShared<usize(usize)>([] (usize a) -> usize { return a * 2; })));
            /* 10 */ m->emit(OpCode::stLoc(loc_v));
            /* 11 */ m->emit(OpCode::ldLoc(loc_i));
            /* 12 */ m->emit(OpCode::invoke(rtti::BindFnShared<usize(usize)>([] (usize a) -> usize { return a + 1; })));
            /* 13 */ m->emit(OpCode::stLoc(loc_i));
            /* 14 */ m->emit(OpCode::jmp(lbl_1));
            lbl_0.mark();
            /* 15 */ //m->emit(OpCode::ldLoc(loc_i, OpCode::LoadMode::move));
            /* 15 */ //m->emit(OpCode::rem());
            /* 15 */ m->emit(OpCode::clLoc(loc_i));
            /* 16 */ m->emit(OpCode::ldLoc(loc_v, OpCode::LoadMode::move));
            /* 17 */ m->emit(OpCode::ret());
        }

        auto compilaitonResult = t.compile();
        module = std::move(compilaitonResult);
    }
    {
        usize arg0 = 5;
        auto expected = [] (usize arg0) -> usize {
            usize v = 1;
            for (usize i = 0; i < arg0; ++i) {
                v = v * 2;
            }
            return v;
        } (arg0);
        runtime::ExecutionContext ctx;
        auto result = ctx.execute<usize(usize)>(module->find<rtti::Method>("m"), arg0);
        assert(result == expected);
    }
}
