#include <cassert>
#include <map>
#include <cmath>
#include <memory>
#include <vector>
#include <string>
#include <cstring>
#include <algorithm>

#include "zephyr.hpp"

using ldouble = long double;

namespace ast {

    struct Args {
        std::map<std::string, ldouble> vars;
    };

    struct Visitor;

    struct Operator {
        virtual ~Operator() = default;
        virtual int argCount() const = 0;
        virtual void setArg(int arg, std::unique_ptr<Operator>) = 0;
        virtual ldouble compute(const Args &) const = 0;
        virtual void visit(Visitor *) = 0;
    };

    struct BinaryOperator : Operator {
        std::unique_ptr<Operator> args[2];
        virtual int argCount() const override { return 2; }
        virtual void setArg(int arg, std::unique_ptr<Operator> op) override { args[arg] = std::move(op); }
    };

#define impl_a_(identifier, op) \
    struct identifier : BinaryOperator { \
        virtual ldouble compute(const Args & args) const override { return this->args[0]->compute(args) op this->args[1]->compute(args); } \
        virtual void visit(Visitor *) override; \
    }

    impl_a_(Add, +);
    impl_a_(Sub, -);
    impl_a_(Mul, *);
    impl_a_(Div, /);

#undef impl_a_

    struct Pow : BinaryOperator {
        virtual ldouble compute(const Args & args) const override { return std::pow(this->args[0]->compute(args), this->args[1]->compute(args)); }
        virtual void visit(Visitor *) override;
    };

    struct Constant : Operator {
        ldouble m_value = 0;
        virtual int argCount() const override { return 0; }
        virtual void setArg(int, std::unique_ptr<Operator>) override { assert(false && "unsupported"); }
        virtual ldouble compute(const Args &) const override { return m_value; }
        virtual void visit(Visitor *) override;
    };

    struct Variable : Operator {
        std::string m_identifier;
        virtual int argCount() const override { return 0; }
        virtual void setArg(int, std::unique_ptr<Operator>) override { assert(false && "unsupported"); }
        virtual ldouble compute(const Args & args) const override { return args.vars.at(m_identifier); }
        virtual void visit(Visitor *) override;
    };

}

namespace ast {

    struct Visitor {
        virtual ~Visitor() = default;
        virtual void visit(Operator *) {}
        virtual void visit(Add * o) { visit(static_cast<Operator *>(o)); }
        virtual void visit(Sub * o) { visit(static_cast<Operator *>(o)); }
        virtual void visit(Mul * o) { visit(static_cast<Operator *>(o)); }
        virtual void visit(Div * o) { visit(static_cast<Operator *>(o)); }
        virtual void visit(Pow * o) { visit(static_cast<Operator *>(o)); }
        virtual void visit(Constant * o) { visit(static_cast<Operator *>(o)); }
        virtual void visit(Variable * o) { visit(static_cast<Operator *>(o)); }
    };

    void Add::visit(Visitor * v) { v->visit(this); }
    void Sub::visit(Visitor * v) { v->visit(this); }
    void Mul::visit(Visitor * v) { v->visit(this); }
    void Div::visit(Visitor * v) { v->visit(this); }
    void Pow::visit(Visitor * v) { v->visit(this); }
    void Constant::visit(Visitor * v) { v->visit(this); }
    void Variable::visit(Visitor * v) { v->visit(this); }

}

static std::unique_ptr<ast::Operator> parseExpression(const char * data) {
    using cstr = const char *;

    auto isWs = [] (char x) {
        switch (x) {
            case ' ':
                return true;
            default: return false;
        }
    };

    auto skipWs = [&] () { for (; *data != 0 && isWs(*data); data++); };

    auto nextToken = [&] (cstr & start, cstr & end) {
        skipWs();
        start = data;
        if (*data == 0) { end = data; return; }
        if (*data == '-' || std::isdigit(*data)) {
            data++;
            for (; *data != 0 && !isWs(*data) && std::isdigit(*data); data++);
            if (*data != 0 && *data == '.') {
                data++;
                for (; *data != 0 && !isWs(*data) && std::isdigit(*data); data++);
            }
        } else if (std::isalpha(*data)) {
            data++;
            for (; *data != 0 && !isWs(*data) && std::isalpha(*data); data++);
        } else if (*data == '(' || *data == ')' || *data == ',') { data++; }
        end = data;
    };

    {
        std::vector<std::unique_ptr<ast::Operator>> ops;
        std::vector<int> currentOp;
        enum struct State { readOp0 = 1000, readOp1, readOp2, argSep };
        std::vector<State> state;
        cstr a, b;

        auto nullToken = [&] () { return a == b; };
        auto tokenEq = [&] (cstr s) { return std::strncmp(a, s, b - a) == 0; };

        state.emplace_back(State::readOp0);

        while (!state.empty()) {
            auto s = state.back();
            state.pop_back();
            switch (s) {
                case State::readOp0:
                    nextToken(a, b);
                    if (nullToken()) { assert(false && "illegal token: expected expression"); }
                    if (tokenEq("add")) {
                        ops.emplace_back(new ast::Add());
                        goto lbl_binary;
                    } else if (tokenEq("sub")) {
                        ops.emplace_back(new ast::Sub());
                        goto lbl_binary;
                    } else if (tokenEq("mul")) {
                        ops.emplace_back(new ast::Mul());
                        goto lbl_binary;
                    } else if (tokenEq("div")) {
                        ops.emplace_back(new ast::Div());
                        goto lbl_binary;
                    } else if (tokenEq("pow")) {
                        ops.emplace_back(new ast::Pow());
                        goto lbl_binary;
                    } else {
                        char * e;
                        auto v = std::strtold(a, &e);
                        if (e == b) {
                            ast::Constant * x;
                            ops.emplace_back(x = new ast::Constant());
                            x->m_value = v;
                        } else {
                            ast::Variable * x;
                            ops.emplace_back(x = new ast::Variable());
                            x->m_identifier.assign(a, b);
                        }
                        state.emplace_back(State::readOp2);
                    }
                    if (false) {
                    lbl_binary:;
                        state.emplace_back(State::readOp1);
                        state.emplace_back(State::readOp0);
                        state.emplace_back(State::argSep);
                        state.emplace_back(State::readOp0);
                        nextToken(a, b); assert(tokenEq("("));
                    }
                    currentOp.emplace_back((int)ops.size() - 1);
                    break;
                case State::readOp1:
                    nextToken(a, b);
                    assert(tokenEq(")"));
                case State::readOp2:
                {
                    auto op = currentOp.back();
                    currentOp.pop_back();
                    for (int i = 0, l = ops.at(op)->argCount(); i < l; ++i) {
                        auto idx = op + (l - i);
                        ops[op]->setArg(l - i - 1, std::move(ops.at(idx)));
                        ops.erase(ops.begin() + idx);
                    }
                }
                break;
                case State::argSep:
                    nextToken(a, b); assert(tokenEq(","));
                    break;
                default: assert(!"unexpected state");
            }
        };
        assert(state.empty());
        assert(ops.size() == 1);
        return std::move(ops[0]);
    }
}

struct TestCase {
    std::vector<ldouble> inputs;
    ldouble output{};
};

static std::vector<TestCase> parseTestCases(int argc, const char * const * argv) {
    std::vector<TestCase> testCases;

    std::vector<std::string> args;
    int size;
    auto split = [&] (const char * begin) {
        const char * end = begin;
        size = 0;
        while (true) {
            if (*end == 0 || *end == ':') {
                if (size >= args.size()) { args.emplace_back(); }
                args[size].assign(begin, end);
                size++;
                if (*end == 0) { return; }
                end++;
                begin = end;
                continue;
            }
            end++;
        }
    };
    auto parseLD = [] (const std::string & s) {
        char * e;
        auto v = std::strtold(s.c_str(), &e);
        assert(((s.c_str() + s.length()) == e) && "failed to parse test case");
        return v;
    };
    for (int i = 0; i < argc; ++i) {
        split(argv[i]);
        assert((size >= 2) && "test case must contain at least 2 value: input:output");
        testCases.emplace_back();
        TestCase & tc = testCases.back();
        tc.inputs.resize(size - 1);
        for (int j = 0; j < size - 1; ++j) { tc.inputs[j] = parseLD(args[j]); }
        tc.output = parseLD(args[size - 1]);
    }

    return testCases;
}

static zephyr::rtti::MethodPtr compileExpression(const std::unique_ptr<ast::Operator> & expression, std::vector<std::string> & vars) {
    using namespace zephyr;

    compilation::MappedDataProvider sd;
#define impl_a_(sign) sd.registerFunction(#sign, rtti::BindFnShared<ldouble(ldouble, ldouble)>([] (ldouble a, ldouble b) -> ldouble { return a sign b; }));
    impl_a_(+);
    impl_a_(-);
    impl_a_(*);
    impl_a_(/ );
#undef impl_a_
    sd.registerFunction("^", rtti::BindFnShared<ldouble(ldouble, ldouble)>([] (ldouble a, ldouble b) -> ldouble { return std::pow(a, b); }));

    {
        using namespace compilation;
        CompilationUnit cu;

        auto n = cu.declareNativeType<ldouble>();

        auto m = cu.declareMethod();
        {
            std::map<ldouble, std::shared_ptr<rtti::Value>> consts;
            m->identifier("m");
            m->ret(n);

            {
                struct scanner : ast::Visitor {
                    std::map<ldouble, std::shared_ptr<rtti::Value>> * consts;
                    std::vector<std::string> * vars;
                    virtual void visit(ast::Operator * o) {
                        auto x = dynamic_cast<ast::BinaryOperator *>(o);
                        if (x) {
                            x->args[0]->visit(this);
                            x->args[1]->visit(this);
                        }
                    }
                    virtual void visit(ast::Constant * o) {
                        auto i = consts->find(o->m_value);
                        if (i == consts->end()) {
                            (*consts)[o->m_value] = rtti::BindValueShared<ldouble>(o->m_value);
                        }
                    }
                    virtual void visit(ast::Variable * o) {
                        for (int i = 0, l = (int)vars->size(); i < l; ++i) {
                            if ((*vars)[i] == o->m_identifier) { return; }
                        }
                        vars->emplace_back(o->m_identifier);
                    }
                } v;
                v.consts = &consts;
                v.vars = &vars;
                expression->visit(&v);
            }

            std::sort(vars.begin(), vars.end());
            {
                std::vector<rtti::TypePtr> args;
                args.reserve(vars.size());
                for (int i = 0, l = (int)vars.size(); i < l; ++i) { args.emplace_back(n); }
                m->args(args);
            }

            {
                struct visitor : ast::Visitor {
                    Method * m;
                    MappedDataProvider * sd;
                    std::map<ldouble, std::shared_ptr<rtti::Value>> * consts;
                    const std::vector<std::string> * vars;
#define impl_a_(node, sign) \
                        virtual void visit(ast::node * o) { \
                            o->args[0]->visit(this); \
                            o->args[1]->visit(this); \
                            m->emit(OpCode::invoke(sd->function(#sign))); \
                        }
                    impl_a_(Add, +);
                    impl_a_(Sub, -);
                    impl_a_(Mul, *);
                    impl_a_(Div, / );
                    impl_a_(Pow, ^);
#undef impl_a_
                    virtual void visit(ast::Constant * o) {
                        m->emit(OpCode::ldConst(consts->at(o->m_value)));
                    }
                    virtual void visit(ast::Variable * o) {
                        for (int i = 0, l = (int)vars->size(); i < l; ++i) {
                            if (o->m_identifier == (*vars)[i]) {
                                m->emit(OpCode::ldArg(i));
                                return;
                            }
                        }
                        assert(false && "variable not found");
                    }
                } v;
                v.m = m.get();
                v.sd = &sd;
                v.consts = &consts;
                v.vars = &vars;
                expression->visit(&v);
            }

            m->emit(OpCode::ret());
        }

        return cu.compile()->find<rtti::Method>("m");
    }
}

int main(int argc, char ** argv) {
    assert(argc >= 2);
    auto expression = parseExpression(argv[1]);
    auto testCases = parseTestCases(argc - 2, argv + 2);

    std::vector<std::string> vars;

    using namespace zephyr;

    auto m = compileExpression(expression, vars);

    ast::Args args1;
    runtime::ExecutionContext ctx;
    std::shared_ptr<rtti::Value> ret0 = { rtti::BindValueShared<ldouble>(0) };
    std::vector<std::shared_ptr<rtti::Value>> args0;
    constexpr ldouble epsilon = 0.00001;

    for (auto & x : testCases) {
        if (args0.size() < x.inputs.size()) {
            for (int i = 0, l = (int)(x.inputs.size() - args0.size()); i < l; ++i) {
                args0.emplace_back(rtti::BindValueShared<ldouble>(0));
            }
        }
        for (int i = 0, l = (int)x.inputs.size(); i < l; ++i) {
            args0[i]->cast<ldouble>() = x.inputs[i];
            args1.vars[vars[i]] = x.inputs[i];
        }
        ctx.executeDynamic(m, ret0, args0, x.inputs.size());
        auto ret00 = ret0->cast<ldouble>();

        auto ret1 = expression->compute(args1);

        assert(ret00 == ret1);

        assert(std::max(ret00, x.output) - std::min(ret00, x.output) < epsilon);
        assert(std::max(ret1, x.output) - std::min(ret1, x.output) < epsilon);
    }

}
