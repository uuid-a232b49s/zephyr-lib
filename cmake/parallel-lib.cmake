cmake_minimum_required(VERSION 3.11)

include(FetchContent)

FetchContent_Declare(
	parallel_lib
	GIT_REPOSITORY https://gitlab.com/uuid-a232b49s/parallel-lib.git
	GIT_TAG b8c0b042080f06e658a11f8555d7caa52c577df6
	UPDATE_DISCONNECTED TRUE
)
FetchContent_GetProperties(parallel_lib)
if(NOT parallel_lib_POPULATED)
	FetchContent_Populate(parallel_lib)
	set(PARALLEL_TESTS OFF CACHE INTERNAL "" FORCE)
	add_subdirectory("${parallel_lib_SOURCE_DIR}" "${parallel_lib_BINARY_DIR}" EXCLUDE_FROM_ALL)
endif()
