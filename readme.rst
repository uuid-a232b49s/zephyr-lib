zephyr-lib
==========


What is this?
-------------

- A tiny, quick (like zephyr), lightweight (like zefir ^^) runtime emulator

- Transforms a set of opcodes into a list of sequentially-invoked functions

- Dynamic opcode "compiler"

- `Lets you write assembly without writing assembly`


License
-------

Boost Software License - Version 1.0


Building
--------

- Dependencies

  - ``cmake`` 3.1+

  - ``c++11`` compiler

  - build tools (``build-essentials``, ``msvc``, etc)

1. Create build dir

  .. code-block:: bash

    mkdir _build
    cd _build

2. Initialize cmake cache

  .. code-block:: bash

    cmake -G Ninja ..

  - replace/remove ``-G Ninja`` as needed

3. Run build

  .. code-block:: bash

    cmake --build .

4. Run tests (probably a good practice)

  .. code-block:: bash

    ctest


CMake
-----

- static library target: ``zephyr-lib``

- shared library target: ``zephyr-lib-d``

  - utilized STL symbols are not exported

  - ensure that this target and the user code are compiled with the same settings (& compiler)

- tests variable: ``ZEPHYRLIB_TESTS``

  - ``ON`` by default

- docs variable: ``ZEPHYRLIB_DOCS``

  - ``OFF`` by default

  - incomplete

  - requires doxygen (FindDoxygen)

- library targets are excluded from ``ALL``

- static link

  .. code-block:: cmake

    # define executable/library
    add_executable(SomeObject ...)
    #add_library(SomeObject ...)
    ...
    target_link_libraries(SomeObject zephyr-lib) # link zephyr static lib

- dynamic link

  .. code-block:: cmake

    # define executable/library
    add_executable(SomeObject ...)
    #add_library(SomeObject ...)
    ...

    # link zephyr so
    target_link_libraries(SomeObject zephyr-lib-d)

    # copy so library to the output dir of the target
    add_custom_command(
      TARGET SomeObject
      POST_BUILD
      COMMAND ${CMAKE_COMMAND} -E copy $<TARGET_FILE:zephyr-lib-d> $<TARGET_FILE_DIR:SomeObject>
    )


Tested compilers
----------------

- gnu 10.2.1

- clang 11.0.1-2

- msvc 19


How do i use this?
------------------

- Check how tests work

Modules
*******

- ``zephyr::runtime``

  - runtime management

  - invocation interface

- ``zephyr::rtti``

  - rtti interface

  - type erasure

  - method/function abstraction

- ``zephyr::compilation``

  - transformation (type definition, opcodes -> method, cu -> module)

Theory
******

- ``zephyr::rtti::Symbol``

  - ``include/zephyr/rtti/symbol.hpp``

  - a symbol that represents some object

  - can be anything that the runtime would need

    - ``rtti::Method``, ``rtti::Type``, or anything else (atm nothing else)

- ``zephyr::rtti::Module``

  - ``include/zephyr/rtti/module.hpp``

  - a collection of all ``symbols`` that are needed to execute any compiled ``rtti::Method`` present in this ``Module``

  - can reference (depend) on other (separately compiled) ``Modules``

    - types declared in the referenced ``Modules`` are accessible to this ``Module``

- ``zephyr::runtime::ExecutionContext``

  - ``include/zephyr/runtime/execution.hpp``

  - represents a single thread of execution

  - has an ``execution stack``

    - memory space where variables are manipulated by ``opcodes``

- ``zephyr::compilation::CompilationUnit``

  - ``include/zephyr/compilation/compilation-unit.hpp``

  - represents a single unit that can be compiled into ``zephyr::rtti::Module``

  - defines ``methods`` & ``types``

  - compiles all defined ``symbols``

- ``zephyr::compilation::Method``

  - ``include/zephyr/compilation/method.hpp``

  - consists of argument types, return type and opcodes

- ``zephyr::compilation::OpCode``

  - ``include/zephyr/compilation/opcode.hpp``

  - represents a concrete operation

  - each opcode modifies the execution stack in a specific way

    - e.g. ``OpCode::ldConst(...)`` would copy a constant value onto the top of the stack

    - then ``OpCode::stLoc(loc_2)`` can consume that value & store it into ``loc_2`` local
