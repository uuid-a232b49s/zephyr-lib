#include <map>
#include <thread>
#include <cstddef>
#include <cstring>
#include <utility>
#include <iterator>
#include <typeindex>
#include <algorithm>
#include <functional>
#include <type_traits>

#include "zephyr/compilation/compilation-unit.hpp"
#include "zephyr/compilation/method.hpp"
#include "zephyr/compilation/type.hpp"
#include "zephyr/rtti/value.hpp"
#include "../c_compilation.hxx"
#include "../c_type.hxx"
#include "../r_method.hxx"
#include "../r_type.hxx"
#include "../rt_compilation.hxx"
#include "../rtti_module.hxx"
#include "../throw_.hxx"
#include "../structLayout.hxx"
#include "../common.hxx"
#include "parallel/parallel.hxx"

#define throw_(msg) throw EXC(zephyr::CompilationException, msg)
#define assert_(condition, msg) ASSERT_(condition, zephyr::CompilationException, msg)
#define assert_0(condition, ...) ASSERT_0(condition, zephyr::CompilationException, __VA_ARGS__)

#define inst_() (reinterpret_cast<const Instruction * &>(c->m_instruction))

#define addPatchField_(instUuid, field, value) \
(this->meta)->addPatchField( \
    (instUuid), \
    [] (Instruction * I) { return & I-> field ; }, \
    [=] (methodMeta * m) { return value ; }, \
    this->m_instruction \
)

#define dcast_0 do {
#define dswitch_0(inVar, type, var) { auto var = dynamic_cast<const type *>(inVar); if (var != nullptr) {
#define dswitch_00(inVar, outVar, type0, var0, type1, var1) { \
auto var0 = dynamic_cast<const type0 *>(inVar); \
auto var1 = dynamic_cast<const type1 *>(inVar); \
auto outVar = var0 ? static_cast<const zephyr::rtti::Type *>(var0) : (var1 ? var1 : nullptr); \
if (var0 != nullptr || var1 != nullptr) {
#define dswitch_1 break; } } 
#define dcast_1 assert_(false, "unexpected type on stack"); } while(0);

namespace {

    using zephyr::impl::Instruction;
    using zephyr::impl::invokeMeta;
    using zephyr::impl::getAlignPad;

    static bool isVoid(const zephyr::rtti::Type * t) {
        return t == zephyr::rtti::VoidType::Instance.get();// || dynamic_cast<const zephyr::rtti::VoidType *>(t) != nullptr;
    }

    template<typename emittedInst_t, typename field_t, typename value_t>
    static void applyPatch(emittedInst_t & inst, field_t & f, value_t v, zephyr::SLL srcInstruction) {
        if (!inst.patched) {
            f = std::move(v);
            inst.patched = true;
        } else {
            assert_0(f == v, "src instruction=", srcInstruction, ", branch emission diverged: different patch values");
        }
    }

    static bool compareInstructions(const Instruction & a, const Instruction & b) {
        return a.fn == b.fn && std::memcmp(&a.data, &b.data, sizeof(decltype(a.data))) == 0;
    }

    static const zephyr::rtti::NativeType * verifyTypeInfo(const std::type_info * i, const zephyr::rtti::Symbol * s) {
        auto n = dynamic_cast<const zephyr::rtti::NativeType *>(s);
        if (n) {
            auto r = n->rtti();
            if (r) {
                if (r->typeinfo() == *i) {
                    return n;
                }
            }
        }
        return nullptr;
    }

    template<typename container_t>
    static const zephyr::rtti::NativeType * findTypeByInfo(const std::type_info * i, const container_t & container) {
        for (auto & x : container) {
            auto n = verifyTypeInfo(i, &*x);
            if (n) { return n; }
        }
        return nullptr;
    };

    static const zephyr::runtime::impl::NativeTypeImpl * const invokeMetaType = [] () -> zephyr::runtime::impl::NativeTypeImpl * {
        static zephyr::runtime::impl::NativeTypeImpl x;
        x.m_rtti = zephyr::rtti::RttiDescriptorGeneric<zephyr::impl::invokeMeta>::Instance();
        return &x;
    } ();

    static const zephyr::rtti::impl::AddressTypeImpl * asAddress(const zephyr::rtti::Symbol * t) {
        struct visitor : zephyr::rtti::Visitor {
            const zephyr::rtti::impl::AddressTypeImpl * o = nullptr;
            virtual std::shared_ptr<void> visit(std::shared_ptr<const zephyr::rtti::AddressType> a) override {
                o = static_cast<const zephyr::rtti::impl::AddressTypeImpl *>(a.get());
                return nullptr;
            }
        } v;
        t->accept(&v);
        return v.o;
    }

    static std::shared_ptr<const zephyr::rtti::impl::AddressTypeImpl> asAddress(const std::shared_ptr<const zephyr::rtti::Symbol> & t) {
        struct visitor : zephyr::rtti::Visitor {
            std::shared_ptr<const zephyr::rtti::impl::AddressTypeImpl> o = nullptr;
            virtual std::shared_ptr<void> visit(std::shared_ptr<const zephyr::rtti::AddressType> a) override {
                o = std::static_pointer_cast<const zephyr::rtti::impl::AddressTypeImpl>(a);
                return nullptr;
            }
        } v;
        t->accept(&v);
        return v.o;
    }

    static zephyr::rtti::RttiDescriptorGeneric<void *> ptrRtti {};

}

struct zephyr::compilation::CompilationUnit::impl {

    struct emittingBranch;

    struct instUuid {
        SLL srcInstIdx;
        ULL emittedIdx;
        bool operator<(const instUuid & rhs) const { return std::tie(srcInstIdx, emittedIdx) < std::tie(rhs.srcInstIdx, rhs.emittedIdx); }
        bool operator==(const instUuid & rhs) const { return std::tie(srcInstIdx, emittedIdx) == std::tie(rhs.srcInstIdx, rhs.emittedIdx); }
    };

    struct emittedInst {
        instUuid uuid;
        Instruction inst;
        bool patched;
    };

    struct emittedLocal {
        const rtti::Type * type;
        ULL srcLocalIdx;

    // layoutStruct interface

        ULL align() { return type->align(); }
        ULL size() { return type->size(); }
        emittedLocal & operator*() { return *this; }

    };

    struct methodMeta {
        std::function<const rtti::Type * (const rtti::Type *)> * toRtType = nullptr;
        std::function<const runtime::impl::MethodImpl * (const rtti::Method *)> * toRtMethod = nullptr;
        std::function<const rtti::Type * (const std::type_info *)> * infoToType = nullptr;
        std::shared_ptr<rtti::impl::CompiledModuleImpl> implModule;
        const compilation::Method::impl::cMethod * srcMethod = nullptr;
        runtime::impl::MethodImpl * implMethod = nullptr;

        std::map<usize, usize> m_constValIdxSrcToImpl;
        std::vector<std::shared_ptr<const rtti::AbstractFn<void>>> m_rttiFnStorage;
        ULL m_maxStackSize = 0;
        std::vector<emittedInst> m_instructions;

        struct {
            std::vector<emittedLocal> m_locals;
            bool m_changed = true;

        private:
            std::vector<ULL> m_offsets;
            ULL m_size, m_align;

            void recompute() {
                if (m_changed) {
                    m_offsets.resize(m_locals.size());
                    zephyr::impl::layoutStruct(m_locals.begin(), m_locals.end(), m_align, m_size, m_offsets.begin());
                    m_size += getAlignPad(m_size, alignof(std::max_align_t));
                    m_changed = false;
                }
            }

        public:
            ULL size() {
                recompute();
                return m_size;
            }

            ULL align() {
                recompute();
                return m_align;
            }

            const std::vector<ULL> & offsets() {
                recompute();
                return m_offsets;
            }

        } m_locals;

        std::vector<std::function<void(methodMeta *)>> m_patches;
        std::vector<std::unique_ptr<emittingBranch>> m_branches;

        void compile() {
            {
                auto b = std::unique_ptr<impl::emittingBranch>(new impl::emittingBranch());
                b->meta = this;
                m_branches.emplace_back(std::move(b));
            }
            while (!m_branches.empty()) {
                auto b = std::move(m_branches.back());
                m_branches.pop_back();
                b->emit();
            }
            if (!m_locals.m_locals.empty()) {
                // allocate local storage
                emittedInst E;
                E.inst.data.m_ull = m_locals.size();
                E.inst.fn = [] (const Instruction * i, runtime::ExecutionContext * c) {
                    c->m_stack += i->data.m_ull;
                    inst_()++;
                };
                E.patched = true;
                E.uuid = instUuid{ -1, 0 };
                m_instructions.insert(m_instructions.begin(), E);
            }
            for (auto & x : m_patches) { x(this); }
        }

        void applyToImpl() {
            implMethod->m_instructions.reserve(m_instructions.size());
            for (auto & x : m_instructions) { implMethod->m_instructions.emplace_back(x.inst); }
            implMethod->m_rttiFnData = std::move(m_rttiFnStorage);
            implMethod->m_rttiFnData.insert(implMethod->m_rttiFnData.begin(), std::make_move_iterator(m_cachedRtti_lbfn.begin()), std::make_move_iterator(m_cachedRtti_lbfn.end()));
            implMethod->m_maxStackSize = m_maxStackSize + m_locals.size();
        }

        ULL findInstructionInsertIdx(SLL srcInstIdx) {
            for (ULL i = 0, l = m_instructions.size(); i < l; ++i) {
                auto & I = m_instructions[i];
                //if (I.srcOpCodeIdx < srcInstIndex) { continue; }
                if (I.uuid.srcInstIdx > srcInstIdx) { return i; }
                assert_(I.uuid.srcInstIdx != srcInstIdx, "unable to find insert index: instructions already present");
                if (I.uuid.srcInstIdx > srcInstIdx) { return i; }
            }
            return m_instructions.size();
        }

        bool tryFindInstructionIdxByUUID(instUuid uuid, ULL & idx) {
            for (ULL i = 0, l = m_instructions.size(); i < l; ++i) {
                if (m_instructions[i].uuid == uuid) {
                    idx = i;
                    return true;
                }
            }
            return false;
        }

        ULL findInstructionIdxByUUID(instUuid uuid) {
            ULL idx;
            assert_(tryFindInstructionIdxByUUID(uuid, idx), "emitted instruction not found");
            return idx;
        }

        bool tryFindInstructionsIdxByIndex(SLL srcInstIndex, ULL & begin, ULL & end) {
            bool startFound = false;
            for (ULL i = 0, l = m_instructions.size(); i < l; ++i) {
                if (m_instructions[i].uuid.srcInstIdx == srcInstIndex) {
                    if (!startFound) {
                        begin = i;
                        startFound = true;
                    }
                } else {
                    if (startFound) {
                        end = i;
                        return true;
                    }
                }
            }
            if (startFound) {
                end = m_instructions.size();
                return true;
            }
            begin = end = m_instructions.size();
            return false;
        }

        void findInstructionsByIndex(SLL srcInstIndex, ULL & begin, ULL & end) {
            assert_(tryFindInstructionsIdxByIndex(srcInstIndex, begin, end), "emitted instruction not found");
        }

        void addPatch(std::function<void(methodMeta *)> patch) { m_patches.emplace_back(std::move(patch)); }

        template<typename fieldSelector_t, typename getValue_t>
        void addPatchField(instUuid uuid, fieldSelector_t fieldSelector, getValue_t getValue, SLL srcInstruction) {
            addPatch(
                std::bind([=] (methodMeta * m, fieldSelector_t & fieldSelector, getValue_t & getValue) -> void {
                    auto & I = m->m_instructions[m->findInstructionIdxByUUID(uuid)];
                    applyPatch(I, *fieldSelector(&I.inst), getValue(m), srcInstruction);
                },
                std::placeholders::_1, std::move(fieldSelector), std::move(getValue))
            );
            /*addPatch([=] (methodMeta * m) {
                auto & I = m->m_instructions[m->findInstructionIdxByUUID(uuid)];
                applyPatch(I, *fieldSelector(&I.inst), getValue(m), srcInstruction);
            });*/
        }

        ULL getLocalOffsetFrom0(ULL srcLocalIdx) {
            for (ULL i = 0, l = m_locals.m_locals.size(); i < l; ++i) {
                if (m_locals.m_locals[i].srcLocalIdx == srcLocalIdx) {
                    return m_locals.offsets()[i];
                }
            }
            assert_(false, "failed to find target local");
        }

        const SLL * getCachedOffsets(std::vector<SLL> offsets) {
            if (offsets.empty()) { return nullptr; }
            for (auto & x : implMethod->m_offsets) {
                if (*x == offsets) { return &x->operator[](0); }
            }
            implMethod->m_offsets.emplace_back(new std::vector<SLL>(std::move(offsets)));
            return &implMethod->m_offsets.back()->operator[](0);
        }

    #define impl_a_(identifier, rttiFn, errMsgAdj)\
        std::map<const rtti::NativeType *, zephyr::impl::Fn##identifier *> m_cachedRtti_##identifier;\
        zephyr::impl::Fn##identifier * getCachedRtti_##identifier(const rtti::NativeType * t) {\
            auto f = m_cachedRtti_##identifier.find(t);\
            if (f != m_cachedRtti_##identifier.end()) { return f->second; }\
            {\
                auto rtti = t->rtti();\
                assert_(rtti, "native type missing rtti descriptor");\
                auto fn = rtti->rttiFn();\
                assert_(fn, "type not " #errMsgAdj);\
                m_cachedRtti_##identifier[t] = fn.get();\
                implMethod->m_rttiFnData.emplace_back(fn);\
                return fn.get();\
            }\
        }

        impl_a_(Dtor, dtor, destructible);
        impl_a_(Copy, copy, copyable);
        impl_a_(Move, move, movable);
        impl_a_(ToBool, toBool, bool - convertible);

    #undef impl_a_

        std::vector<std::shared_ptr<const rtti::LateBoundFn>> m_cachedRtti_lbfn;
        const zephyr::impl::FnLb * getCachedRtti_LbFn(const std::shared_ptr<const rtti::LateBoundFn> & lbfn) {
            auto f = std::find(m_cachedRtti_lbfn.begin(), m_cachedRtti_lbfn.end(), lbfn);
            if (f != m_cachedRtti_lbfn.end()) { return f->get(); }
            m_cachedRtti_lbfn.emplace_back(lbfn);
            return m_cachedRtti_lbfn.back().get();
        }

        const void * getCachedRtti_Const(const std::shared_ptr<const rtti::Value> & c) {
            auto & s = implMethod->m_constants;
            auto f = std::find(s.begin(), s.end(), c);
            if (f != s.end()) { return f->get()->data(); }
            s.emplace_back(c);
            return s.back().get()->data();
        }

        void addModuleReference(const rtti::Symbol * s) {
            {
                auto a = asAddress(s);
                if (a) {
                    auto & r = implModule->m_referencedSymbols;
                    if (std::find_if(r.begin(), r.end(), [&] (const std::shared_ptr<const rtti::Symbol> & b) { return b.get() == a; }) == r.end()) {
                        implModule->m_referencedSymbols.emplace_back(asAddress(a->m_this.lock()));
                    }
                }
            }
            auto m = s->module();
            if (m && m != implModule) {
                auto & r = implModule->m_referencedModules;
                if (std::find(r.begin(), r.end(), m) == r.end()) {
                    implModule->m_referencedModules.emplace_back(std::move(m));
                }
            }
        }

    };

#undef assert_
#define assert_(condition, ...) ASSERT_0(condition, zephyr::CompilationException, "src instruction=", this->m_instruction, ": ", __VA_ARGS__)

    struct emittingBranch {

        struct stackObject {
            const rtti::Type * m_value;
            ULL m_pad;
            stackObject(decltype(m_value) v, ULL pad) : m_value(v), m_pad(pad) {}
            bool operator==(const stackObject & rhs) const { return std::tie(m_pad, m_value) == std::tie(rhs.m_pad, rhs.m_value); }
        };

        struct emittigLocal {
            ULL m_emittedLocalIdx;
            bool m_init;
            bool operator==(const emittigLocal & rhs) const { return std::tie(m_emittedLocalIdx, m_init) == std::tie(rhs.m_emittedLocalIdx, rhs.m_init); }
        };

        methodMeta * meta = nullptr;

    private:
        SLL m_instruction = 0;
        std::vector<emittedInst> m_staged;
        bool m_refMode = false;

        std::vector<stackObject> m_stack;
        ULL m_currentStackSize = 0;

        std::vector<emittigLocal> m_locals;

    public:

        void emit() {
            while (m_instruction >= 0) {
            lbl_begin:;
                assert_((ULL)m_instruction < meta->srcMethod->m_opcodes.size(), "unexpected instruction end");
                {
                    auto & op = meta->srcMethod->m_opcodes[m_instruction];
                    switch (op.op()) {
                        case OpCode::Operator::ldArg:
                            assert_(op.data().m_ull_b.i < meta->implMethod->m_args.size(), "ldArg specified illegal argument index");
                            emit_ldArg(op.data().m_ull_b.i, static_cast<OpCode::LoadMode>(op.data().m_ull_b.b));
                            break;
                        case OpCode::Operator::ldConst:
                            assert_(op.data().value != nullptr, "ldConst specified null constant");
                            emit_ldConst(op.data().value);
                            break;
                        case OpCode::Operator::ldLoc: {
                            auto & d = op.data().m_ull_b;
                            assert_(d.i < meta->srcMethod->m_locals.size(), "ldLoc specified illegal local");
                            emit_ldLoc(d.i, static_cast<OpCode::LoadMode>(d.b));
                            break;
                        }
                        case OpCode::Operator::stLoc:
                            assert_(op.data().ull < meta->srcMethod->m_locals.size(), "stLoc specified illegal local");
                            emit_stLoc(op.data().ull);
                            break;
                        case OpCode::Operator::clLoc:
                            assert_(op.data().ull < meta->srcMethod->m_locals.size(), "clLoc specified illegal local");
                            emit_clLoc(op.data().ull);
                            break;
                        case OpCode::Operator::dup:
                            emit_dup();
                            break;
                        case OpCode::Operator::rem:
                            emit_rem();
                            break;
                        case OpCode::Operator::ret:
                            emit_ret();
                            break;
                        case OpCode::Operator::invoke:
                            assert_(op.data().method, "invoke specified null method");
                            emit_invoke(op.data().method.get());
                            break;
                        case OpCode::Operator::invokeLbFn:
                            assert_(op.data().lbfn != nullptr, "invoke specified null method");
                            emit_invokeLbFn(op.data().lbfn);
                            break;
                        case OpCode::Operator::jmp:
                            assert_(op.data().ull < meta->srcMethod->m_labels.size(), "jmp specified illegal label");
                            emit_branch(meta->srcMethod->m_labels[op.data().ull], true, true);
                            break;
                        case OpCode::Operator::branch: {
                            auto & d = op.data().m_ull_b;
                            assert_(d.i < meta->srcMethod->m_labels.size(), "branch specified illegal label");
                            emit_branch(meta->srcMethod->m_labels[d.i], d.b, false);
                            break;
                        }
                        case OpCode::Operator::ref:
                            if (m_refMode) { throw_("duplicate ref instruction"); }
                            ++m_instruction;
                            m_refMode = true;
                            goto lbl_begin;
                        case OpCode::Operator::deref:
                            emit_deref();
                            break;
                        default: throw_("instruction not implemented");
                    }
                }
                assert_(m_staged.empty(), "instructions not commited");
                assert_(m_refMode == false, "ref instruction not consumed");
            }
            assert_(m_stack.empty(), "illegal stack state: non-empty");
        }

    private:

        stackObject getStackObj() {
            assert_(!m_stack.empty(), "stack object expected while stack is empty");
            auto o = std::move(m_stack.back());
            m_stack.pop_back();
            m_currentStackSize -= o.m_value->size() + o.m_pad;
            return o;
        }

        const stackObject & peekStackObj(ULL offset = 0) const {
            assert_(offset < m_stack.size(), "failed to peek target stack object: out of bounds");
            return m_stack[m_stack.size() - 1 - offset];
        }

        void putStackObj(const rtti::Type * value, ULL pad) {
            m_stack.emplace_back(value, pad);
            m_currentStackSize += value->size() + pad;
            if (meta->m_maxStackSize < m_currentStackSize) { meta->m_maxStackSize = m_currentStackSize; }
        }

        emittigLocal & getLocal(ULL srcLocalIdx) {
            for (auto & x : m_locals) {
                auto & el = meta->m_locals.m_locals[x.m_emittedLocalIdx];
                if (el.srcLocalIdx == srcLocalIdx) { return x; }
            }
            meta->m_locals.m_changed = true;
            meta->m_locals.m_locals.emplace_back(emittedLocal{ (*meta->toRtType)(meta->srcMethod->m_locals[srcLocalIdx].get()), srcLocalIdx });
            m_locals.emplace_back(emittigLocal{ meta->m_locals.m_locals.size() - 1, false });
            return m_locals.back();
        }

        instUuid stageInstruction(Instruction i) {
            m_staged.emplace_back(emittedInst{ instUuid{ m_instruction, m_staged.size() }, std::move(i), false });
            return m_staged.back().uuid;
        }

        void commitInstructions() {
            assert_(!m_staged.empty(), "unable to commit instructions: none staged");
            ULL Ja, Jb;
            if (meta->tryFindInstructionsIdxByIndex(m_instruction, Ja, Jb)) {
                assert_((Jb - Ja) == m_staged.size(), "branch emission diverged: different micro-instruction count");
                for (ULL i = 0, l = m_staged.size(); i < l; ++i) {
                    assert_(compareInstructions(m_staged[i].inst, meta->m_instructions[Ja + i].inst), "branch emission diverged: different micro-instruction data");
                }
            } else {
                auto idx = meta->findInstructionInsertIdx(m_instruction);
                meta->m_instructions.insert(meta->m_instructions.begin() + idx, std::make_move_iterator(m_staged.begin()), std::make_move_iterator(m_staged.end()));
            }
            m_staged.clear();
        }

#include "./emit/ldArg.inl"
        void emit_ldArg(ULL idx, OpCode::LoadMode mode) {
            assert_(mode == OpCode::LoadMode::copy, "move mode unsupported for ldArg");
            auto t_ = (*meta->toRtType)(meta->srcMethod->m_args[idx].get());
            meta->addModuleReference(t_);
            auto pad = getAlignPad(m_currentStackSize, t_->align());

            dcast_0 {
                dswitch_0(t_, rtti::NativeType, t) {
                    Instruction I;
                    I.data.m_ldArg.typeSize = t->size();
                    ULL stackSize0 = m_currentStackSize + pad;
                    I.data.m_ldArg.argIdx = idx;
                    I.data.m_ldArg.pad = pad;
                    I.data.m_ldArg.copy = meta->getCachedRtti_Copy(t);
                    I.fn = [] (const Instruction * i, runtime::ExecutionContext * c) {
                        auto & d = i->data.m_ldArg;
                        auto im = reinterpret_cast<invokeMeta *>(c->m_stack - d.offsetTo0 - sizeof(invokeMeta));
                        d.copy->invoke(im->argsStartAddr + im->argOffsets[d.argIdx], c->m_stack + d.pad);
                        c->m_stack += d.typeSize + d.pad;
                        inst_()++;
                    };
                    auto iUuid = stageInstruction(I);
                    putStackObj(t, pad);
                    commitInstructions();
                    addPatchField_(iUuid, data.m_ldArg.offsetTo0, stackSize0 + m->m_locals.size());
                } dswitch_1;
            } dcast_1;
            m_instruction += 1;
        }

        void emit_deref() {
            auto so = getStackObj();
            dcast_0 {
                dswitch_0(so.m_value, rtti::AddressType, t) {
                    meta->addModuleReference(t);
                    auto base = t->base();
                    auto pad = getAlignPad(m_currentStackSize, base->align());
                    Instruction I;
                    I.data.m_deref.pad0 = so.m_pad;
                    I.data.m_deref.pad1 = pad;
                    I.data.m_deref.typeSize = base->size();
                    dcast_0 {
                        /*dswitch_00(base.get(), b, rtti::NativeType, nb, rtti::AddressType, ab) {
                            I.data.m_deref.copy = meta->getCachedRtti_Copy(b);
                            I.fn = [] (const Instruction * i, runtime::ExecutionContext * c) {
                                auto & d = i->data.m_deref;
                                d.copy->invoke(*reinterpret_cast<void **>(c->m_stack - sizeof(void *)), c->m_stack - sizeof(void *) - d.pad0 + d.pad1);
                                c->m_stack += d.pad1 + d.typeSize - sizeof(void *) - d.pad0;
                                inst_()++;
                            };
                        } dswitch_1;*/
                        dswitch_0(base.get(), rtti::NativeType, b) {
                            I.data.m_deref.copy = meta->getCachedRtti_Copy(b);
                            I.fn = [] (const Instruction * i, runtime::ExecutionContext * c) {
                                auto & d = i->data.m_deref;
                                d.copy->invoke(*reinterpret_cast<void **>(c->m_stack - sizeof(void *)), c->m_stack - sizeof(void *) - d.pad0 + d.pad1);
                                c->m_stack += d.pad1 + d.typeSize - sizeof(void *) - d.pad0;
                                inst_()++;
                            };
                        } dswitch_1;
                        dswitch_0(base.get(), rtti::AddressType, b) {
                            I.fn = [] (const Instruction * i, runtime::ExecutionContext * c) {
                                auto & d = i->data.m_deref;
                                *reinterpret_cast<void **>(c->m_stack - sizeof(void *) - d.pad0 + d.pad1) = **reinterpret_cast<void ***>(c->m_stack - sizeof(void *));
                                c->m_stack += d.typeSize + d.pad1 - sizeof(void *) - d.pad0;
                                inst_()++;
                            };
                        } dswitch_1;
                    } dcast_1;
                    stageInstruction(I);
                    putStackObj(base.get(), pad);
                    commitInstructions();
                } dswitch_1;
            } dcast_1;
            m_instruction += 1;
        }

#include "./emit/ldConst.inl"
#include "./emit/ldLoc.inl"
#include "./emit/stLoc.inl"
#include "./emit/clLoc.inl"
#include "./emit/dup.inl"
#include "./emit/rem.inl"
#include "./emit/ret.inl"
#include "./emit/invoke.inl"
#include "./emit/branch.inl"

        // m_currentStackSize not modified
        instUuid stagePadInst(ULL amount) {
            Instruction I;
            I.data.m_ull = amount;
            I.fn = [] (const Instruction * i, runtime::ExecutionContext * c) {
                c->m_stack += i->data.m_ull;
                inst_()++;
            };
            return stageInstruction(I);
        }

        // m_currentStackSize not modified
        instUuid stagePadConsumeInst(ULL amount) {
            Instruction I;
            I.data.m_ull = amount;
            I.fn = [] (const Instruction * i, runtime::ExecutionContext * c) {
                c->m_stack -= i->data.m_ull;
                inst_()++;
            };
            return stageInstruction(I);
        }

    };

};

#undef assert_
#define assert_(condition, msg) ASSERT_(condition, zephyr::CompilationException, msg)

namespace zephyr {
    namespace compilation {

        std::shared_ptr<const rtti::Module> CompilationUnit::compile(unsigned int threadCount) const {
            std::shared_ptr<zephyr::rtti::impl::CompiledModuleImpl> implModule{ new zephyr::rtti::impl::CompiledModuleImpl() };
            implModule->this_ = implModule;

            std::vector<std::shared_ptr<const rtti::impl::AddressTypeImpl>> refAddrTypes; // prevents destruction

            std::map<const compilation::Type *, runtime::impl::TypeImpl *> compiledTypes;
            std::function<const rtti::Type * (const rtti::Type *)> toRtType = [&] (const rtti::Type * t) -> const rtti::Type * {
                {
                    auto x = dynamic_cast<const compilation::Type *>(t);
                    if (x) {
                        auto r = compiledTypes.find(x);
                        assert_(r != compiledTypes.end(), "failed to find compiled type");
                        return r->second;
                    }
                }
                {
                    auto x = asAddress(t);
                    if (x) {
                        usize i = 0;
                        const rtti::Type * a = x;
                        for (auto b = x; b; ++i, b = asAddress(b->m_base.get())) {
                            a = b->m_base.get();
                        }
                        if (!dynamic_cast<const compilation::Type *>(a)) { return x; } // pointer type already based on runtime-compatible type object
                        std::shared_ptr<const rtti::Type> y { std::shared_ptr<const rtti::Type>(), toRtType(a) }; // should refactor this 'toRtType' call
                        for (auto j = i - 1; j < i; --j) { // 'descend up' from the inner-most pointer type & map to runtime pointer type
                            std::shared_ptr<const rtti::impl::AddressTypeImpl> b = asAddress(x->m_this.lock());
                            for (auto k = 0; k < j; ++k) {
                                b = asAddress(b->m_base);
                            }
                            y = y->address(b->constraints());
                        }
                        refAddrTypes.emplace_back(asAddress(y));
                        return y.get();
                    }
                }
                return t;
            };
            std::function<const rtti::Type * (const std::type_info *)> infoToType = [&] (const std::type_info * ti) -> const rtti::Type * {
                if (*ti == typeid(void)) { return rtti::VoidType::Instance.get(); }
                for (auto & x : compiledTypes) {
                    auto t = verifyTypeInfo(ti, x.second);
                    if (t) { return t; }
                }
                for (auto & x : referencedModules) {
                    if (x) {
                        auto m = dynamic_cast<const rtti::impl::CompiledModuleImpl *>(x.get());
                        if (m) {
                            auto t = findTypeByInfo(ti, m->m_unnamedSymbols);
                            if (t) { return t; }
                            for (auto & xb : m->m_namedSymbols) {
                                t = verifyTypeInfo(ti, xb.second.get());
                                if (t) { return t; }
                            }
                        }
                    }
                }
                assert_(false, "failed to find target type from a given type_info");
            };

            auto addSymbol = [&] (std::unique_ptr<rtti::Symbol> symbol) {
                auto & id = symbol->identifier();
                if (!id.empty()) {
                    ASSERT_(implModule->m_namedSymbols.count(id) == 0, zephyr::AssertionException, "symbol identifier duplicate");
                    implModule->m_namedSymbols[id] = std::move(symbol);
                } else implModule->m_unnamedSymbols.emplace_back(std::move(symbol));
            };

            // compile types
            for (auto & n0 : m_nativeTypes) {
                auto n = static_cast<compilation::impl::NativeTypeImpl *>(n0.get());
                ASSERT_NOT_NULL(n->m_rtti);
                auto c = std::unique_ptr<runtime::impl::NativeTypeImpl>(new runtime::impl::NativeTypeImpl());
                c->m_module = implModule;
                c->m_rtti = n->m_rtti;
                c->m_identifier = n->m_identifier;
                compiledTypes[n] = c.get();
                implModule->m_nativeTypes[std::type_index(c->m_rtti->typeinfo())] = c.get();
                addSymbol(std::move(c));
            }

            std::map<const compilation::Method::impl::cMethod *, runtime::impl::MethodImpl *> compiledMethods;
            std::function<const runtime::impl::MethodImpl * (const rtti::Method *)> toRtMethod = [&] (const rtti::Method * m) -> const runtime::impl::MethodImpl * {
                {
                    auto x = dynamic_cast<const runtime::impl::MethodImpl *>(m);
                    if (x) { return x; }
                }
                {
                    auto x = dynamic_cast<const compilation::Method::impl::cMethod *>(m);
                    if (x) {
                        auto r = compiledMethods.find(x);
                        assert_(r != compiledMethods.end(), "failed to find compiled method");
                        return r->second;
                    }
                }
                assert_(false, "target method not found");
            };

            // compile methods
            for (auto & x0 : m_methods) { // managed methods
                auto getImplType = [&] (const rtti::TypePtr & t) -> rtti::TypePtr {
                    auto a = dynamic_cast<const compilation::Type *>(t.get());
                    if (a) { return rtti::TypePtr(implModule, compiledTypes.at(a)); }
                    {
                        auto rt = toRtType(t.get());
                        if (rt != t.get()) {
                            auto p = dynamic_cast<const rtti::impl::AddressTypeImpl *>(rt);
                            assert_(p, "unexpected type");
                            return p->m_this.lock();
                        }
                    }
                    return t;
                };
                auto x = static_cast<compilation::Method::impl::cMethod *>(x0.get());
                auto c = std::unique_ptr<runtime::impl::MethodImpl>(new runtime::impl::MethodImpl());
                c->m_module = implModule;
                c->m_args.reserve(x->m_args.size());
                for (auto & arg : x->m_args) { c->m_args.emplace_back(getImplType(arg)); }
                c->m_ret = getImplType(x->m_ret);
                c->m_identifier = x->m_identifier;
                compiledMethods[x] = c.get();
                addSymbol(std::move(c));
            }
            {
                auto compileMethod = [&] (Method::impl::cMethod * m, runtime::impl::MethodImpl * rm) {
                    impl::methodMeta meta;
                    meta.toRtType = &toRtType;
                    meta.toRtMethod = &toRtMethod;
                    meta.infoToType = &infoToType;
                    meta.implModule = implModule;
                    meta.srcMethod = m;
                    meta.implMethod = rm;
                    meta.compile();
                    meta.applyToImpl();
                };
                if (threadCount == 1) {
                    for (auto & x0 : m_methods) {
                        auto x = static_cast<Method::impl::cMethod *>(x0.get());
                        compileMethod(x, compiledMethods.at(x));
                    }
                } else {
                    int threadCount0 = threadCount <= 0 ? std::thread::hardware_concurrency() : threadCount;
                    parallel::thread_pool p { threadCount0 };
                    std::vector<std::future<void>> f;
                    f.reserve(threadCount0);
                    auto collect = [&] {
                        std::string aggregateErr;
                        for (auto & x : f) {
                            try {
                                x.get();
                            } catch (zephyr::Exception & ex) {
                                aggregateErr += ex.what();
                                aggregateErr += "\n";
                            }
                        }
                        f.clear();
                        ASSERT_0(aggregateErr.empty(), zephyr::CompilationException, aggregateErr);
                    };
                    for (ULL i = 0, l = m_methods.size(); i < l; ++i) {
                        auto m = static_cast<Method::impl::cMethod *>(m_methods[i].get());
                        auto rm = compiledMethods[static_cast<Method::impl::cMethod *>(m_methods[i].get())];
                        f.emplace_back(p.execute([&compileMethod, m, rm] { compileMethod(m, rm); }));
                        if (f.size() >= threadCount0) { collect(); }
                    }
                    collect();
                }
            }

            implModule->m_identifier = identifier;

            return implModule;
        }

    }
}
