void emit_ldLoc(ULL idx, OpCode::LoadMode mode) {
    //assert_(!mode, "move ldLoc not supported");

    auto & loc = getLocal(idx);
    assert_(loc.m_init, "target local not initialized");
    auto & eloc = meta->m_locals.m_locals[loc.m_emittedLocalIdx];
    meta->addModuleReference(eloc.type);

    auto stackSize0 = m_currentStackSize;

    auto pad = getAlignPad(m_currentStackSize, eloc.type->align());

    do {
        {
            auto t = dynamic_cast<const rtti::NativeType *>(eloc.type);
            if (t != nullptr) {
                Instruction I;
                switch (mode) {
                    case OpCode::LoadMode::copy:
                        I.data.m_ldLoc.copy = meta->getCachedRtti_Copy(t);
                        break;
                    case OpCode::LoadMode::move:
                        I.data.m_ldLoc.move = meta->getCachedRtti_Move(t);
                        break;
                    default: assert_(false, "illegal LoadMode");
                }
                I.data.m_ldLoc.typeSize = eloc.type->size();
                I.data.m_ldLoc.pad = pad;
#define impl_a_(field_)\
                I.fn = [] (const Instruction * i, runtime::ExecutionContext * c) {\
                    auto & d = i->data.m_ldLoc;\
                    d.field_->invoke(c->m_stack - d.offsetToLoc, c->m_stack + d.pad);\
                    c->m_stack += d.typeSize + d.pad;\
                    inst_()++;\
                }
                switch (mode) {
                    case OpCode::LoadMode::copy:
                        impl_a_(copy);
                        break;
                    case OpCode::LoadMode::move:
                        impl_a_(move);
                        loc.m_init = false;
                        break;
                    default: assert_(false, "illegal LoadMode");
                }
#undef impl_a_
                auto iUuid = stageInstruction(I);
                addPatchField_(iUuid, data.m_ldLoc.offsetToLoc, (stackSize0 + m->m_locals.size()) - m->getLocalOffsetFrom0(idx));
                commitInstructions();
                putStackObj(eloc.type, pad);
                break;
            }
        }
        assert_(false, "unexpected type");
    } while (0);
    m_instruction++;
}
