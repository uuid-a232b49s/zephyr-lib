void emit_ldConst(std::shared_ptr<const rtti::Value> cv) {
    auto t_ = (*meta->infoToType)(&cv->typeinfo());
    meta->addModuleReference(t_);

    auto pad = getAlignPad(m_currentStackSize, t_->align());

    do {
        {
            auto t = dynamic_cast<const rtti::NativeType *>(t_);
            if (t != nullptr) {
                Instruction I;
                I.data.m_ldConst.typeSize = t->size();
                I.data.m_ldConst.pad = pad;
                I.data.m_ldConst.data = meta->getCachedRtti_Const(cv);
                I.data.m_ldConst.copy = meta->getCachedRtti_Copy(t);
                I.fn = [] (const Instruction * i, runtime::ExecutionContext * c) {
                    auto & d = i->data.m_ldConst;
                    d.copy->invoke(d.data, c->m_stack + d.pad);
                    c->m_stack += d.typeSize + d.pad;
                    inst_()++;
                };
                stageInstruction(I);
                putStackObj(t, pad);
                break;
            }
        }
        assert_(false, "unexpected type");
    } while (0);
    commitInstructions();
    m_instruction++;
}
