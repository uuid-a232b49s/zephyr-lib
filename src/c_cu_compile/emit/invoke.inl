void emit_invoke(const rtti::Method * method0) {
    // mostly a copy paste from emit_invokeLbFn

    auto method = (*meta->toRtMethod)(method0);
    const rtti::Type * ret = method->ret().get();
    auto argCount = method->argc();

    for (ULL i = 0, l = argCount; i < l; ++i) {
        assert_(method->argv(i).get() == peekStackObj(l - 1 - i).m_value, "mismatching arg types");
    }

    for (ULL i = 0, l = argCount; i < l; ++i) { meta->addModuleReference(method->argv(i).get()); }
    meta->addModuleReference(ret);
    meta->addModuleReference(method);

    ULL stackSizeAtRet; // where the return value will be placed initially after native method is invoked
    emit0_invoke_allocRetStorage(ret, argCount, stackSizeAtRet);

    bool isRetVoid = isVoid(ret);

    // invocation
    {
        Instruction I;
        I.data.m_invoke.pad = getAlignPad(m_currentStackSize, alignof(zephyr::impl::invokeMeta));
        I.data.m_invoke.target = method;
        I.data.m_invoke.this_ = meta->implMethod;
        {
            std::vector<SLL> offsets;
            offsets.resize(argCount + (!isRetVoid ? 1 : 0));
            usize cumulativeOffset = 0;
            if (!isRetVoid) {
                auto & so = peekStackObj(0);
                cumulativeOffset -= so.m_value->size();
                offsets[argCount] = cumulativeOffset;
                cumulativeOffset -= so.m_pad;
            }
            for (usize i = 0, l = argCount; i < l; ++i) {
                auto & so = peekStackObj(i + (!isRetVoid ? 1 : 0));
                cumulativeOffset -= so.m_value->size();
                offsets[l - 1 - i] = cumulativeOffset;
                cumulativeOffset -= so.m_pad;
            }
            I.data.m_invoke.argOffsets = meta->getCachedOffsets(std::move(offsets));
        }
        I.fn = [] (const Instruction * i, runtime::ExecutionContext * c) {
            auto & d = i->data.m_invoke;
            zephyr::impl::invokeMeta * im;
            auto stack = c->m_stack;
            {
                auto m = c->m_memory[c->m_currentPage].get();
                auto targetInvocationSpace = d.target->m_maxStackSize + (sizeof(zephyr::impl::alignedNumber) * 2 - 1);
                if (targetInvocationSpace > (ULL)((&(*m)[0] + m->size()) - c->m_stack)) {
                    auto offset = c->m_stack - &(*m)[0];
                    c->allocateStackPage(targetInvocationSpace + sizeof(zephyr::impl::invokeMeta));
                    reinterpret_cast<zephyr::impl::alignedNumber *>(c->m_stack)->a = offset;
                    im = reinterpret_cast<zephyr::impl::invokeMeta *>(c->m_stack + sizeof(zephyr::impl::alignedNumber));
                    c->m_stack += sizeof(zephyr::impl::alignedNumber) + sizeof(zephyr::impl::invokeMeta);
                } else {
                    im = reinterpret_cast<zephyr::impl::invokeMeta *>(c->m_stack + d.pad);
                    c->m_stack += d.pad + sizeof(zephyr::impl::invokeMeta);
                }
            }
            im->argOffsets = d.argOffsets;
            im->argsStartAddr = stack;
            im->retInstAddr = &d.this_->m_instructions[d.thisInstOffset];
            inst_() = &d.target->m_instructions[0];
        };
        putStackObj(invokeMetaType, I.data.m_invoke.pad);
        auto uuid = stageInstruction(I);
        addPatchField_(uuid, data.m_invoke.thisInstOffset, m->findInstructionIdxByUUID(uuid) + 1);
    }

    // handle return

    // deallocate metadata storage
    {
        //usize pad = 0;
        Instruction I;
        // consume invokeMeta storage
        {
            auto so = getStackObj();
            assert_(so.m_value == invokeMetaType, "unexpected stack state");
            //pad += so.m_value->size() + so.m_pad;
            I.data.m_ull_ull.a = so.m_pad;
        }
        // consume ret storage (if exists)
        if (!isRetVoid) {
            auto soRet = getStackObj();
            assert_(soRet.m_value == ret, "unexpected stack state");
            //pad += soRet.m_value->size() + soRet.m_pad;
            I.data.m_ull_ull.b = soRet.m_value->size() + soRet.m_pad;
        } else {
            I.data.m_ull_ull.b = 0;
        }
        //stagePadConsumeInst(pad);
        I.fn = [] (const Instruction * i, runtime::ExecutionContext * c) {
            auto & d = i->data.m_ull_ull;
            if (&(*c->m_memory[c->m_currentPage])[0] == (c->m_stack - sizeof(zephyr::impl::invokeMeta) - sizeof(zephyr::impl::alignedNumber))) {
                auto offset = reinterpret_cast<zephyr::impl::alignedNumber *>(c->m_stack - sizeof(zephyr::impl::invokeMeta) - sizeof(zephyr::impl::alignedNumber))->a;
                c->deallocateStackPage(offset);
                c->m_stack -= d.b;
            } else {
                c->m_stack -= sizeof(zephyr::impl::invokeMeta) + d.a + d.b;
            }
            inst_()++;
        };
        stageInstruction(I);
    }
    // clean args
    {
        for (ULL i = 0, l = argCount; i < l; ++i) { // arg cleanup
            auto so = getStackObj();
            do {
                {
                    Instruction I;
                    auto t = dynamic_cast<const rtti::NativeType *>(so.m_value);
                    if (t != nullptr) {
                        I.data.m_argClean.dtor = meta->getCachedRtti_Dtor(t);
                        I.data.m_argClean.typeSize = t->size();
                        I.data.m_argClean.pad = so.m_pad;
                        I.fn = [] (const Instruction * i, runtime::ExecutionContext * c) {
                            auto & d = i->data.m_argClean;
                            d.dtor->invoke(c->m_stack - d.typeSize);
                            c->m_stack -= d.typeSize + d.pad;
                            inst_()++;
                        };
                        stageInstruction(I);
                        break;
                    }
                }
                assert_(false, "unexpected type");
            } while (0);
        }
    }
    // move return (if exists)
    if (!isRetVoid) {
        do {
            {
                auto t = dynamic_cast<const rtti::NativeType *>(ret);
                if (t != nullptr) {
                    Instruction I;
                    I.data.m_retMove.move = meta->getCachedRtti_Move(t);
                    I.data.m_retMove.pad = getAlignPad(m_currentStackSize, t->align());
                    I.data.m_retMove.typeSize = t->size();
                    I.data.m_retMove.retOffset = stackSizeAtRet - m_currentStackSize;
                    I.fn = [] (const Instruction * i, runtime::ExecutionContext * c) {
                        auto & d = i->data.m_retMove;
                        d.move->invoke(c->m_stack + d.retOffset, c->m_stack + d.pad);
                        c->m_stack += d.typeSize + d.pad;
                        inst_()++;
                    };
                    putStackObj(t, I.data.m_retMove.pad);
                    stageInstruction(I);
                    break;
                }
            }
            assert_(false, "unexpected type");
        } while (0);
    }

    commitInstructions();
    m_instruction++;
}

void emit_invokeLbFn(std::shared_ptr<const rtti::LateBoundFn> lbfn) {
    const rtti::Type * ret;
    std::vector<const rtti::Type *> args;
    {
        ASSERT_NOT_NULL(lbfn);
        auto ret_ = lbfn->ret();
        auto & args_ = lbfn->args();
        ASSERT_NOT_NULL(ret_);
        for (auto & arg : args_) { ASSERT_NOT_NULL(arg); }
        ret = (*meta->infoToType)(ret_);
        args.reserve(args_.size());
        for (auto & x : args_) { args.emplace_back((*meta->infoToType)(x)); }
    }
    auto argCount = args.size();

    for (ULL i = 0, l = argCount; i < l; ++i) {
        assert_(args[i] == peekStackObj(l - 1 - i).m_value, "mismatching arg types");
    }

    for (auto & x : args) { meta->addModuleReference(x); }
    meta->addModuleReference(ret);

    ULL stackSizeAtRet{}; // where the return value will be placed initially after native method is invoked
    emit0_invoke_allocRetStorage(ret, argCount, stackSizeAtRet);

    // invocation
    {
        Instruction I;
        I.data.m_invokeLbFn.fn = meta->getCachedRtti_LbFn(lbfn);
        if (!isVoid(ret) || argCount > 0) {
            std::vector<SLL> offsets;
            offsets.resize(argCount + !isVoid(ret));
            SLL cumulativeOffset = 0;
            for (ULL i = 0, l = offsets.size(); i < l; ++i) {
                auto & so = peekStackObj(i);
                cumulativeOffset -= so.m_value->size();
                offsets[l - 1 - i] = cumulativeOffset;
                cumulativeOffset -= so.m_pad;
            }
            I.data.m_invokeLbFn.argOffsets = meta->getCachedOffsets(std::move(offsets));
            I.fn = [] (const Instruction * i, runtime::ExecutionContext * c) {
                auto & d = i->data.m_invokeLbFn;
                d.fn->invoke(c->m_stack, d.argOffsets);
                inst_()++;
            };
        } else {
            I.data.m_invokeLbFn.argOffsets = nullptr;
            I.fn = [] (const Instruction * i, runtime::ExecutionContext * c) {
                i->data.m_invokeLbFn.fn->invoke(nullptr, nullptr);
                inst_()++;
            };
        }
        stageInstruction(I);
    }

    // return handling
    {
        if (!isVoid(ret)) { // remove return from stack record
            auto so = getStackObj();
            assert_(so.m_value == ret, "mismatching return type");
            stagePadConsumeInst(so.m_pad + so.m_value->size()); // decrement stack, value still persists
        }
        for (ULL i = 0, l = argCount; i < l; ++i) { // arg cleanup
            auto so = getStackObj();
            do {
                {
                    Instruction I;
                    auto t = dynamic_cast<const rtti::NativeType *>(so.m_value);
                    if (t != nullptr) {
                        I.data.m_argClean.dtor = meta->getCachedRtti_Dtor(t);
                        I.data.m_argClean.typeSize = t->size();
                        I.data.m_argClean.pad = so.m_pad;
                        I.fn = [] (const Instruction * i, runtime::ExecutionContext * c) {
                            auto & d = i->data.m_argClean;
                            d.dtor->invoke(c->m_stack - d.typeSize);
                            c->m_stack -= d.typeSize + d.pad;
                            inst_()++;
                        };
                        stageInstruction(I);
                        break;
                    }
                }
                assert_(false, "unexpected type");
            } while (0);
        }
        if (!isVoid(ret)) { // move ret value to stack
            do {
                {
                    auto t = dynamic_cast<const rtti::NativeType *>(ret);
                    if (t != nullptr) {
                        Instruction I;
                        I.data.m_retMove.move = meta->getCachedRtti_Move(t);
                        auto pad = I.data.m_retMove.pad = getAlignPad(m_currentStackSize, t->align());
                        I.data.m_retMove.typeSize = t->size();
                        I.data.m_retMove.retOffset = stackSizeAtRet - m_currentStackSize;
                        I.fn = [] (const Instruction * i, runtime::ExecutionContext * c) {
                            auto & d = i->data.m_retMove;
                            d.move->invoke(c->m_stack + d.retOffset, c->m_stack + d.pad);
                            c->m_stack += d.typeSize + d.pad;
                            inst_()++;
                        };
                        putStackObj(t, pad);
                        stageInstruction(I);
                        break;
                    }
                }
                assert_(false, "unexpected type");
            } while (0);
        }
    }

    commitInstructions();
    m_instruction++;
}

void emit0_invoke_allocRetStorage(const rtti::Type * ret, ULL argCount, ULL & stackSizeAtRet) {
    if (!isVoid(ret)) {
        ULL stackSizeRetTarget = m_currentStackSize; // where the return value will be moved to before this opcode is finished
        for (ULL i = 0, l = argCount; i < l; ++i) { // get stack size at &arg[0]
            auto & so = peekStackObj(l - 1 - i);
            stackSizeRetTarget -= so.m_value->size() + so.m_pad;
        }
        stackSizeRetTarget += getAlignPad(stackSizeRetTarget, ret->align()); // align for return type
        auto pad = getAlignPad(m_currentStackSize, ret->align());
        stackSizeAtRet = m_currentStackSize + pad;
        if (ret->size() > stackSizeAtRet - stackSizeRetTarget) { // move will overlap
            pad = (stackSizeRetTarget + ret->size()) - stackSizeAtRet;
            pad += getAlignPad(m_currentStackSize + pad, ret->align()); // useless?
            stackSizeAtRet = m_currentStackSize + pad;
        }
        stagePadInst(pad + ret->size());
        putStackObj(ret, pad);
    }
}
