void emit_dup() {
    auto & so = peekStackObj();
    assert_(getAlignPad(m_currentStackSize, so.m_value->align()) == 0, "unexpected type size");
    meta->addModuleReference(so.m_value);
    do {
        {
            auto t = dynamic_cast<const rtti::NativeType *>(so.m_value);
            if (t != nullptr) {
                Instruction I;
                I.data.m_dup.typeSize = t->size();
                I.data.m_dup.copy = meta->getCachedRtti_Copy(t);
                I.fn = [] (const Instruction * i, runtime::ExecutionContext * c) {
                    auto & d = i->data.m_dup;
                    d.copy->invoke(c->m_stack - d.typeSize, c->m_stack);
                    c->m_stack += d.typeSize;
                    inst_()++;
                };
                stageInstruction(I);
                putStackObj(so.m_value, 0);
                break;
            }
        }
        assert_(false, "unexpected type");
    } while (0);
    commitInstructions();
    m_instruction++;
}
