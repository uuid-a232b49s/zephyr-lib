/*void emit_ldArg(ULL idx, OpCode::LoadMode mode) {
    assert_(mode == OpCode::LoadMode::copy, "move mode unsupported for ldArg");
    auto t_ = (*meta->toRtType)(meta->srcMethod->m_args[idx].get());
    meta->addModuleReference(t_);
    auto pad = getAlignPad(m_currentStackSize, t_->align());
    do {
        {
            auto t = dynamic_cast<const rtti::NativeType *>(t_);
            if (t != nullptr) {
                Instruction I;
                I.data.m_ldArg.typeSize = t->size();
                ULL stackSize0 = m_currentStackSize + pad;
                I.data.m_ldArg.argIdx = idx;
                I.data.m_ldArg.pad = pad;
                I.data.m_ldArg.copy = meta->getCachedRtti_Copy(t);
                I.fn = [] (const Instruction * i, runtime::ExecutionContext * c) {
                    auto & d = i->data.m_ldArg;
                    auto im = reinterpret_cast<invokeMeta *>(c->m_stack - d.offsetTo0 - sizeof(invokeMeta));
                    d.copy->invoke(im->argsStartAddr + im->argOffsets[d.argIdx], c->m_stack + d.pad);
                    c->m_stack += d.typeSize + d.pad;
                    inst_()++;
                };
                auto iUuid = stageInstruction(I);
                putStackObj(t, pad);
                commitInstructions();
                addPatchField_(iUuid, data.m_ldArg.offsetTo0, stackSize0 + m->m_locals.size());
                break;
            }
        }
        assert_(false, "unexpected type");
    } while (0);
    m_instruction += 1;
}*/
