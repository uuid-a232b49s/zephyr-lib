void emit_stLoc(ULL idx) {
    auto stackSize0 = m_currentStackSize;
    auto so = getStackObj();

    auto & loc = getLocal(idx);
    auto & eloc = meta->m_locals.m_locals[loc.m_emittedLocalIdx];
    assert_(eloc.type == so.m_value, "illegal stLoc: type mismatch");

    meta->addModuleReference(eloc.type);

    do {
        {
            auto t = dynamic_cast<const rtti::NativeType *>(so.m_value);
            if (t != nullptr) {
                if (loc.m_init) { // clean previous value
                    Instruction I;
                    I.data.m_locClean.dtor = meta->getCachedRtti_Dtor(t);
                    I.fn = [] (const Instruction * i, runtime::ExecutionContext * c) {
                        auto & d = i->data.m_locClean;
                        d.dtor->invoke(c->m_stack - d.offsetToLoc);
                        inst_()++;
                    };
                    auto iUuid = stageInstruction(I);
                    addPatchField_(iUuid, data.m_locClean.offsetToLoc, (stackSize0 + m->m_locals.size()) - m->getLocalOffsetFrom0(idx));
                }
                {
                    Instruction I;
                    I.data.m_stLoc.move = meta->getCachedRtti_Move(t);
                    I.data.m_stLoc.typeSize = so.m_value->size();
                    I.data.m_stLoc.pad = so.m_pad;
                    I.fn = [] (const Instruction * i, runtime::ExecutionContext * c) {
                        auto & d = i->data.m_stLoc;
                        d.move->invoke(c->m_stack - d.typeSize, c->m_stack - d.offsetToLoc);
                        c->m_stack -= d.typeSize + d.pad;
                        inst_()++;
                    };
                    auto iUuid = stageInstruction(I);
                    addPatchField_(iUuid, data.m_stLoc.offsetToLoc, (stackSize0 + m->m_locals.size()) - m->getLocalOffsetFrom0(idx));
                }
                break;
            }
        }
        assert_(false, "unexpected type");
    } while (0);
    loc.m_init = true;
    commitInstructions();
    m_instruction++;
}
