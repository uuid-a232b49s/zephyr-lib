void emit_ret() {
    instUuid retInstUuid;
    auto stackSize = m_currentStackSize;
    if (!isVoid(meta->srcMethod->m_ret.get())) {
        auto so = getStackObj();
        assert_((*meta->toRtType)(meta->srcMethod->m_ret.get()) == so.m_value, "return type is different from stack type");
        assert_(so.m_pad == 0, "unexpected pad");
        assert_(m_stack.empty(), "stack not empty upon return");
        for (auto & x : m_locals) {
            assert_(!x.m_init, "acctive locals present");
        }
        meta->addModuleReference(so.m_value);
        do {
            {
                auto t = dynamic_cast<const rtti::NativeType *>(so.m_value);
                if (t != nullptr) {
                    Instruction I;
                    I.data.m_ret.typeSize = t->size();
                    I.data.m_ret.argIdx = meta->srcMethod->m_args.size();
                    I.data.m_ret.move = meta->getCachedRtti_Move(t);
                    I.fn = [] (const Instruction * i, runtime::ExecutionContext * c) {
                        auto & d = i->data.m_ret;
                        auto im = reinterpret_cast<invokeMeta *>(c->m_stack - d.offsetTo0 - sizeof(invokeMeta));
                        d.move->invoke(c->m_stack - d.typeSize, im->argsStartAddr + im->argOffsets[d.argIdx]);
                        c->m_stack -= d.offsetTo0;
                        inst_() = im->retInstAddr;
                    };
                    //if (so.m_pad > 0) { throw_("unexpected pad"); /*emitPadConsumeInst(so.m_pad);*/ } // shouldnt ever happen
                    retInstUuid = stageInstruction(I);
                    break;
                }
            }
            throw_("unexpected type");
        } while (0);
    } else {
        //assert_(false, "not supported");
        assert_(m_stack.empty(), "stack not empty upon return");
        Instruction I;
        I.data.m_ret.argIdx = meta->srcMethod->m_args.size();
        I.fn = [] (const Instruction * i, runtime::ExecutionContext * c) {
            auto & d = i->data.m_ret;
            auto im = reinterpret_cast<invokeMeta *>(c->m_stack - d.offsetTo0 - sizeof(invokeMeta));
            c->m_stack -= d.offsetTo0;
            inst_() = im->retInstAddr;
        };
        //if (so.m_pad > 0) { throw_("unexpected pad"); /*emitPadConsumeInst(so.m_pad);*/ } // shouldnt ever happen
        retInstUuid = stageInstruction(I);
    }
    commitInstructions();
    addPatchField_(retInstUuid, data.m_ret.offsetTo0, stackSize + m->m_locals.size());
    m_instruction = -1;
}
