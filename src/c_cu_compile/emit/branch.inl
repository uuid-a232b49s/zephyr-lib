void emit_branch(ULL instruction, bool branchOnTrue, bool unconditional) {
    bool emitted = false;
    {
        ULL _;
        if (meta->tryFindInstructionIdxByUUID(instUuid{ m_instruction, 0 }, _)) {
            emitted = true;
        }
    }

    assert_(instruction != ~0ull, "branch label not marked");
    assert_(instruction < meta->srcMethod->m_opcodes.size(), "illegal branch label");
    stackObject so{ nullptr, 0 };
    if (!unconditional) {
        so = getStackObj();
        meta->addModuleReference(so.m_value);
    }
    {
        Instruction I;
        if (!unconditional) {
            do {
                {
                    auto t = dynamic_cast<const rtti::NativeType *>(so.m_value);
                    if (t != nullptr) {
                        I.data.m_branch.typeSize = t->size();
                        I.data.m_branch.pad = so.m_pad;
                        I.data.m_branch.toBool = meta->getCachedRtti_ToBool(t);
#define impl_a_(conditionMatch) \
                        [] (const Instruction * i, runtime::ExecutionContext * c) {\
                            auto & d = i->data.m_branch;\
                            bool condition = d.toBool->invoke(c->m_stack - d.typeSize);\
                            c->m_stack -= d.typeSize + d.pad;\
                            inst_() += (condition == conditionMatch) ? d.jumpDelta : 1;\
                        };
                        if (branchOnTrue) {
                            I.fn = impl_a_(true);
                        } else {
                            I.fn = impl_a_(false);
                        }
#undef impl_a_
                        break;
                    }
                }
                throw_("unexpected type");
            } while (0);
        } else {
            I.fn = [] (const Instruction * i, runtime::ExecutionContext * c) {
                inst_() += i->data.m_branch.jumpDelta;
            };
        }
        auto u = stageInstruction(I);
        commitInstructions();
        meta->addPatch([=] (methodMeta * m) {
            auto i = m->findInstructionIdxByUUID(u);
            auto & I = m->m_instructions[i];
            ULL Ja, Jb; m->findInstructionsByIndex(instruction, Ja, Jb);
            applyPatch(I, I.inst.data.m_branch.jumpDelta, (SLL)Ja - (SLL)i, m_instruction);
        });
        if (!unconditional) {
            if (!emitted) {
                auto b = std::unique_ptr<emittingBranch>(new emittingBranch(*this));
                b->m_instruction = !branchOnTrue ? instruction : (m_instruction + 1);;
                meta->m_branches.emplace_back(std::move(b));
            }
            m_instruction = branchOnTrue ? instruction : (m_instruction + 1);
        } else {
            m_instruction = instruction;
        }
        if (emitted) { m_instruction = -1; }
    }
}
