void emit_clLoc(ULL idx) {
    auto stackSize0 = m_currentStackSize;

    auto & loc = getLocal(idx);
    auto & eloc = meta->m_locals.m_locals[loc.m_emittedLocalIdx];
    assert_(loc.m_init, "illegal clLoc: local already deinitialized");
    meta->addModuleReference(eloc.type);

    do {
        {
            auto t = dynamic_cast<const rtti::NativeType *>(eloc.type);
            if (t != nullptr) {
                Instruction I;
                I.data.m_locClean.dtor = meta->getCachedRtti_Dtor(t);
                I.fn = [] (const Instruction * i, runtime::ExecutionContext * c) {
                    auto & d = i->data.m_locClean;
                    d.dtor->invoke(c->m_stack - d.offsetToLoc);
                    inst_()++;
                };
                auto iUuid = stageInstruction(I);
                addPatchField_(iUuid, data.m_locClean.offsetToLoc, (stackSize0 + m->m_locals.size()) - m->getLocalOffsetFrom0(idx));
                break;
            }
        }
        assert_(false, "unexpected type");
    } while (0);
    loc.m_init = false;
    commitInstructions();
    m_instruction++;
}
