void emit_rem() {
    auto so = getStackObj();
    meta->addModuleReference(so.m_value);
    do {
        {
            auto t = dynamic_cast<const rtti::NativeType *>(so.m_value);
            if (t != nullptr) {
                Instruction I;
                I.data.m_rem.typeSize = t->size();
                I.data.m_rem.pad = so.m_pad;
                I.data.m_rem.dtor = meta->getCachedRtti_Dtor(t);
                I.fn = [] (const Instruction * i, runtime::ExecutionContext * c) {
                    auto & d = i->data.m_rem;
                    d.dtor->invoke(c->m_stack - d.typeSize);
                    c->m_stack -= d.typeSize + d.pad;
                    inst_()++;
                };
                stageInstruction(I);
                break;
            }
        }
        assert_(false, "unexpected type");
    } while (0);
    commitInstructions();
    m_instruction++;
}
