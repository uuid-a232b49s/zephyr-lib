#include <utility>

#include "zephyr/rtti/visitor.hpp"
#include "zephyr/compilation/method.hpp"
#include "c_compilation.hxx"
#include "throw_.hxx"
#include "common.hxx"

namespace zephyr {
    namespace compilation {

        std::shared_ptr<const rtti::Module> Method::impl::cMethod::module() const { return nullptr; }

        usize Method::impl::cMethod::argc() const { return m_args.size(); }

        std::shared_ptr<const rtti::Type> Method::impl::cMethod::argv(usize index) const { return m_args.at(index); }

        void Method::impl::cMethod::argc(usize count) { m_args.resize(count); }

        void Method::impl::cMethod::argv(usize index, const std::shared_ptr<const rtti::Type> type) {
            ASSERT_NOT_NULL(type);
            m_args.at(index) = std::move(type);
        }

        std::shared_ptr<const rtti::Type> Method::impl::cMethod::ret() const { return m_ret; }

        void Method::impl::cMethod::emit(OpCode c) { m_opcodes.emplace_back(std::move(c)); }

        LocalRef Method::impl::cMethod::local(std::shared_ptr<const rtti::Type> type) {
            ASSERT_TRUE(type != nullptr);
            LocalRef r;
            r.m_locIdx = m_locals.size();
            m_locals.emplace_back(std::move(type));
            return r;
        }

        const std::string & Method::impl::cMethod::identifier() const { return m_identifier; }

        usize Method::impl::cMethod::maxStackSize() const { ASSERT_(false, zephyr::Exception, "not supported"); }

        std::shared_ptr<void> Method::impl::cMethod::accept(rtti::Visitor * v) const { return v->visit(m_this.lock()); }

        void Method::impl::cMethod::identifier(const std::string & i) { m_identifier = i; }

        void Method::impl::cMethod::args(const std::vector<std::shared_ptr<const rtti::Type>> & args) {
            for (auto & arg : args) { ASSERT_NOT_NULL(arg); }
            m_args = args;
        }

        void Method::impl::cMethod::ret(std::shared_ptr<const rtti::Type> ret) {
            ASSERT_NOT_NULL(ret);
            m_ret = std::move(ret);
        }

        void Method::impl::cMethod::clear() {
            m_args.clear();
            m_ret.reset();
            m_locals.clear();
            m_opcodes.clear();
            m_labels.clear();
            m_identifier.clear();
        }

        std::shared_ptr<Method::impl::cMethod> Method::impl::cMethod::new_() {
            auto p = std::shared_ptr<Method::impl::cMethod>(new cMethod());
            p->m_this = p;
            return p;
        }

        LabelRef Method::impl::cMethod::label() {
            LabelRef r;
            r.m_method = this;
            r.m_lblIdx = m_labels.size();
            m_labels.emplace_back(~0ull);
            return r;
        }

        void LabelRef::mark() {
            ASSERT_TRUE(m_method != nullptr);
            ASSERT_TRUE(m_lblIdx != ~0ull);
            auto method = static_cast<Method::impl::cMethod *>(m_method);
            ASSERT_TRUE(method->m_labels[m_lblIdx] == ~0ull);
            method->m_labels[m_lblIdx] = method->m_opcodes.size();
        }

    }
}
