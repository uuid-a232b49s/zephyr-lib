#ifndef HEADER_ZEPHYR_SRC_COMMON
#define HEADER_ZEPHYR_SRC_COMMON 1

#ifndef NDEBUG
#include <iostream>
#define x_print(expr) std::cerr << expr << std::endl
#else
#define x_print(expr)
#endif

#include "../include/zephyr/def.hpp"

namespace zephyr {

    using ULL = usize;
    using SLL = isize;
    using UC = unsigned char;

}

#endif
