#include <utility>
#include <typeinfo>

#include "zephyr/rtti/value.hpp"
#include "zephyr/rtti/descriptor.hpp"
#include "zephyr/compilation/dataprovider.hpp"
#include "throw_.hxx"

namespace zephyr {
    namespace compilation {

#define impl_all_ impl_2_(char) impl_2_(short) impl_2_(int) impl_2_(long) impl_2_(long long) impl_1_(float) impl_1_(double) impl_1_(long double) impl_1_(bool)
#define impl_2_(type) impl_1_(signed type) impl_1_(unsigned type)

        const std::type_info & DataProvider::typeinfo(const std::string & identifier) const {
#define impl_1_(type) if (identifier == #type) { return typeid(type); }
            impl_all_;
            impl_1_(void);
#undef impl_1_
            ASSERT_(false, zephyr::AssertionException, "type not supported");
        }

        std::string DataProvider::identifier(const std::type_info & typeinfo) const {
#define impl_1_(type) if (typeinfo == typeid(type)) { return #type; }
            impl_all_;
            impl_1_(void);
#undef impl_1_
            ASSERT_(false, zephyr::AssertionException, "type not supported");
        }

        std::shared_ptr<const rtti::RttiDescriptor> DataProvider::rtti(const std::type_info & typeinfo) const {
#define impl_1_(type) if (typeinfo == typeid(type)) { return rtti::RttiDescriptorGeneric<type>::Instance(); }
            impl_all_;
            impl_1_(void);
#undef impl_1_
            ASSERT_(false, zephyr::AssertionException, "type not supported");
        }

        std::unique_ptr<rtti::Value> DataProvider::value(const std::type_info & typeinfo) const {
#define impl_1_(type) if (typeinfo == typeid(type)) { return std::unique_ptr<rtti::Value> { new rtti::ValueImpl<type>({}) }; }
            impl_all_;
#undef impl_1_
            ASSERT_(false, zephyr::AssertionException, "type not supported");
        }

#undef impl_2_
#undef impl_all_

        std::shared_ptr<const rtti::LateBoundFn> DataProvider::function(const std::string & identifier) const {
            throw EXC(zephyr::Exception, "function not found");
        }

        std::string DataProvider::identifier(const std::shared_ptr<const rtti::LateBoundFn> & function) const {
            throw EXC(zephyr::Exception, "function not found");
        }

    }
}

namespace zephyr {
    namespace compilation {

        const std::type_info & MappedDataProvider::typeinfo(const std::string & identifier) const {
            auto i = m_idToTi.find(identifier);
            return i != m_idToTi.end() ? *i->second : DataProvider::typeinfo(identifier);
        }

        std::string MappedDataProvider::identifier(const std::type_info & ti) const {
            auto i = m_tiToData.find(std::type_index(ti));
            return i != m_tiToData.end() ? *i->second.m_identifier : DataProvider::identifier(ti);
        }

        std::shared_ptr<const rtti::RttiDescriptor> MappedDataProvider::rtti(const std::type_info & ti) const {
            auto i = m_tiToData.find(std::type_index(ti));
            return i != m_tiToData.end() && i->second.m_descriptor ? i->second.m_descriptor : DataProvider::rtti(ti);
        }

        std::unique_ptr<rtti::Value> MappedDataProvider::value(const std::type_info & ti) const {
            auto i = m_tiToData.find(std::type_index(ti));
            return i != m_tiToData.end() && i->second.m_newValue ? i->second.m_newValue() : DataProvider::value(ti);
        }

        std::shared_ptr<const rtti::LateBoundFn> MappedDataProvider::function(const std::string & id) const {
            auto i = m_IdToFn.find(id);
            return i != m_IdToFn.end() ? *i->second : DataProvider::function(id);
        }

        std::string MappedDataProvider::identifier(const std::shared_ptr<const rtti::LateBoundFn> & fn) const {
            auto i = m_fnToId.find(fn);
            return i != m_fnToId.end() ? *i->second : DataProvider::identifier(fn);
        }

        void MappedDataProvider::registerType(
            std::string identifier,
            const std::type_info & ti,
            std::shared_ptr<const rtti::RttiDescriptor> descriptor,
            std::function<std::unique_ptr<rtti::Value>(void)> newValue
        ) {
            ASSERT_(m_tiToData.count(ti) == 0, zephyr::AssertionException, "specified typeinfo is already registered");
            ASSERT_(m_idToTi.count(identifier) == 0, zephyr::AssertionException, "specified identifier is already registered");
            ASSERT_(!descriptor || descriptor->typeinfo() == ti, zephyr::AssertionException, "descriptor doesnt match typeinfo");
            auto i = &m_idToTi.insert(std::make_pair(std::move(identifier), &ti)).first->first;
            auto & d = m_tiToData[std::type_index(ti)];
            d.m_identifier = i;
            d.m_descriptor = std::move(descriptor);
            d.m_newValue = std::move(newValue);
        }

        const std::shared_ptr<const rtti::LateBoundFn> & MappedDataProvider::registerFunction(std::string identifier, std::shared_ptr<const rtti::LateBoundFn> fn) {
            ASSERT_(m_fnToId.count(fn) == 0, zephyr::AssertionException, "duplicate function pointer");
            ASSERT_(m_IdToFn.count(identifier) == 0, zephyr::AssertionException, "duplicate function identifier");
            auto fnToId = m_fnToId.emplace(std::move(fn), nullptr);
            auto idToFn = m_IdToFn.emplace(std::move(identifier), nullptr);
            fnToId.first->second = &idToFn.first->first;
            idToFn.first->second = &fnToId.first->first;
            return fnToId.first->first;
        }

    }
}

namespace zephyr {
    namespace compilation {

        std::shared_ptr<const rtti::Module> MappedModuleProvider::find(const std::string & identifier) const {
            auto i = map.find(identifier);
            ASSERT_(i != map.end(), Exception, "module not found");
            return i->second;
        }

    }
}
