#ifndef HEADER_ZEPHYR_SRC_R_TYPE
#define HEADER_ZEPHYR_SRC_R_TYPE 1

#include <memory>
#include <utility>
#include <typeinfo>

#include "../include/zephyr/rtti/type.hpp"
#include "rtti_type.hxx"

namespace zephyr {
    namespace runtime {
        namespace impl {

            struct TypeImpl : virtual rtti::Type {
                std::weak_ptr<rtti::Module> m_module;
                std::string m_identifier;
                rtti::impl::addressTypeMgr m_addrMgr;
                virtual const std::string & identifier() const override { return m_identifier; }
                virtual std::shared_ptr<const rtti::Module> module() const override { return m_module.lock(); }
                virtual std::shared_ptr<const rtti::AddressType> address(rtti::AddressConstraints) const override;
            };

            struct NativeTypeImpl : virtual TypeImpl, virtual rtti::NativeType {
                std::shared_ptr<const rtti::RttiDescriptor> m_rtti;
                virtual usize size() const override;
                virtual usize align() const override;
                virtual std::shared_ptr<const rtti::RttiDescriptor> rtti() const override { return m_rtti; }
                virtual const std::string & identifier() const override { return TypeImpl::identifier(); } // inheritance dominance warning
                virtual std::shared_ptr<const rtti::Module> module() const override { return TypeImpl::module(); } // ^^
                virtual std::shared_ptr<const rtti::AddressType> address(rtti::AddressConstraints x) const override { return TypeImpl::address(std::move(x)); } // ^^
                virtual std::shared_ptr<void> accept(rtti::Visitor * v) const override;
            };

            bool compareTypes(const rtti::Type *, const std::type_info *);

        }
    }
}

#endif
