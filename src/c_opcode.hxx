#ifndef HEADER_ZEPHYR_SRC_C_OPCODE
#define HEADER_ZEPHYR_SRC_C_OPCODE 1

#include <map>
#include <vector>
#include <memory>
#include <istream>
#include <ostream>
#include <functional>

#include "zephyr/compilation/opcode.hpp"
#include "crc.hxx"

namespace zephyr {
    namespace compilation {
        struct DataProvider;
        struct Method;
    }
}

namespace zephyr {
    namespace compilation {

        struct OpCode::impl_ {

            static void serialize(
                const OpCode & c,
                std::ostream & out,
                const DataProvider & infoProvider,
                const std::function<usize(const rtti::Value *)> & getValueIdx,
                const std::function<void(const rtti::Type *)> & writeTypeRef,
                const std::function<void(const rtti::Method *)> & writeMethodRef,
                bool binary,
                zephyr::impl::crc32_t & crc
            );

            static OpCode deserialize(
                std::istream & in,
                compilation::Method * currentMethod,
                const DataProvider & infoProvider,
                const std::function<std::shared_ptr<const rtti::Value>(usize)> & getValue,
                const std::function<std::shared_ptr<const rtti::Type>(void)> & readTypeRef,
                const std::function<std::shared_ptr<const rtti::Method>(void)> & readMethodRef,
                bool binary,
                zephyr::impl::crc32_t & crc
            );

        };

    }
}

#endif
