#include <utility>

#include "zephyr/compilation/opcode.hpp"
#include "zephyr/compilation/method.hpp"
#include "zephyr/compilation/dataprovider.hpp"
#include "zephyr/rtti/module.hpp"
#include "zephyr/rtti/value.hpp"
#include "primitive_serialization.hxx"
#include "c_opcode.hxx"
#include "throw_.hxx"

namespace zephyr {
    namespace compilation {

        OpCode OpCode::ldArg(usize idx, LoadMode mode) noexcept {
            OpCode c;
            c.m_operator = Operator::ldArg;
            c.m_data.m_ull_b.i = idx;
            c.m_data.m_ull_b.b = static_cast<usize>(mode);
            return c;
        }

        OpCode OpCode::ldConst(std::shared_ptr<const rtti::Value> value) {
            OpCode c;
            c.m_operator = Operator::ldConst;
            c.m_data.value = std::move(value);
            return c;
        }

        OpCode OpCode::ldLoc(LocalRef local, LoadMode mode) noexcept {
            OpCode c;
            c.m_operator = Operator::ldLoc;
            c.m_data.m_ull_b.i = local.m_locIdx;
            c.m_data.m_ull_b.b = static_cast<usize>(mode);
            return c;
        }

        OpCode OpCode::stLoc(LocalRef local) noexcept {
            OpCode c;
            c.m_operator = Operator::stLoc;
            c.m_data.ull = local.m_locIdx;
            return c;
        }

        OpCode OpCode::clLoc(LocalRef local) noexcept {
            OpCode c;
            c.m_operator = Operator::clLoc;
            c.m_data.ull = local.m_locIdx;
            return c;
        }

        OpCode OpCode::dup() noexcept {
            OpCode c;
            c.m_operator = Operator::dup;
            return c;
        }

        OpCode OpCode::rem() noexcept {
            OpCode c;
            c.m_operator = Operator::rem;
            return c;
        }

        OpCode OpCode::ret() noexcept {
            OpCode c;
            c.m_operator = Operator::ret;
            return c;
        }

        OpCode OpCode::ref() noexcept {
            OpCode c;
            c.m_operator = Operator::ref;
            return c;
        }

        OpCode OpCode::deref() noexcept {
            OpCode c;
            c.m_operator = Operator::deref;
            return c;
        }

        OpCode OpCode::invoke(std::shared_ptr<const rtti::Method> method) noexcept {
            OpCode c;
            c.m_operator = Operator::invoke;
            c.m_data.method = std::move(method);
            return c;
        }

        OpCode OpCode::invoke(std::shared_ptr<const rtti::LateBoundFn> lbfn) {
            OpCode c;
            c.m_operator = Operator::invokeLbFn;
            c.m_data.lbfn = std::move(lbfn);
            return c;
        }

        OpCode OpCode::jmp(LabelRef label) noexcept {
            OpCode c;
            c.m_operator = Operator::jmp;
            c.m_data.ull = label.m_lblIdx;
            return c;
        }

        OpCode OpCode::branch(LabelRef label, bool condition) noexcept {
            OpCode c;
            c.m_operator = Operator::branch;
            c.m_data.m_ull_b.i = label.m_lblIdx;
            c.m_data.m_ull_b.b = condition ? 1 : 0;
            return c;
        }

    }
}

namespace zephyr {
    namespace compilation {

#define assert_(c, m) ASSERT_(c, SerializationException, m);

        void OpCode::impl_::serialize(
            const OpCode & c,
            std::ostream & out,
            const DataProvider & infoProvider,
            const std::function<usize(const rtti::Value *)> & getValueIdx,
            const std::function<void(const rtti::Type *)> & writeTypeRef,
            const std::function<void(const rtti::Method *)> & writeMethodRef,
            bool binary,
            zephyr::impl::crc32_t & crc
        ) {
            if (binary) {
                using namespace zephyr::impl;
                writeUInt8(out, static_cast<std::uint8_t>(c.m_operator), crc);
                switch (c.m_operator) {
                    case OpCode::Operator::null: break;
                    case OpCode::Operator::ldArg:
                    case OpCode::Operator::ldLoc:
                        writeUInt64(out, c.m_data.m_ull_b.i, crc);
                        writeUInt8(out, static_cast<std::uint8_t>(static_cast<OpCode::LoadMode>(c.m_data.m_ull_b.b)), crc);
                        break;
                    case OpCode::Operator::ldConst:
                        writeUInt64(out, c.m_data.value ? getValueIdx(c.m_data.value.get()) : ~0ULL, crc);
                        break;
                    case OpCode::Operator::stLoc:
                    case OpCode::Operator::clLoc:
                        writeUInt64(out, c.m_data.ull, crc);
                        break;
                    case OpCode::Operator::dup:
                    case OpCode::Operator::rem:
                    case OpCode::Operator::ret:
                    case OpCode::Operator::ref:
                    case OpCode::Operator::deref:
                        break;
                    case OpCode::Operator::invoke:
                        if (c.m_data.method) {
                            writeUInt8(out, 1, crc);
                            writeMethodRef(c.m_data.method.get());
                        } else {
                            writeUInt8(out, 0, crc);
                        }
                        break;
                    case OpCode::Operator::invokeLbFn:
                        if (c.m_data.lbfn) {
                            writeUInt8(out, 1, crc);
                            writeString(out, infoProvider.identifier(c.m_data.lbfn), crc);
                        } else {
                            writeUInt8(out, 0, crc);
                        }
                        break;
                    case OpCode::Operator::jmp:
                        writeUInt64(out, c.m_data.ull, crc);
                        break;
                    case OpCode::Operator::branch:
                        writeUInt64(out, c.m_data.m_ull_b.i, crc);
                        writeUInt8(out, c.m_data.m_ull_b.b == 1 ? 1 : 0, crc);
                        break;
                    default: assert_(false, "opcode not supported");
                }
            } else {
                assert_(false, "not implemented");
            }
        }

        OpCode OpCode::impl_::deserialize(
            std::istream & in,
            compilation::Method * currentMethod,
            const DataProvider & infoProvider,
            const std::function<std::shared_ptr<const rtti::Value>(usize)> & getValue,
            const std::function<std::shared_ptr<const rtti::Type>(void)> & readTypeRef,
            const std::function<std::shared_ptr<const rtti::Method>(void)> & readMethodRef,
            bool binary,
            zephyr::impl::crc32_t & crc
        ) {
            if (binary) {
                using namespace zephyr::impl;
                auto op = static_cast<OpCode::Operator>(readUInt8(in, crc));
                switch (op) {
                    case OpCode::Operator::null: return OpCode();
                    case OpCode::Operator::ldArg: {
                        auto i = readUInt64(in, crc);
                        auto b = static_cast<OpCode::LoadMode>(readUInt8(in, crc));
                        return OpCode::ldArg(i, b);
                    }
                    case OpCode::Operator::ldConst: {
                        auto i = readUInt64(in, crc);
                        if (i == ~0ULL) {
                            return OpCode::ldConst(nullptr);
                        } else {
                            return OpCode::ldConst(getValue(i));
                        }
                    }
                    case OpCode::Operator::ldLoc: {
                        LocalRef l;
                        l.m_locIdx = readUInt64(in, crc);
                        auto b = static_cast<OpCode::LoadMode>(readUInt8(in, crc));
                        return OpCode::ldLoc(std::move(l), b);
                    }
                    case OpCode::Operator::stLoc: {
                        LocalRef l;
                        l.m_locIdx = readUInt64(in, crc);
                        return OpCode::stLoc(l);
                    }
                    case OpCode::Operator::clLoc: {
                        LocalRef l;
                        l.m_locIdx = readUInt64(in, crc);
                        return OpCode::clLoc(l);
                    }
                    case OpCode::Operator::dup: return OpCode::dup();
                    case OpCode::Operator::rem: return OpCode::rem();
                    case OpCode::Operator::ret: return OpCode::ret();
                    case OpCode::Operator::ref: return OpCode::ref();
                    case OpCode::Operator::deref: return OpCode::deref();
                    case OpCode::Operator::invoke:
                        if (readUInt8(in, crc)) {
                            return OpCode::invoke(readMethodRef());
                        } else {
                            return OpCode::invoke(rtti::MethodPtr());
                        }
                    case OpCode::Operator::invokeLbFn:
                        if (readUInt8(in, crc)) {
                            return OpCode::invoke(infoProvider.function(readString(in, crc)));
                        } else {
                            return OpCode::invoke(decltype(Data::lbfn)());
                        }
                    case OpCode::Operator::jmp: {
                        LabelRef l;
                        l.m_lblIdx = readUInt64(in, crc);
                        l.m_method = currentMethod;
                        return OpCode::jmp(std::move(l));
                    }
                    case OpCode::Operator::branch: {
                        LabelRef l;
                        l.m_lblIdx = readUInt64(in, crc);
                        l.m_method = currentMethod;
                        bool b = readUInt8(in, crc) == 1;
                        return OpCode::branch(std::move(l), b);
                    }
                    default: assert_(false, "opcode not supported");
                }
            } else {
                assert_(false, "not implemented");
            }
        }

    }
}
