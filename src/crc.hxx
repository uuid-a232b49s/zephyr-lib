#ifndef HEADER_ZEPHYR_SRC_CRC
#define HEADER_ZEPHYR_SRC_CRC 1

#include <array>
#include <cstdint>
#include <numeric>

namespace zephyr {
    namespace impl {

        using crc32_t = std::uint_fast32_t;

        extern const std::uint_fast32_t crc32_table[256];

        template<typename begin_t, typename end_t>
        inline crc32_t crc32(begin_t begin, end_t end, crc32_t seed = 0) {
            return crc32_t { 0xFFFFFFFFul } &
                ~std::accumulate(
                    begin,
                    end,
                    crc32_t { 0xFFFFFFFFul } & crc32_t { ~seed },
                    [] (crc32_t checksum, std::uint_fast8_t value) { return crc32_table[(checksum ^ value) & 0xFFu] ^ (checksum >> 8); }
                );
        }

    }
}

#endif
