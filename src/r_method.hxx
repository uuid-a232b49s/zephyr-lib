#ifndef HEADER_ZEPHYR_SRC_R_METHOD
#define HEADER_ZEPHYR_SRC_R_METHOD 1

#include <memory>
#include <vector>
#include <typeinfo>

#include "../include/zephyr/rtti/method.hpp"
#include "rt_compilation.hxx"
#include "common.hxx"

namespace zephyr {
    namespace rtti {
        template<typename>
        struct AbstractFn;
        struct Value;
    }
}

namespace zephyr {
    namespace runtime {
        namespace impl {

            struct MethodImpl : rtti::Method {
                std::weak_ptr<rtti::Module> m_module;
                std::vector<std::shared_ptr<const rtti::AbstractFn<void>>> m_rttiFnData;
                std::vector<std::shared_ptr<const rtti::Value>> m_constants;
                std::vector<std::unique_ptr<std::vector<SLL>>> m_offsets;
                std::vector<zephyr::impl::Instruction> m_instructions;
                ULL m_maxStackSize = 0;
                std::vector<std::weak_ptr<const rtti::Type>> m_args;
                std::weak_ptr<const rtti::Type> m_ret;
                std::string m_identifier;
                virtual usize argc() const override;
                virtual std::shared_ptr<const rtti::Type> argv(usize index) const override;
                virtual std::shared_ptr<const rtti::Type> ret() const override;
                virtual const std::string & identifier() const override;
                virtual usize maxStackSize() const override;
                virtual std::shared_ptr<const rtti::Module> module() const override;
                virtual std::shared_ptr<void> accept(rtti::Visitor *) const override;
            };

        }
    }
}

#endif
