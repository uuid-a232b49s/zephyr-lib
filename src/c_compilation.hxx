#ifndef HEADER_ZEPHYR_SRC_COMPILATION
#define HEADER_ZEPHYR_SRC_COMPILATION 1

#include <memory>
#include <utility>

#include "../include/zephyr/rtti/type.hpp"
#include "../include/zephyr/compilation/method.hpp"

namespace zephyr {
    namespace compilation {

        struct Method::impl {
            struct cMethod;
        };

        struct Method::impl::cMethod : Method {
            std::vector<OpCode> m_opcodes;
            std::vector<rtti::TypePtr> m_locals;
            std::vector<usize> m_labels;
            std::vector<rtti::TypePtr> m_args;
            rtti::TypePtr m_ret = rtti::VoidType::Instance;
            std::string m_identifier;
            std::weak_ptr<cMethod> m_this;
            virtual std::shared_ptr<const rtti::Module> module() const override;
            virtual usize argc() const override;
            virtual std::shared_ptr<const rtti::Type> argv(usize index) const override;
            virtual void argc(usize) override;
            virtual void argv(usize, const std::shared_ptr<const rtti::Type>) override;
            virtual std::shared_ptr<const rtti::Type> ret() const override;
            virtual void emit(OpCode) override;
            virtual LabelRef label() override;
            virtual LocalRef local(std::shared_ptr<const rtti::Type>) override;
            virtual const std::string & identifier() const override;
            virtual usize maxStackSize() const override;
            virtual std::shared_ptr<void> accept(rtti::Visitor *) const override;

            virtual void identifier(const std::string &) override;
            virtual void args(const std::vector<std::shared_ptr<const rtti::Type>> &) override;
            virtual void ret(std::shared_ptr<const rtti::Type>) override;

            void clear();

            static std::shared_ptr<cMethod> new_();
        };

    }
}

#endif
