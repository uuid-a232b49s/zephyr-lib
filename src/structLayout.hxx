#ifndef HEADER_ZEPHYR_SRC_STRUCT_LAYOUT
#define HEADER_ZEPHYR_SRC_STRUCT_LAYOUT 1

#include "throw_.hxx"
#include "common.hxx"

namespace zephyr {
    namespace impl {

        constexpr ULL getAlignPad(ULL currentAlign, ULL requiredAlign) {
            //return (currentAlign % requiredAlign == 0) ? 0 : requiredAlign - currentAlign % requiredAlign;
            return ((requiredAlign == 0) ? 0 : (currentAlign % requiredAlign == 0) ? 0 : requiredAlign - currentAlign % requiredAlign);
        }

        template<typename itrTypeBegin, typename itrTypeEnd, typename itrOffsetBegin>
        void layoutStruct(itrTypeBegin begin, itrTypeEnd end, ULL & align, ULL & size, itrOffsetBegin offsets) {
            align = 0;
            size = 0;

            for (; begin != end; ++begin, ++offsets) {
                auto & t = **begin;

                auto typeSize = t.size();
                auto typeAlign = t.align();
                //if (typeAlign > typeSize) { typeSize = typeAlign; }

                ASSERT_TRUE(typeSize >= typeAlign);
                ASSERT_TRUE(typeSize > 0);
                ASSERT_TRUE(typeAlign > 0);
                ASSERT_TRUE(typeAlign == 1 || typeAlign % 2 == 0);

                if (typeAlign > align) { align = typeAlign; }

                ULL pad = getAlignPad(size, typeAlign);

                *offsets = size + pad;
                size += typeSize + pad;
            }

            size += getAlignPad(size, align);
        }

    }
}

#endif
