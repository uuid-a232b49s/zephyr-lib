#ifndef HEADER_ZEPHYR_SRC_RT_COMPILATION
#define HEADER_ZEPHYR_SRC_RT_COMPILATION 1

#include <cstddef>

#include "../include/zephyr/rtti/type.hpp"
#include "../include/zephyr/rtti/descriptor.hpp"
#include "../include/zephyr/runtime/execution.hpp"
#include "common.hxx"

namespace zephyr {
    namespace runtime {
        namespace impl {
            struct MethodImpl;
        }
    }
}

namespace zephyr {
    namespace impl {

        using FnMove = rtti::AbstractFn<void(void *, void *)>;
        using FnCopy = rtti::AbstractFn<void(const void *, void *)>;
        using FnToBool = rtti::AbstractFn<bool(void *)>;
        using FnLb = rtti::AbstractFn<void(void * address, const SLL * offsets)>;
        using FnDtor = rtti::AbstractFn<void(void *)>;
        
        struct Instruction {
            union {
                ULL m_ull;
                struct {
                    ULL a, b;
                } m_ull_ull;
                struct {
                    FnCopy * copy;
                    ULL typeSize, pad;
                    ULL offsetTo0;
                    ULL argIdx;
                } m_ldArg;
                struct {
                    FnMove * move;
                    ULL typeSize;
                    ULL argIdx;
                    ULL offsetTo0;
                } m_ret;
                struct {
                    FnToBool * toBool;
                    ULL typeSize, pad;
                    SLL jumpDelta;
                } m_branch;
                struct {
                    union {
                        FnMove * move;
                        FnCopy * copy;
                    };
                    ULL typeSize, pad;
                    ULL offsetToLoc;
                } m_ldLoc;
                struct {
                    FnMove * move;
                    ULL typeSize, pad;
                    ULL offsetToLoc;
                } m_stLoc;
                struct {
                    const FnLb * fn;
                    const SLL * argOffsets;
                } m_invokeLbFn;
                struct {
                    const runtime::impl::MethodImpl * target, * this_;
                    const SLL * argOffsets;
                    ULL thisInstOffset;
                    ULL pad;
                } m_invoke;
                struct {
                    FnDtor * dtor;
                    ULL typeSize, pad;
                } m_argClean;
                struct {
                    FnDtor * dtor;
                    ULL offsetToLoc;
                } m_locClean;
                struct {
                    FnMove * move;
                    ULL typeSize, pad;
                    ULL retOffset;
                } m_retMove;
                struct {
                    FnCopy * copy;
                    ULL typeSize;
                } m_dup;
                struct {
                    FnDtor * dtor;
                    ULL typeSize, pad;
                } m_rem;
                struct {
                    FnCopy * copy;
                    ULL typeSize, pad;
                    const void * data;
                } m_ldConst;
                struct {
                    FnCopy * copy;
                    ULL typeSize, pad0, pad1;
                } m_deref;
            } data;
            void(*fn)(const Instruction *, runtime::ExecutionContext *);
            constexpr Instruction() noexcept : data { 0 }, fn(nullptr) {}
        };

        // args, ret?, |, invokeMeta, <method start>, locals, stack
        struct alignas(std::max_align_t) invokeMeta {
            UC * argsStartAddr; // arguments start address
            const SLL * argOffsets; // arguments offsets from the start address
            const Instruction * retInstAddr; // instruction address to which to return control
        };
        static_assert(alignof(invokeMeta) >= alignof(std::max_align_t), "unexpected invokeMeta alignment");

        union alignas(invokeMeta) alignedNumber {
            ULL a;
        };

    }
}

namespace zephyr {
    namespace impl {

        struct reference;

        struct refImpl {
            virtual ~refImpl() = default;
            virtual void * deref(const reference *) const = 0;
            virtual bool valid(const reference *) const = 0;
        };

        struct reference {
            union {
                void * pointer;
            } data;
            const refImpl * impl;
        };

        struct refStaticImpl : refImpl {
            static refStaticImpl instance;
            virtual void * deref(const reference * r) const override { return r->data.pointer; }
            virtual bool valid(const reference * r) const override { return true; }
        };

    }
}

#endif
