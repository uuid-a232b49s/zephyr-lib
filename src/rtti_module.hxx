#ifndef HEADER_ZEPHYR_SRC_RTTI_MODULE
#define HEADER_ZEPHYR_SRC_RTTI_MODULE 1

#include <map>
#include <memory>
#include <vector>
#include <string>
#include <typeindex>

#include "zephyr/rtti/module.hpp"

namespace zephyr {
    namespace rtti {
        namespace impl {

            struct ZEPHYRLIB_LOCAL CompiledModuleImpl : Module {
                std::vector<std::unique_ptr<rtti::Symbol>> m_unnamedSymbols;
                std::map<std::string, std::unique_ptr<rtti::Symbol>> m_namedSymbols;
                std::map<std::type_index, rtti::NativeType *> m_nativeTypes;
                std::vector<std::shared_ptr<const rtti::Module>> m_referencedModules;
                std::vector<std::shared_ptr<const rtti::Symbol>> m_referencedSymbols; // keeps objects alive
                std::weak_ptr<CompiledModuleImpl> this_;
                std::string m_identifier;
                CompiledModuleImpl();
                ~CompiledModuleImpl();
                virtual usize referenceCount() const override;
                virtual std::shared_ptr<const Module> referenceAt(usize index) const override;
                virtual std::shared_ptr<const rtti::Symbol> findSymbol(const std::string &) const override;
                virtual std::shared_ptr<const NativeType> findNativeType(const std::type_info &) const override;
                virtual const std::string & identifier() const override;
            };

        }
    }
}

#endif
