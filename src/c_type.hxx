#ifndef HEADER_ZEPHYR_SRC_C_TYPE
#define HEADER_ZEPHYR_SRC_C_TYPE 1

#include <memory>
#include <string>

#include "../include/zephyr/rtti/type.hpp"
#include "../include/zephyr/compilation/type.hpp"
#include "rtti_type.hxx"

namespace zephyr {
    namespace rtti {
        struct RttiDescriptor;
    }
}

namespace zephyr {
    namespace compilation {
        namespace impl {

            struct NativeTypeImpl : compilation::NativeType {
                std::shared_ptr<const rtti::RttiDescriptor> m_rtti;
                std::string m_identifier;
                rtti::impl::addressTypeMgr m_addrMgr;
                std::weak_ptr<NativeTypeImpl> m_this;
                virtual usize size() const override;
                virtual usize align() const override;
                virtual std::shared_ptr<const rtti::RttiDescriptor> rtti() const override;
                virtual const std::string & identifier() const override;
                virtual void identifier(std::string) override;
                virtual std::shared_ptr<const rtti::AddressType> address(rtti::AddressConstraints) const override;
                virtual std::shared_ptr<void> accept(rtti::Visitor *) const override;
                static std::shared_ptr<NativeTypeImpl> new_(std::shared_ptr<const rtti::RttiDescriptor>);
            };

        }
    }
}

#endif
