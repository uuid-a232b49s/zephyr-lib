#ifndef HEADER_ZEPHYR_SRC_PRIMITIVESERIALIZATION
#define HEADER_ZEPHYR_SRC_PRIMITIVESERIALIZATION 1

#include <cmath>
#include <array>
#include <string>
#include <cstring>
#include <climits>
#include <cstdint>
#include <istream>
#include <ostream>

#include "crc.hxx"
#include "common.hxx"
#include "throw_.hxx"

namespace zephyr {
    namespace impl {

        enum struct endianness_t { big, little, unknown };
        extern const endianness_t endianness;

    }
}

#define SC(type, value) static_cast<type>(value)

namespace zephyr {
    namespace impl {

        inline void writeUInt8(std::ostream & out, std::uint8_t value, crc32_t & crc) {
            out.write(reinterpret_cast<const char *>(&value), 1);
            crc = crc32(&value, &value + 1, crc);
        }

        inline std::uint8_t readUInt8(std::istream & in, crc32_t & crc) {
            std::uint8_t v;
            in.read(reinterpret_cast<char *>(&v), 1);
            crc = crc32(&v, &v + 1, crc);
            return v;
        }

        inline void writeUInt16(std::ostream & out, std::uint16_t value, crc32_t & crc) {
            if (endianness == endianness_t::big) {
                out.write(reinterpret_cast<const char *>(&value), 2);
                const auto v = reinterpret_cast<const UC *>(&value);
                crc = crc32(v, v + 2, crc);
            } else {
                std::uint8_t v[2] {
                    SC(std::uint8_t, value >> CHAR_BIT),
                    SC(std::uint8_t, value)
                };
                out.write(reinterpret_cast<const char *>(&v), 2);
                crc = crc32(v, v + 2, crc);
            }
        }

        inline std::uint16_t readUInt16(std::istream & in, crc32_t & crc) {
            std::uint8_t v[2];
            in.read(reinterpret_cast<char *>(v), 2);
            crc = crc32(v, v + 2, crc);
            if (endianness == endianness_t::big) {
                return (SC(std::uint16_t, v[0])) | (SC(std::uint16_t, v[1]) << CHAR_BIT);
            } else {
                return (SC(std::uint16_t, v[1])) | (SC(std::uint16_t, v[0]) << CHAR_BIT);
            }
        }

        inline void writeUInt32(std::ostream & out, std::uint32_t value, crc32_t & crc) {
            if (endianness == endianness_t::big) {
                out.write(reinterpret_cast<const char *>(&value), 4);
                const auto v = reinterpret_cast<const UC *>(&value);
                crc = crc32(v, v + 4, crc);
            } else {
                std::uint8_t v[4] {
                    SC(std::uint8_t, value >> (CHAR_BIT * 3)),
                    SC(std::uint8_t, value >> (CHAR_BIT * 2)),
                    SC(std::uint8_t, value >> CHAR_BIT),
                    SC(std::uint8_t, value)
                };
                out.write(reinterpret_cast<const char *>(&v), 4);
                crc = crc32(v, v + 4, crc);
            }
        }

        inline std::uint32_t readUInt32(std::istream & in, crc32_t & crc) {
            std::uint8_t v[4];
            in.read(reinterpret_cast<char *>(v), 4);
            crc = crc32(v, v + 4, crc);
            if (endianness == endianness_t::big) {
                return
                    (SC(std::uint32_t, v[0])) |
                    (SC(std::uint32_t, v[1]) << CHAR_BIT) |
                    (SC(std::uint32_t, v[2]) << (CHAR_BIT * 2)) |
                    (SC(std::uint32_t, v[3]) << (CHAR_BIT * 3));
            } else {
                return
                    (SC(std::uint32_t, v[3])) |
                    (SC(std::uint32_t, v[2]) << CHAR_BIT) |
                    (SC(std::uint32_t, v[1]) << (CHAR_BIT * 2)) |
                    (SC(std::uint32_t, v[0]) << (CHAR_BIT * 3));
            }
        }

        inline void writeUInt64(std::ostream & out, std::uint64_t value, crc32_t & crc) {
            if (endianness == endianness_t::big) {
                out.write(reinterpret_cast<const char *>(&value), 8);
                const auto v = reinterpret_cast<const UC *>(&value);
                crc = crc32(v, v + 8, crc);
            } else {
                std::uint8_t v[8] {
                    SC(std::uint8_t, value >> (CHAR_BIT * 7)),
                    SC(std::uint8_t, value >> (CHAR_BIT * 6)),
                    SC(std::uint8_t, value >> (CHAR_BIT * 5)),
                    SC(std::uint8_t, value >> (CHAR_BIT * 4)),
                    SC(std::uint8_t, value >> (CHAR_BIT * 3)),
                    SC(std::uint8_t, value >> (CHAR_BIT * 2)),
                    SC(std::uint8_t, value >> CHAR_BIT),
                    SC(std::uint8_t, value)
                };
                out.write(reinterpret_cast<const char *>(&v), 8);
                crc = crc32(v, v + 8, crc);
            }
        }

        inline std::uint64_t readUInt64(std::istream & in, crc32_t & crc) {
            std::uint8_t v[8];
            in.read(reinterpret_cast<char *>(v), 8);
            crc = crc32(v, v + 8, crc);
            if (endianness == endianness_t::big) {
                return
                    (SC(std::uint64_t, v[0])) |
                    (SC(std::uint64_t, v[1]) << CHAR_BIT) |
                    (SC(std::uint64_t, v[2]) << (CHAR_BIT * 2)) |
                    (SC(std::uint64_t, v[3]) << (CHAR_BIT * 3)) |
                    (SC(std::uint64_t, v[4]) << (CHAR_BIT * 4)) |
                    (SC(std::uint64_t, v[5]) << (CHAR_BIT * 5)) |
                    (SC(std::uint64_t, v[6]) << (CHAR_BIT * 6)) |
                    (SC(std::uint64_t, v[7]) << (CHAR_BIT * 7));
            } else {
                return
                    (SC(std::uint64_t, v[7])) |
                    (SC(std::uint64_t, v[6]) << CHAR_BIT) |
                    (SC(std::uint64_t, v[5]) << (CHAR_BIT * 2)) |
                    (SC(std::uint64_t, v[4]) << (CHAR_BIT * 3)) |
                    (SC(std::uint64_t, v[3]) << (CHAR_BIT * 4)) |
                    (SC(std::uint64_t, v[2]) << (CHAR_BIT * 5)) |
                    (SC(std::uint64_t, v[1]) << (CHAR_BIT * 6)) |
                    (SC(std::uint64_t, v[0]) << (CHAR_BIT * 7));
            }
        }

        inline void writeInt8(std::ostream & out, std::int8_t value, crc32_t & crc) {
            out.write(reinterpret_cast<const char *>(&value), 1);
            const auto v = reinterpret_cast<const UC *>(&value);
            crc = crc32(v, v + 1, crc);
        }
        inline std::int8_t readInt8(std::istream & in, crc32_t & crc) {
            std::int8_t v;
            in.read(reinterpret_cast<char *>(&v), 1);
            {
                const auto v0 = reinterpret_cast<const UC *>(&v);
                crc = crc32(v0, v0 + 1, crc);
            }
            return v;
        }

        inline void writeInt16(std::ostream & out, std::int16_t value, crc32_t & crc) {
            value < 0 ? writeUInt16(out, SC(std::uint16_t, -value) + 0x7fffU, crc) : writeUInt16(out, value, crc);
        }
        inline std::int16_t readInt16(std::istream & in, crc32_t & crc) {
            auto v = readUInt16(in, crc);
            return v > 0x7fffU ? -SC(std::int16_t, v - 0x7fffU) : SC(std::int16_t, v);
        }

        inline void writeInt32(std::ostream & out, std::int32_t value, crc32_t & crc) {
            value < 0 ? writeUInt32(out, SC(std::uint32_t, -value) + 0x7fffffffU, crc) : writeUInt32(out, value, crc);
        }
        inline std::int32_t readInt32(std::istream & in, crc32_t & crc) {
            auto v = readUInt32(in, crc);
            return v > 0x7fffffffU ? -SC(std::int32_t, v - 0x7fffffffU) : SC(std::int32_t, v);
        }

        inline void writeInt64(std::ostream & out, std::int64_t value, crc32_t & crc) {
            value < 0 ? writeUInt64(out, SC(std::uint64_t, -value) + 0x7fffffffffffffffU, crc) : writeUInt64(out, value, crc);
        }
        inline std::int64_t readInt64(std::istream & in, crc32_t & crc) {
            auto v = readUInt64(in, crc);
            return v > 0x7fffffffffffffffU ? -SC(std::int64_t, v - 0x7fffffffffffffffU) : SC(std::int64_t, v);
        }

        inline void writeFlt32(std::ostream & out, float value, crc32_t & crc) {
            int exp;
            float n;
            {
                using namespace std;
                n = frexpf(value, &exp);
            }
            writeInt16(out, exp, crc);
            writeInt32(out, SC(std::int32_t, n * (2 << 29)), crc);
        }

        inline float readFlt32(std::istream & in, crc32_t & crc) {
            auto exp = readInt16(in, crc);
            auto n = readInt32(in, crc);
            using namespace std;
            return ldexpf(SC(float, n) / (2 << 29), exp);
        }

        inline void writeFlt64(std::ostream & out, double value, crc32_t & crc) {
            int exp;
            double n;
            {
                using namespace std;
                n = frexp(value, &exp);
            }
            writeInt16(out, exp, crc);
            writeInt64(out, SC(std::int64_t, n * (2LL << 61LL)), crc);
        }

        inline double readFlt64(std::istream & in, crc32_t & crc) {
            auto exp = readInt16(in, crc);
            auto n = readInt64(in, crc);
            using namespace std;
            return ldexp(SC(double, n) / (2LL << 61LL), exp);
        }

        inline void writeString(std::ostream & out, const char * value, crc32_t & crc) {
            auto l = std::strlen(value);
            writeUInt64(out, l, crc);
            out.write(value, l);
        }

        inline void writeString(std::ostream & out, const std::string & value, crc32_t & crc) {
            return writeString(out, value.c_str(), crc);
        }

        inline void readString(std::istream & in, std::string & value, crc32_t & crc) {
            auto l = readUInt64(in, crc);
            value.resize(l);
            in.read(&value[0], l);
        }

        inline std::string readString(std::istream & in, crc32_t & crc) {
            std::string v;
            readString(in, v, crc);
            return v;
        }

        inline void writeCrc(std::ostream & out, crc32_t & crc) {
            writeUInt32(out, crc, crc);
        }

        inline bool checkCrc(std::istream & in, crc32_t & crc) {
            auto c = crc;
            return c == readUInt32(in, crc);
        }

    }
}

#undef SC

#endif
