#include <typeinfo>

#include "zephyr/rtti/type.hpp"
#include "zephyr/rtti/visitor.hpp"
#include "zephyr/rtti/descriptor.hpp"

#include "rtti_type.hxx"
#include "throw_.hxx"

namespace zephyr {
    namespace rtti {

        const std::shared_ptr<const VoidType> VoidType::Instance = [] () -> const std::shared_ptr<const VoidType> {
            struct VoidTypeImpl : VoidType {
                impl::addressTypeMgr m_addrMgr;
                std::weak_ptr<const VoidType> m_this;
                virtual usize size() const override { return 0; }
                virtual usize align() const override { return 0; }
                virtual std::shared_ptr<const Module> module() const override { return nullptr; }
                virtual const std::string & identifier() const override { static const std::string identifier; return identifier; }
                virtual std::shared_ptr<const rtti::AddressType> address(rtti::AddressConstraints c) const override { return m_addrMgr.getAddressType(c, [&] { return m_this.lock(); }); }
                virtual std::shared_ptr<void> accept(Visitor * v) const override { return v->visit(m_this.lock()); }
            } static instance;
            static const std::shared_ptr<const VoidType> ptr{ &instance, [] (const void *) {} };
            instance.m_this = ptr;
            return ptr;
        } ();

        namespace impl {

            namespace {
                static const rtti::RttiDescriptorGeneric<void *> addrRtti {};
                static const std::shared_ptr<decltype(addrRtti)> addrRttiPtr { &addrRtti, [] (const void *) noexcept {} };
            }

            usize AddressTypeImpl::size() const { return sizeof(void *); }
            usize AddressTypeImpl::align() const { return alignof(void *); }
            std::shared_ptr<const Module> AddressTypeImpl::module() const { return m_base->module(); }
            AddressConstraints AddressTypeImpl::constraints() const { return m_constraints; }
            std::shared_ptr<const Type> AddressTypeImpl::base() const { return m_base; }
            std::shared_ptr<void> AddressTypeImpl::accept(Visitor * v) const { return v->visit(std::static_pointer_cast<const rtti::AddressType>(m_this.lock())); }
            std::shared_ptr<const RttiDescriptor> AddressTypeImpl::rtti() const { return addrRttiPtr; }
            std::shared_ptr<const rtti::AddressType> AddressTypeImpl::address(rtti::AddressConstraints c) const { return m_addrMgr.getAddressType(c, [&] { return m_this.lock(); }); }

        }

    }
}
