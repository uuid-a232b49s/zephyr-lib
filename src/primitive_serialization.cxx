#include <cstdint>
#include <exception>

#include "primitive_serialization.hxx"

namespace zephyr {
    namespace impl {

        const endianness_t endianness = [] () noexcept -> endianness_t {
            std::uint8_t v[8];
            *reinterpret_cast<std::uint64_t *>(v) = 0x1234567898765432ULL;
            if (v[0] == 0x12 && v[1] == 0x34 && v[2] == 0x56 && v[3] == 0x78 && v[4] == 0x98 && v[5] == 0x76 && v[6] == 0x54 && v[7] == 0x32) {
                return endianness_t::big;
            } else if (v[7] == 0x12 && v[6] == 0x34 && v[5] == 0x56 && v[4] == 0x78 && v[3] == 0x98 && v[2] == 0x76 && v[1] == 0x54 && v[0] == 0x32) {
                return endianness_t::little;
            } else {
                x_print("unsupported endianness");
                std::terminate(); // unsupported
                return endianness_t::unknown;
            }
        } ();

    }
}
