#include <typeindex>

#include "zephyr/rtti/symbol.hpp"
#include "rtti_module.hxx"

namespace zephyr {
    namespace rtti {
        namespace impl {

            CompiledModuleImpl::CompiledModuleImpl() = default;
            CompiledModuleImpl::~CompiledModuleImpl() = default;

            usize CompiledModuleImpl::referenceCount() const { return m_referencedModules.size(); }
            std::shared_ptr<const Module> CompiledModuleImpl::referenceAt(usize index) const { return m_referencedModules.at(index); }

            std::shared_ptr<const rtti::Symbol> CompiledModuleImpl::findSymbol(const std::string & identifier) const {
                auto i = m_namedSymbols.find(identifier);
                return (i == m_namedSymbols.end()) ? nullptr : std::shared_ptr<const Symbol>(this_.lock(), i->second.get());
            }

            std::shared_ptr<const NativeType> CompiledModuleImpl::findNativeType(const std::type_info & ti) const {
                auto i = m_nativeTypes.find(std::type_index(ti));
                return (i == m_nativeTypes.end()) ? nullptr : std::shared_ptr<const NativeType>(this_.lock(), i->second);
            }

            const std::string & CompiledModuleImpl::identifier() const { return m_identifier; }

        }
    }
}
