#include <map>
#include <tuple>
#include <cstring>
#include <utility>
#include <iterator>

#include "zephyr/compilation/compilation-unit.hpp"
#include "zephyr/compilation/type.hpp"
#include "zephyr/rtti/module.hpp"
#include "zephyr/rtti/value.hpp"
#include "primitive_serialization.hxx"
#include "c_compilation.hxx"
#include "c_type.hxx"
#include "c_opcode.hxx"
#include "throw_.hxx"
#include "common.hxx"

namespace zephyr {
    namespace compilation {

        CompilationUnit::CompilationUnit() = default;

        CompilationUnit::~CompilationUnit() {
            for (auto & x_ : m_methods) {
                static_cast<Method::impl::cMethod *>(x_.get())->clear();
            }
        }

        CompilationUnit::CompilationUnit(CompilationUnit && m) noexcept {
            identifier = std::move(m.identifier);
            m_nativeTypes = std::move(m.m_nativeTypes);
            m_methods = std::move(m.m_methods);
            referencedModules = std::move(m.referencedModules);
        }

        std::shared_ptr<NativeType> CompilationUnit::declareNativeType0(std::shared_ptr<const rtti::RttiDescriptor> rtti) {
            auto t = compilation::impl::NativeTypeImpl::new_(std::move(rtti));
            m_nativeTypes.emplace_back(std::move(t));
            return m_nativeTypes.back();
        }

        std::shared_ptr<Method> CompilationUnit::declareMethod() {
            auto m = Method::impl::cMethod::new_();
            m_methods.emplace_back(std::move(m));
            return m_methods.back();
        }

    }
}

namespace zephyr {
    namespace compilation {

#define assert_(condition, msg) ASSERT_((condition), zephyr::SerializationException, msg)
#define assert_0(condition, ...) ASSERT_0((condition), zephyr::SerializationException, __VA_ARGS__)

        void CompilationUnit::serialize(std::ostream & out, const DataProvider & dataProvider, bool binary) const {
            if (binary) {
                zephyr::impl::crc32_t crc = 0;
                using namespace zephyr::impl;
                writeString(out, identifier, crc);

                auto writeTypeReference = [&] (const rtti::Type * t) {
                    auto t0 = dynamic_cast<const compilation::impl::NativeTypeImpl *>(t);
                    if (t0) {
                        writeUInt8(out, 1, crc);
                        usize i = 0;
                        for (auto l = m_nativeTypes.size(); i < l; ++i) {
                            if (m_nativeTypes[i].get() == t0) { goto lbl_break; }
                        }
                        assert_(false, "illegal declared type");
                    lbl_break:;
                        writeUInt64(out, i, crc);
                    } else {
                        writeUInt8(out, 0, crc);
                        {
                            auto & i = t->module()->identifier();
                            assert_(!i.empty(), "external module identifier is empty");
                            writeString(out, i, crc);
                        }
                        {
                            auto & i = t->identifier();
                            assert_(!i.empty(), "external type identifier is empty");
                            writeString(out, i, crc);
                        }
                    }
                    {
                        usize i = 0;
                        for (auto c = dynamic_cast<const rtti::AddressType *>(t); c; ++i, c = dynamic_cast<const rtti::AddressType *>(c->base().get()));
                        assert_(i <= 0xffffULL, "pointer indirection too large");
                        writeUInt16(out, (std::uint16_t)i, crc);
                    }
                    for (auto c = dynamic_cast<const rtti::AddressType *>(t); c; c = dynamic_cast<const rtti::AddressType *>(c->base().get())) {
                        writeUInt8(out, c->constraints().value(), crc);
                    }
                };

                auto writeMethodReference = [&] (const rtti::Method * m) {
                    auto m0 = dynamic_cast<const compilation::Method::impl::cMethod *>(m);
                    if (m0) {
                        writeUInt8(out, 1, crc);
                        usize i = 0;
                        for (auto l = m_methods.size(); i < l; ++i) {
                            if (m_methods[i].get() == m0) { goto lbl_break; }
                        }
                        assert_(false, "illegal declared method");
                    lbl_break:;
                        writeUInt64(out, i, crc);
                    } else {
                        writeUInt8(out, 0, crc);
                        {
                            auto & i = m->module()->identifier();
                            assert_(!i.empty(), "external module identifier is empty");
                            writeString(out, i, crc);
                        }
                        {
                            auto & i = m->identifier();
                            assert_(!i.empty(), "external method identifier is empty");
                            writeString(out, i, crc);
                        }
                    }
                };

                // write constants
                std::map<const rtti::Value *, usize> constants;
                {
                    {
                        usize i = 0;
                        for (auto & a : m_methods) {
                            for (auto & b : static_cast<Method::impl::cMethod *>(a.get())->m_opcodes) {
                                auto & c = b.m_data.value;
                                if (c && !constants.count(c.get())) {
                                    constants[c.get()] = i++;
                                }
                            }
                        }
                    }
                    {
                        writeUInt64(out, constants.size(), crc);
                        for (usize i = 0, l = constants.size(); i < l; ++i) {
                            for (auto & a : constants) {
                                if (a.second == i) {
                                    writeString(out, dataProvider.identifier(a.first->typeinfo()), crc);
                                    usize crc0 = static_cast<usize>(crc);
                                    a.first->serialize(out, true, crc0);
                                    crc = static_cast<decltype(crc)>(crc0);
                                    goto lbl_continue;
                                }
                            }
                            assert_(false, "failed to enumerate constants");
                        lbl_continue:;
                        }
                    }
                    writeCrc(out, crc);
                }

                // write native types
                {
                    writeUInt64(out, m_nativeTypes.size(), crc);
                    for (auto & x : m_nativeTypes) {
                        writeString(out, dataProvider.identifier(x->rtti()->typeinfo()), crc);
                        writeString(out, x->identifier(), crc);
                    }
                    writeCrc(out, crc);
                }

                // write methods
                {
                    writeUInt64(out, m_methods.size(), crc);
                    for (auto & a : m_methods) {
                        writeString(out, a->identifier(), crc);
                        writeUInt64(out, a->argc(), crc);
                        for (usize i = 0, l = a->argc(); i < l; ++i) { writeTypeReference(a->argv(i).get()); }
                        writeTypeReference(a->ret().get());
                    }
                    std::function<usize(const rtti::Value *)> getValueIdx = [&] (const rtti::Value * v) { return constants.at(v); };
                    std::function<void(const rtti::Type *)> writeTypeRef = [&] (const rtti::Type * t) { writeTypeReference(t); };
                    std::function<void(const rtti::Method *)> writeMethodRef = [&] (const rtti::Method * m) { writeMethodReference(m); };
                    for (auto & a : m_methods) {
                        auto b = static_cast<Method::impl::cMethod *>(a.get());
                        { // locals
                            writeUInt64(out, b->m_locals.size(), crc);
                            for (auto & l : b->m_locals) { writeTypeReference(l.get()); }
                        }
                        { // labels
                            writeUInt64(out, b->m_labels.size(), crc);
                            for (auto l : b->m_labels) { writeUInt64(out, l, crc); }
                        }
                        { // opcodes
                            writeUInt64(out, b->m_opcodes.size(), crc);
                            for (auto & c : b->m_opcodes) {
                                OpCode::impl_::serialize(c, out, dataProvider, getValueIdx, writeTypeRef, writeMethodRef, true, crc);
                            }
                        }
                    }
                    writeCrc(out, crc);
                }
            } else {
                assert_(false, "not implemented");
            }
        }

        void CompilationUnit::deserialize(std::istream & in, const DataProvider & dataProvider, const ModuleProvider & moduleProvider, bool binary) {
            std::string identifier;
            std::vector<std::shared_ptr<compilation::impl::NativeTypeImpl>> m_nativeTypes;
            std::vector<std::shared_ptr<Method::impl::cMethod>> m_methods;

            if (binary) {
                zephyr::impl::crc32_t crc = 0;

                using namespace zephyr::impl;

                readString(in, identifier, crc);

                auto readTypeReference = [&] () -> std::shared_ptr<const rtti::Type> {
                    std::shared_ptr<const rtti::Type> t;
                    if (readUInt8(in, crc)) {
                        t = m_nativeTypes.at(readUInt64(in, crc));
                    } else {
                        auto m = moduleProvider.find(readString(in, crc));
                        assert_(m, "module not found");
                        t = m->find<rtti::Type>(readString(in, crc));
                        assert_(m, "external type not found");
                    }
                    for (usize i = 0, l = readUInt16(in, crc); i < l; ++i) {
                        t = t->address(rtti::AddressConstraints(readUInt8(in, crc)));
                    }
                    return t;
                };

                auto readMethodReference = [&] () -> std::shared_ptr<const rtti::Method> {
                    if (readUInt8(in, crc)) {
                        return m_methods.at(readUInt64(in, crc));
                    } else {
                        auto m = moduleProvider.find(readString(in, crc));
                        assert_(m, "module not found");
                        auto mm = m->find<rtti::Method>(readString(in, crc));
                        assert_(mm, "external method not found");
                        return mm;
                    }
                };

                // read constants
                std::vector<std::shared_ptr<const rtti::Value>> constants;
                {
                    for (usize i = 0, l = readUInt64(in, crc); i < l; ++i) {
                        auto v = dataProvider.value(dataProvider.typeinfo(readString(in, crc)));
                        auto crc0 = static_cast<usize>(crc);
                        v->deserialize(in, true, crc0);
                        crc = static_cast<decltype(crc)>(crc0);
                        constants.emplace_back(std::move(v));
                    }
                    assert_(checkCrc(in, crc), "crc mismatch");
                }

                // read native types
                {
                    auto l = readUInt64(in, crc);
                    m_nativeTypes.resize(l);
                    for (usize i = 0; i < l; ++i) {
                        auto & t = m_nativeTypes[i];
                        auto rtti = dataProvider.rtti(dataProvider.typeinfo(readString(in, crc)));
                        assert_(rtti != nullptr, "failed to find target rtti");
                        t = compilation::impl::NativeTypeImpl::new_(std::move(rtti));
                        t->identifier(readString(in, crc));
                    }
                    assert_(checkCrc(in, crc), "crc mismatch");
                }

                // read methods
                {
                    auto l = readUInt64(in, crc);
                    m_methods.resize(l);
                    for (usize i = 0; i < l; ++i) {
                        auto & m = m_methods[i];
                        m = Method::impl::cMethod::new_();
                        m->identifier(readString(in, crc));
                        {
                            usize l = readUInt64(in, crc);
                            m->argc(l);
                            for (usize i = 0; i < l; ++i) {
                                m->argv(i, readTypeReference());
                            }
                            m->ret(readTypeReference());
                        }
                    }
                    std::function<std::shared_ptr<const rtti::Value>(usize)> getValue = [&] (usize idx) { return constants.at(idx); };
                    std::function<std::shared_ptr<const rtti::Type>(void)> readTypeRef = [&] { return readTypeReference(); };
                    std::function<std::shared_ptr<const rtti::Method>(void)> readMethodRef = [&] { return readMethodReference(); };
                    for (usize i = 0; i < l; ++i) {
                        auto & m = m_methods[i];
                        { // locals
                            m->m_locals.resize(readUInt64(in, crc));
                            for (auto & l : m->m_locals) { l = readTypeReference(); }
                        }
                        { // labels
                            m->m_labels.resize(readUInt64(in, crc));
                            for (auto & l : m->m_labels) { l = readUInt64(in, crc); }
                        }
                        { // opcodes
                            m->m_opcodes.resize(readUInt64(in, crc));
                            for (auto & c : m->m_opcodes) {
                                c = OpCode::impl_::deserialize(in, m.get(), dataProvider, getValue, readTypeRef, readMethodRef, true, crc);
                            }
                        }
                    }
                    assert_(checkCrc(in, crc), "crc mismatch");
                }
            } else {
                assert_(false, "not implemented");
            }

            this->identifier = std::move(identifier);
            this->m_nativeTypes.insert(this->m_nativeTypes.end(), std::make_move_iterator(m_nativeTypes.begin()), std::make_move_iterator(m_nativeTypes.end()));
            this->m_methods.insert(this->m_methods.end(), std::make_move_iterator(m_methods.begin()), std::make_move_iterator(m_methods.end()));
        }

    }
}
