#include <typeinfo>

#include "zephyr/rtti/descriptor.hpp"
#include "throw_.hxx"

namespace zephyr {
    namespace rtti {
        static void noOpDtor(const void *) noexcept {}
#define impl_c_ impl_1_(bool, b); impl_2_(char, c); impl_2_(short, s); impl_2_(int, i); impl_2_(long, l); impl_2_(long long, ll); impl_1_(float, f); impl_1_(double, d); impl_1_(long double, ld);
#define impl_1_(type, t) \
        static const RttiDescriptorGeneric<type> instance_##t; \
        static const std::shared_ptr<const RttiDescriptorGeneric<type>> instance_ptr_##t { &instance_##t, noOpDtor }; \
        template<> ZEPHYRLIB_API const std::shared_ptr<const RttiDescriptorGeneric<type>> & RttiDescriptorGeneric<type>::Instance() noexcept { return instance_ptr_##t; }
#define impl_2_(type, t) impl_1_(signed type, s##t) impl_1_(unsigned type, u##t)
        impl_c_
#undef impl_2_
#undef impl_1_
    }
}

namespace zephyr {
    namespace rtti {

        const std::type_info & RttiDescriptorGeneric<void>::typeinfo() const { return typeid(void); }
        const std::type_info & RttiDescriptorGeneric<void>::ptrTypeinfo(bool immutable) const { return immutable ? typeid(const void *) : typeid(void *); }

        const std::shared_ptr<const RttiDescriptorGeneric<void>> & RttiDescriptorGeneric<void>::Instance() noexcept {
            static const RttiDescriptorGeneric<void> instance;
            static const std::shared_ptr<decltype(instance)> ptr { &instance, [] (const void *) noexcept {} };
            return ptr;
        }

    }
}
