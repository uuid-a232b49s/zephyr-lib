#include "../include/zephyr/rtti/absfn.hpp"
#include "../include/zephyr/rtti/value.hpp"
#include "../include/zephyr/rtti/visitor.hpp"

#include "r_method.hxx"

namespace zephyr {
    namespace runtime {
        namespace impl {

            usize MethodImpl::argc() const { return m_args.size(); }
            std::shared_ptr<const rtti::Type> MethodImpl::argv(usize index) const { return m_args.at(index).lock(); }
            std::shared_ptr<const rtti::Type> MethodImpl::ret() const { return m_ret.lock(); }
            const std::string & MethodImpl::identifier() const { return m_identifier; }
            usize MethodImpl::maxStackSize() const { return m_maxStackSize; }
            std::shared_ptr<const rtti::Module> MethodImpl::module() const { return m_module.lock(); }
            std::shared_ptr<void> MethodImpl::accept(rtti::Visitor * v) const { return v->visit(std::shared_ptr<const rtti::Method>(m_module.lock(), this)); }

        }
    }
}
