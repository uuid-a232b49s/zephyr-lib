#ifndef HEADER_ZEPHYR_SRC_THROW_
#define HEADER_ZEPHYR_SRC_THROW_ 1

#include <string>
#include <cstring>
#include <type_traits>

#include "../include/zephyr/exceptions.hpp"

#define THROW_STRINGIZING(x) #x
#define THROW_STR(x) THROW_STRINGIZING(x)
#define THROW_FILE_LINE __FILE__ ":" THROW_STR(__LINE__)
#define EXC(ex, msg) ex(msg " (" THROW_FILE_LINE ")")

#define ASSERT_(condition, exc, msg) ((void)( (condition) ? 0 : throw EXC(exc, msg) ))
#define ASSERT_NOT_NULL(value) ASSERT_(value, zephyr::NullPointerException, "(" THROW_STR(value) ") is null")
#define ASSERT_TRUE(condition) ASSERT_(condition, zephyr::AssertionException, "(" THROW_STR(condition) ") not true")

namespace {
    [[maybe_unused]] static unsigned long long THROW_string_length_0(const char * x) { return std::strlen(x); }
    [[maybe_unused]] static unsigned long long THROW_string_length_0(const std::string & s) { return s.length(); }
    template<typename x>
    [[maybe_unused]] static auto THROW_string_length_0(x i) -> typename std::enable_if<std::is_arithmetic<x>::value, unsigned long long>::type { return 10; }
    [[maybe_unused]] static int THROW_string_append(std::string & s, const char * x) { s += x; return 0; }
    [[maybe_unused]] static int THROW_string_append(std::string & s, const std::string & x) { s += x; return 0; }
    template<typename x>
    [[maybe_unused]] static auto THROW_string_append(std::string & s, x i) -> typename std::enable_if<std::is_arithmetic<x>::value, int>::type { s += std::to_string(i); return 0; }

    template<typename ... strs_t>
    [[maybe_unused]] static std::string THROW_concat_strings_0(const strs_t & ... strs) {
        unsigned long long l = 0;
        { [[maybe_unused]] int x[] = { 0, ((void) (l += THROW_string_length_0(strs)), 0) ... }; }
        std::string v;
        v.reserve(l);
        { [[maybe_unused]] int x[] = { 0, ((void) (THROW_string_append(v, strs)), 0) ... }; }
        return v;
    }
}
#define ASSERT_0(condition, exc, ...) ((void)( (condition) ? 0 : throw exc(THROW_concat_strings_0(__VA_ARGS__, " (" THROW_FILE_LINE ")")) ))

#endif
