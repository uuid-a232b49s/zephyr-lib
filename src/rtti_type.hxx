#ifndef HEADER_ZEPHYR_SRC_RTTI_TYPE
#define HEADER_ZEPHYR_SRC_RTTI_TYPE 1

#include <memory>
#include <typeinfo>

#include "../include/zephyr/rtti/type.hpp"
#include "throw_.hxx"

namespace zephyr {
    namespace rtti {
        namespace impl {

            struct addressTypeMgr {
                mutable std::weak_ptr<const rtti::AddressType> m_addresses[4];
                template<typename getThis_t>
                std::shared_ptr<const rtti::AddressType> getAddressType(rtti::AddressConstraints ac, getThis_t getThis) const;
            };

            struct AddressTypeImpl : AddressType {
                AddressConstraints m_constraints;
                std::shared_ptr<const Type> m_base;
                std::weak_ptr<const AddressTypeImpl> m_this;
                addressTypeMgr m_addrMgr;
                virtual usize size() const override;
                virtual usize align() const override;
                virtual std::shared_ptr<const Module> module() const override;
                virtual const std::string & identifier() const override { static const std::string identifier; return identifier; }
                virtual std::shared_ptr<const rtti::AddressType> address(rtti::AddressConstraints c) const override;
                virtual AddressConstraints constraints() const override;
                virtual std::shared_ptr<const Type> base() const override;
                virtual std::shared_ptr<void> accept(Visitor *) const override;
                virtual std::shared_ptr<const RttiDescriptor> rtti() const override;
            };

            template<typename getThis_t>
            inline std::shared_ptr<const rtti::AddressType> zephyr::rtti::impl::addressTypeMgr::getAddressType(rtti::AddressConstraints ac, getThis_t getThis) const {
                ASSERT_TRUE(ac.value() < 4);
                auto & a = m_addresses[ac.value()];
                auto b = a.lock();
                if (!b) {
                    auto c = std::make_shared<rtti::impl::AddressTypeImpl>();
                    c->m_this = c;
                    c->m_constraints = ac;
                    c->m_base = getThis();
                    a = b = std::move(c);
                }
                return b;
            }

        }
    }
}

#endif
