#include <utility>

#include "zephyr/rtti/visitor.hpp"
#include "zephyr/rtti/descriptor.hpp"
#include "zephyr/compilation/type.hpp"

#include "c_type.hxx"
#include "common.hxx"
#include "throw_.hxx"

namespace zephyr {
    namespace compilation {
        namespace impl {

            const std::string & NativeTypeImpl::identifier() const { return m_identifier; }
            void NativeTypeImpl::identifier(std::string identifier) { m_identifier = std::move(identifier); }

            std::shared_ptr<const rtti::AddressType> NativeTypeImpl::address(rtti::AddressConstraints ac) const {
                return m_addrMgr.getAddressType(ac, [&] { return m_this.lock(); });
            }

            std::shared_ptr<void> NativeTypeImpl::accept(rtti::Visitor * v) const { return v->visit(m_this.lock()); }

            ULL NativeTypeImpl::size() const { return m_rtti->size(); }
            ULL NativeTypeImpl::align() const { return m_rtti->align(); }
            std::shared_ptr<const rtti::RttiDescriptor> NativeTypeImpl::rtti() const { return m_rtti; }

            std::shared_ptr<NativeTypeImpl> NativeTypeImpl::new_(std::shared_ptr<const rtti::RttiDescriptor> rtti) {
                ASSERT_NOT_NULL(rtti);
                auto t = std::shared_ptr<compilation::impl::NativeTypeImpl>(new compilation::impl::NativeTypeImpl());
                t->m_rtti = std::move(rtti);
                t->m_this = t;
                return t;
            }

        }
    }
}
