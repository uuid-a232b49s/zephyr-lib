#include "../include/zephyr/rtti/visitor.hpp"
#include "../include/zephyr/rtti/descriptor.hpp"
#include "r_type.hxx"

namespace zephyr {
    namespace runtime {
        namespace impl {

            std::shared_ptr<const rtti::AddressType> TypeImpl::address(rtti::AddressConstraints ac) const {
                return m_addrMgr.getAddressType(ac, [&] { return rtti::TypePtr(m_module.lock(), this); });
            }

            std::shared_ptr<void> NativeTypeImpl::accept(rtti::Visitor * v) const { return v->visit(std::shared_ptr<const rtti::NativeType>(m_module.lock(), this)); }

            usize NativeTypeImpl::size() const { return m_rtti->size(); }
            usize NativeTypeImpl::align() const { return m_rtti->align(); }

            bool compareTypes(const rtti::Type * t, const std::type_info * i) {
                if (!i) { return false; }
                if (*i == typeid(void)) {
                    return t == rtti::VoidType::Instance.get();
                } else {
                    auto t0 = dynamic_cast<const impl::NativeTypeImpl *>(t);
                    if (t0) {
                        return t0->m_rtti->typeinfo() == *i;
                    } else {
                        auto t1 = dynamic_cast<const rtti::AddressType *>(t);
                        if (t1) {
                            auto b = std::dynamic_pointer_cast<const impl::NativeTypeImpl>(t1->base());
                            if (b) {
                                if (b->m_rtti->ptrTypeinfo(t1->constraints().immutable) == *i) {
                                    return true;
                                }
                            }
                        }
                    }
                }
                return false;
            }

        }
    }
}
