#include <typeinfo>

#include "zephyr/runtime/execution.hpp"
#include "zephyr/rtti/method.hpp"
#include "zephyr/rtti/value.hpp"
#include "r_method.hxx"
#include "r_type.hxx"
#include "rt_compilation.hxx"
#include "throw_.hxx"
#include "common.hxx"
#include "structLayout.hxx"

namespace n_impl = zephyr::impl;

namespace zephyr {
    namespace runtime {

        ExecutionContext::ExecutionContext() {}
        ExecutionContext::~ExecutionContext() {}

        void ExecutionContext::verifyMethodSignature(const std::shared_ptr<const rtti::Method> & method0, const std::type_info * const * argTypes, usize argCount) {
            ASSERT_NOT_NULL(method0);
            auto method = dynamic_cast<const impl::MethodImpl *>(method0.get());
            ASSERT_NOT_NULL(method);

#define assert_(c, m) ASSERT_(c, zephyr::IllegalArgumentException, m)
            assert_(method->m_args.size() == argCount, "mismatching argument count");
            assert_(impl::compareTypes(method->m_ret.lock().get(), argTypes[argCount]), "mismatching return type");
            for (ULL i = 0; i < argCount; ++i) {
                assert_(impl::compareTypes(method->m_args[i].lock().get(), argTypes[i]), "mismatching arg type");
            }
#undef assert_
        }

        void ExecutionContext::execute0(std::shared_ptr<const rtti::Method> method0, void * args, SLL * argOffsets) {
            {
                auto method = static_cast<const impl::MethodImpl *>(method0.get());

                m_currentPage = -1;
                allocateStackPage(method->m_maxStackSize);

                {
                    /*static n_impl::Instruction exitInst = [] () {
                        n_impl::Instruction inst;
                        inst.fn = [] (const n_impl::Instruction *, runtime::ExecutionContext * ctx) {
                            ctx->m_instruction = nullptr;
                        };
                        return inst;
                    }();*/

                    auto im = reinterpret_cast<zephyr::impl::invokeMeta *>(m_stack);
                    im->argOffsets = argOffsets;
                    im->argsStartAddr = reinterpret_cast<UC *>(args);
                    //im->retInstAddr = &exitInst;
                    im->retInstAddr = nullptr;
                    m_stack += sizeof(zephyr::impl::invokeMeta);
                }

                m_instruction = &method->m_instructions[0];
            }
            while (m_instruction) {
                auto i = static_cast<const n_impl::Instruction *>(m_instruction);
                i->fn(i, this);
            }
        }

        void ExecutionContext::executeDynamic(
            std::shared_ptr<const rtti::Method> method0,
            const std::shared_ptr<rtti::Value> & ret,
            const std::vector<std::shared_ptr<rtti::Value>> & args,
            isize argCount
        ) {
#define assert_(c, m) ASSERT_(c, zephyr::IllegalArgumentException, m)
            if (argCount == -1) {
                argCount = args.size();
            } else assert_(argCount <= (isize)args.size(), "argCount can not be bigger than args vector");
            ASSERT_NOT_NULL(method0);
            auto method = dynamic_cast<const impl::MethodImpl *>(method0.get());
            ASSERT_NOT_NULL(method);
            bool voidRet = method->m_ret.lock().get() == rtti::VoidType::Instance.get();
            {
                if (!voidRet) {
                    assert_(ret, "nullptr ret");
                    assert_(impl::compareTypes(method->m_ret.lock().get(), &ret->typeinfo()), "mismatching ret type");
                }
                assert_(method->m_args.size() == (ULL)argCount, "mismatching argument count");
                for (SLL i = 0; i < argCount; ++i) {
                    assert_(args[i], "nullptr arg");
                    assert_(impl::compareTypes(method->m_args[i].lock().get(), &args[i]->typeinfo()), "mismatching arg type");
                }
#undef assert_
            }
            SLL argOffsets10[5];
            std::vector<SLL> argOffsetsV;
            SLL * argOffsets;
            if (argCount > (4 + (voidRet ? 1 : 0))) {
                argOffsetsV.resize(argCount + (voidRet ? 0 : 1));
                argOffsets = &argOffsetsV[0];
            } else argOffsets = argOffsets10;
            for (SLL i = 0; i < argCount; ++i) {
                argOffsets[i] = reinterpret_cast<UC *>(args[i]->data()) - reinterpret_cast<UC *>(0);
            }
            if (!voidRet) {
                argOffsets[argCount] = reinterpret_cast<UC *>(ret->data()) - reinterpret_cast<UC *>(0);
            }
            execute0(std::move(method0), nullptr, argOffsets);
        }

        void ExecutionContext::allocateStackPage(usize minSize) {
            if (minSize < pageSize) { minSize = pageSize; }
            minSize += zephyr::impl::getAlignPad(minSize, alignof(zephyr::impl::invokeMeta));
            {
                usize totalMemory = minSize;
                for (SLL i = 0; i < m_currentPage; ++i) { totalMemory += m_memory[i]->size(); }
                ASSERT_(totalMemory <= maxStackMemory, zephyr::AssertionException, "allocated stack exceeds limit");
            }

            if ((ULL)(m_currentPage + 1) >= m_memory.size()) {
                m_memory.emplace_back(new typename decltype(m_memory)::value_type::element_type());
            }
            auto x = m_memory[m_currentPage + 1].get();
            if (x->size() < minSize) { x->resize(minSize); }
            m_currentPage++;
            m_stack = &(*x)[0];
        }

        void ExecutionContext::deallocateStackPage(usize offset) {
            ASSERT_(m_currentPage >= -1, zephyr::AssertionException, "illegal deallocation");
            if (m_currentPage > 0) {
                auto x = m_memory[m_currentPage - 1].get();
                m_currentPage--;
                m_stack = &(*x)[0] + offset;
            } else {
                m_currentPage = -1;
                m_stack = nullptr;
            }
        }

    }
}
