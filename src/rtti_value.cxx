#include <typeinfo>

#include "zephyr/rtti/value.hpp"
#include "primitive_serialization.hxx"
#include "throw_.hxx"

namespace zephyr {
    namespace rtti {

        usize Value::voidCrc;

        using namespace zephyr::impl;

#define impl_1_(type, r, w) \
        template<> ZEPHYRLIB_API void ValueImpl<type>::serialize(std::ostream & out, bool binary, usize & crc) const { \
            if (binary) { \
                auto crc0 = static_cast<zephyr::impl::crc32_t>(crc); \
                w(out, value, crc0); \
                crc = static_cast<usize>(crc0); \
            } else { \
                out << value; \
            } \
        } \
        template<> ZEPHYRLIB_API void ValueImpl<type>::deserialize(std::istream & in, bool binary, usize & crc) { \
            if (binary) { \
                auto crc0 = static_cast<zephyr::impl::crc32_t>(crc); \
                value = r(in, crc0); \
                crc = static_cast<usize>(crc0); \
            } else { \
                in >> value; \
            } \
        }

        impl_1_(bool, readUInt8, writeUInt8);
        impl_1_(signed char, readInt8, writeInt8);
        impl_1_(unsigned char, readUInt8, writeUInt8);
        impl_1_(signed short, readInt16, writeInt16);
        impl_1_(unsigned short, readUInt16, writeUInt16);
        impl_1_(signed int, readInt32, writeInt32);
        impl_1_(unsigned int, readUInt32, writeUInt32);
        impl_1_(signed long, readInt32, writeInt32);
        impl_1_(unsigned long, readUInt32, writeUInt32);
        impl_1_(signed long long, readInt64, writeInt64);
        impl_1_(unsigned long long, readUInt64, writeUInt64);
        impl_1_(float, readFlt32, writeFlt32);
        impl_1_(double, readFlt64, writeFlt64);
        impl_1_(long double, readFlt64, writeFlt64);

#undef impl_1_

    }
}
